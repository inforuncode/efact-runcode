package co.runcode.test.ubl;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.gov.dian.contratos.facturaelectronica.v1.InvoiceType;
import co.runcode.factura.bo.FirmadorDocumentosBO;
import co.runcode.factura.bo.GeneradorDocumentosUblBO;
import co.runcode.factura.bo.impl.FirmadorDocumentosBOImpl;
import co.runcode.factura.bo.impl.GeneradorDocumentosUblBOImpl;
import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.factura.dm.jpa.DatosProveedor;
import co.runcode.factura.dm.json.Adquiriente;
import co.runcode.factura.dm.json.Direccion;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.Facturador;
import co.runcode.factura.dm.json.Impuesto;
import co.runcode.factura.dm.json.Item;
import co.runcode.factura.dm.json.PersonaJuridica;
import co.runcode.factura.dm.json.PersonaNatural;

public class GenerarFacturaUblTest {
	
	private static final BigDecimal CIEN = BigDecimal.valueOf(100.0);
	private static final String RUTA_REPOSITORIO_FACTURAS = "src/main/resources/documentos";
	private static final String RUTA_CERTIFICADO_PRUEBAS = "src/main/resources/certificados/persona_juridica_pruebas_vigente.p12";
	private static final String PASSWORD_CERTIFICADO = "persona_juridica_pruebas";
	
	public static void main(String[] args) {
		
		GenerarFacturaUblTest test = new GenerarFacturaUblTest();
		
		Factura f = test.generarFactura();
		test.imprimirFactura(f);
		
//		DatosFacturador df = new DatosFacturador(
//				"1000000", 
//				"PREFIJO",
//				100000,
//				110000,
//				new GregorianCalendar(2017, Calendar.MAY, 21),
//				new GregorianCalendar(2027, Calendar.MAY, 21)
//				);
		
		DatosFacturador df = new DatosFacturador(
				"1000000", 
				"PREFIJO",
				100000,
				110000,
				LocalDate.of(2017, 5, 21),
				LocalDate.of(2027, 5, 21)
				);
		
		DatosProveedor dp = new DatosProveedor(
				"NIT-PROVEEDOR",
				"ID-SOFTWARE","PIN-SOFTWARE"
				);
		
		InvoiceType fe = test.generarFacturaUbl(f, df, dp);	
		
		String rutaArchivo = test.firmarFactura(fe);
		
		System.out.println( "rutaArchivo: " + rutaArchivo );
				
	}
	
	public InvoiceType generarFacturaUbl(Factura f, DatosFacturador df, DatosProveedor dp) {
		GeneradorDocumentosUblBO genDocsUbl = new GeneradorDocumentosUblBOImpl();
		
		try {
			return genDocsUbl.generarFacturaUbl(f, df, dp);
			
		} catch (DatatypeConfigurationException | JAXBException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public String firmarFactura(InvoiceType fe) {
		
		String numFac = fe.getID().getValue();

		File ruta = new File( RUTA_REPOSITORIO_FACTURAS );
		File archivo = new File( RUTA_REPOSITORIO_FACTURAS + "/" + numFac + ".xml" );

		ruta.mkdirs();
		
		FirmadorDocumentosBO firDocsUbl = new FirmadorDocumentosBOImpl();
		
		String rutaArchivo = null;
		try {
			
			rutaArchivo = firDocsUbl.firmarFactura(fe, archivo.getAbsolutePath(), RUTA_CERTIFICADO_PRUEBAS, PASSWORD_CERTIFICADO);
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		return rutaArchivo;
		
		
	}
	
	public void imprimirFactura(Factura f) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		String json;
		try {
			json = objectMapper.writeValueAsString(f);
			System.out.println( json );
			
		} catch (JsonProcessingException e) {
			System.out.println("ERROR AL PARCEAR EL OBJETO.");
			e.printStackTrace();
		}
		
	}

	public Factura generarFactura() {
		//Factura f1 = new Factura();
		
		Factura f1 = new Factura();
		f1.setId("FAC00001");		
		f1.setFechaHoraFactura( (new GregorianCalendar(2017, 8, 4, 8, 30)).getTime() );
		f1.setTipoFactura("1");
		f1.setMoneda("COP");
	
		// Facturador
		PersonaJuridica pFacturador = new PersonaJuridica();
		pFacturador.setIdentificacion("846000553");
		pFacturador.setRazonSocial("Razón Social Empresa");
				
		Direccion direccionF = new Direccion();
		direccionF.setDepartamento("Valle del Cauca");
		direccionF.setMunicipio("Cali");
		direccionF.setCentroPoblado("Centro");
		direccionF.setCodigoPais("CO");
		direccionF.setDireccion("Calle 5 # 1 - 2");		
		pFacturador.setDireccion(direccionF);
		
		Facturador facturador = new Facturador(pFacturador);
		f1.setFacturador(facturador);
		
		// Adquiriente
		PersonaNatural pAdquiriente = new PersonaNatural();
		pAdquiriente.setIdentificacion("CC-ADQUIRIENTE");
		pAdquiriente.setNombres("Nombre1 Nombre2");
		pAdquiriente.setPrimerApellido("Apellido1");
		pAdquiriente.setSegundoApellido("Apellido2");
		
		Direccion direccionA = new Direccion();
		direccionA.setDepartamento("Nariño");
		direccionA.setMunicipio("Pasto");
//		direccionA.setCentroPoblado("Centro");
		direccionA.setCodigoPais("CO");
		direccionA.setDireccion("Calle 29 # 1 - 2");
		pAdquiriente.setDireccion(direccionA);
		
		Adquiriente adquiriente = new Adquiriente(pAdquiriente);
		f1.setAdquiriente(adquiriente);
	
		// Lista de items
		Item it1 = new Item();		
		it1.setNumeroLinea("1");
		it1.setCantidad( BigDecimal.valueOf(5) );
		it1.setDescripcion("Item 1");
		it1.setPrecioUnitario( BigDecimal.valueOf(10000.0) );		
		it1.setCostoTotal( it1.getPrecioUnitario().multiply(it1.getCantidad()) );
						
		f1.getListaItems().add(it1);
					
		Item it2 = new Item();		
		it2.setNumeroLinea("2");
		it2.setCantidad( BigDecimal.valueOf(3) );
		it2.setDescripcion("Item 2");
		it2.setPrecioUnitario( BigDecimal.valueOf(4000.0) );
		it2.setCostoTotal( it2.getPrecioUnitario().multiply(it2.getCantidad()) );							
		
		f1.getListaItems().add(it2);	
		
		Impuesto ivaTotal = new Impuesto();		
		ivaTotal.setImpuestoRetenido(true);
		ivaTotal.setCategoria("01");
		ivaTotal.setBaseImponible( BigDecimal.valueOf(50000.0) );
		ivaTotal.setPorcentaje( BigDecimal.valueOf(19.0) );
		ivaTotal.setImporteImpuesto(  ivaTotal.getBaseImponible().multiply(ivaTotal.getPorcentaje()).divide(CIEN) );
		f1.getTotalImpuestos().add( ivaTotal );
		
		Impuesto icTotal = new Impuesto();
		ivaTotal.setImpuestoRetenido(true);
		icTotal.setCategoria("02");
		icTotal.setBaseImponible( BigDecimal.valueOf(12000.0) );
		icTotal.setPorcentaje( BigDecimal.valueOf(4.0) );
		icTotal.setImporteImpuesto(  icTotal.getBaseImponible().multiply(icTotal.getPorcentaje()).divide(CIEN) );
		f1.getTotalImpuestos().add( icTotal );
				
		f1.setTotalImporteBruto( BigDecimal.valueOf(62000.0) ); // sumatoria costoTotal items
		f1.setTotalBaseImponible( ivaTotal.getImporteImpuesto().add( icTotal.getImporteImpuesto() ) ); // sumatoria impuestos
		f1.setTotalFactura( f1.getTotalImporteBruto().add( f1.getTotalBaseImponible() ) );
		
		return f1;

	}
	
}
