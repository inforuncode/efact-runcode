package co.runcode.test.ubl;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.runcode.factura.dm.json.Adquiriente;
import co.runcode.factura.dm.json.Direccion;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.Facturador;
import co.runcode.factura.dm.json.Impuesto;
import co.runcode.factura.dm.json.Item;
import co.runcode.factura.dm.json.NotaCredito;
import co.runcode.factura.dm.json.PersonaJuridica;
import co.runcode.factura.dm.json.PersonaNatural;

public class GenerarNotaCreditoUblTest {
	
	
	public void imprimirDocumento(Object obj) {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		String json;
		try {
			json = objectMapper.writeValueAsString(obj);
			System.out.println( json );
			
		} catch (JsonProcessingException e) {
			System.out.println("ERROR AL PARCEAR EL OBJETO.");
			e.printStackTrace();
		}
		
	}

	public NotaCredito generarNotaCredito() {
		//Factura f1 = new Factura();
		
		//Factura nc1 = new Factura();
		NotaCredito nc1 = new NotaCredito();
		/*
		nc1.setId("NC00000001");
		nc1.setFechaHoraEmision((new GregorianCalendar(2017, 8, 4, 8, 30)).getTime() );		
		nc1.setTipoFactura("1");
		nc1.setMoneda("COP");
	
		// Facturador
		PersonaJuridica pFacturador = new PersonaJuridica();
		pFacturador.setIdentificacion("846000553");
		pFacturador.setRazonSocial("Razón Social Empresa");
				
		Direccion direccionF = new Direccion();
		direccionF.setDepartamento("Valle del Cauca");
		direccionF.setMunicipio("Cali");
		direccionF.setCentroPoblado("Centro");
		direccionF.setCodigoPais("CO");
		direccionF.setDireccion("Calle 5 # 1 - 2");		
		pFacturador.setDireccion(direccionF);
		
		Facturador facturador = new Facturador(pFacturador);
		nc1.setFacturador(facturador);
		
		// Adquiriente
		PersonaNatural pAdquiriente = new PersonaNatural();
		pAdquiriente.setIdentificacion("CC-ADQUIRIENTE");
		pAdquiriente.setNombres("Nombre1 Nombre2");
		pAdquiriente.setPrimerApellido("Apellido1");
		pAdquiriente.setSegundoApellido("Apellido2");
		
		Direccion direccionA = new Direccion();
		direccionA.setDepartamento("Nariño");
		direccionA.setMunicipio("Pasto");
//		direccionA.setCentroPoblado("Centro");
		direccionA.setCodigoPais("CO");
		direccionA.setDireccion("Calle 29 # 1 - 2");
		pAdquiriente.setDireccion(direccionA);
		
		Adquiriente adquiriente = new Adquiriente(pAdquiriente);
		nc1.setAdquiriente(adquiriente);
	
		// Lista de items
		Item it1 = new Item();		
		it1.setNumeroLinea("1");
		it1.setCantidad( BigDecimal.valueOf(5) );
		it1.setDescripcion("Item 1");
		it1.setPrecioUnitario( BigDecimal.valueOf(10000.0) );		
		it1.setCostoTotal( it1.getPrecioUnitario().multiply(it1.getCantidad()) );
						
		nc1.getListaItems().add(it1);
					
		Item it2 = new Item();		
		it2.setNumeroLinea("2");
		it2.setCantidad( BigDecimal.valueOf(3) );
		it2.setDescripcion("Item 2");
		it2.setPrecioUnitario( BigDecimal.valueOf(4000.0) );
		it2.setCostoTotal( it2.getPrecioUnitario().multiply(it2.getCantidad()) );							
		
		nc1.getListaItems().add(it2);	
		
		Impuesto ivaTotal = new Impuesto();		
		ivaTotal.setImpuestoRetenido(true);
		ivaTotal.setCategoria("01");
		ivaTotal.setBaseImponible( BigDecimal.valueOf(50000.0) );
		ivaTotal.setPorcentaje( BigDecimal.valueOf(19.0) );
		ivaTotal.setImporteImpuesto(  ivaTotal.getBaseImponible().multiply(ivaTotal.getPorcentaje()).divide(CIEN) );
		nc1.getTotalImpuestos().add( ivaTotal );
		
		Impuesto icTotal = new Impuesto();
		ivaTotal.setImpuestoRetenido(true);
		icTotal.setCategoria("02");
		icTotal.setBaseImponible( BigDecimal.valueOf(12000.0) );
		icTotal.setPorcentaje( BigDecimal.valueOf(4.0) );
		icTotal.setImporteImpuesto(  icTotal.getBaseImponible().multiply(icTotal.getPorcentaje()).divide(CIEN) );
		nc1.getTotalImpuestos().add( icTotal );
				
		nc1.setTotalImporteBruto( BigDecimal.valueOf(62000.0) ); // sumatoria costoTotal items
		nc1.setTotalBaseImponible( ivaTotal.getImporteImpuesto().add( icTotal.getImporteImpuesto() ) ); // sumatoria impuestos
		nc1.setTotalFactura( nc1.getTotalImporteBruto().add( nc1.getTotalBaseImponible() ) );
		*/
		
		return nc1;

	}

}
