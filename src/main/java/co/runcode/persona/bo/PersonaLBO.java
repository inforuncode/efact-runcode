package co.runcode.persona.bo;

import java.util.List;
import javax.ejb.Local;

import co.runcode.dm.general.Persona;
import co.runcode.generic.bo.GenericBO;

@Local
public interface PersonaLBO extends GenericBO<Persona> {

	/**
	 * Obtiene una persona determinado por su codigo de verificaciòn de correo
	 * electronico generado al momento de registrarse
	 * 
	 * @param codVerificacion
	 * @return
	 */
	public Persona getPersonaByCodVerificacion(String codVerificacion);

	/**
	 * Obtiene una persona por su correo electrònico
	 * 
	 * @param correo
	 * @return
	 */

	public Persona getPersonaByCorreo(String correo);

	/**
	 * Obtiene una persona por su nombre de Usuario
	 * 
	 * @param nomUsuario
	 * @return
	 */
	public Persona getPersonaByNomUsuario(String nomUsuario);

	/**
	 * Obtiene la lista completa de las personas incluidas en el sistema
	 * 
	 * @return
	 */
	public List<Persona> getListPersonas();

}
