package co.runcode.persona.bo.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import co.runcode.dm.general.Persona;
import co.runcode.generic.bo.impl.GenericBOImpl;
import co.runcode.persona.bo.PersonaLBO;

public class PersonBOImpl extends GenericBOImpl<Persona> implements PersonaLBO {

	private static final long serialVersionUID = -7249415631810211819L;
	private final static Logger log = Logger.getLogger(PersonBOImpl.class.getName());

	public PersonBOImpl() {
		this(Persona.class);
	}

	public PersonBOImpl(Class<Persona> entityClass) {
		super(entityClass);
	}

	@Override
	public Persona getPersonaByCodVerificacion(String codVerificacion) {
		Persona persona = new Persona();
		log.info(" >>>>>>>>>>>>>> Etrando al getPersonaByCodVerificacion");
		try {
			persona = this.searchElementWithOneArg(Persona.class, "usuario.codVerificacion", codVerificacion);
		} catch (Exception e) {
			log.severe("    XXXXXX Ocurriò un error al obtener la Persona con el codigo de Verificaciòn");
			e.printStackTrace();
		}
		return persona;
	}

	@Override
	public Persona getPersonaByCorreo(String correo) {
		Persona persona = new Persona();
		log.info(" >>>>>>>>>>>>>> Entrando al getPersonaByCorreo");
		try {
			persona = this.searchElementWithOneArg(Persona.class, "correo", correo);
		} catch (Exception e) {
			log.severe("    XXXXXX Ocurriò un error al obtener la Persona con el correo getPersonaByCorreo");
			e.printStackTrace();
		}
		return persona;
	}

	@Override
	public Persona getPersonaByNomUsuario(String nomUsuario) {
		Persona persona = new Persona();
		log.info(" >>>>>>>>>>>>>> Entrando al getPersonaByNomUsuario");
		try {
			persona = this.searchElementWithOneArg(Persona.class, "usuario.nomUsuario", nomUsuario);
		} catch (Exception e) {
			log.severe("    XXXXXX Ocurriò un error al obtener la Persona con el getPersonaByNomUsuario");
			e.printStackTrace();
		}
		return persona;
	}

	@Override
	public List<Persona> getListPersonas() {
		List<Persona> listPersonas = new ArrayList<>();
		try {
			listPersonas = this.searchAll(Persona.class);
		} catch (Exception e) {
			log.severe("    XXXXXX Ocurriò un error al obtener el listado de personas del Sistema");
			e.printStackTrace();
			listPersonas = null;
		}

		return listPersonas;
	}
}
