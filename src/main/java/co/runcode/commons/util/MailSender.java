package co.runcode.commons.util;

import javax.annotation.PostConstruct;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Logger;

/**
 * Clase utilizada para realizar los envios de correos electronicos
 * 
 * @author sistemas
 *
 */

public class MailSender implements Serializable {

	private static final long serialVersionUID = 4990878030576644091L;
	private final static Logger log = Logger.getLogger(MailSender.class.getName());

	private Utilidades utl;

	// Estos datos se deben tomar desde una tabla de la base de datos, es
	// necesario no dejarlos
	// Estaticos en esta clase
	private final String SMTP_HOST_NAME = "muriel.colombiahosting.com.co";
	private final String SMTP_AUTH_USER = "eyder.ascuntar@runcode.co";
	private final String SMTP_AUTH_PWD = "123456";

	public MailSender() {
		utl = new Utilidades();
	}

	@PostConstruct
	public void init() {
		log.info("  ####### Inicializando MailSender");
	}

	/**
	 * Funciòn creada para realizar el envio de los correos desde la plataforma
	 * 
	 * @param destinatarios
	 * @param asunto
	 * @param mensaje
	 * @param direccionRemitente
	 * @param nombreRemitente
	 * @return
	 * @throws MessagingException
	 */

	public Response postMail(String destinatarios[], String asunto, String mensaje, String direccionRemitente,
			String nombreRemitente) throws MessagingException {
		Response response = new Response();
		try {
			boolean debug = false;

			// Asignando las propiedades del servidor de correo electrònico
			Properties props = new Properties();
			props.put("mail.smtp.host", SMTP_HOST_NAME);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.auth", "true");
			// Asignando la autenticaciòn
			Authenticator auth = new SMTPAuthenticator();
			Session session = Session.getDefaultInstance(props, auth);
			session.setDebug(debug);

			MimeMessage msg = new MimeMessage(session);

			// Definiendo el remitente
			InternetAddress addressFrom = new InternetAddress(direccionRemitente, nombreRemitente);
			msg.setFrom(addressFrom);
			// Definiendo los destinatarios
			InternetAddress[] addressTo = new InternetAddress[destinatarios.length];
			for (int i = 0; i < destinatarios.length; i++) {
				addressTo[i] = new InternetAddress(destinatarios[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			// Agregando el asunto con soporte para UTF-8
			msg.setSubject(asunto, "UTF-8");
			// Agregando el asunto en formato html y con soporte para UTF-8
			msg.setContent(mensaje, "text/html; charset=UTF-8");
			// Enviando el mensaje de correo Electronico
			Transport.send(msg);
			response.setEstado(true);
			response.setMensajeGen("Correo enviado correctamente!");
		} catch (Exception e) {
			response.setCodError("ER-ACRUN-MAIL-01");
			response.setEstado(false);
			response.setMensajeGen("Ocurriò un error al enviar el Correo");
			response.setLog(utl.getLogFromException(e));
		}
		return response;
	}

	/**
	 * SimpleAuthenticator is used to do simple authentication when the SMTP
	 * server requires it.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			String username = SMTP_AUTH_USER;
			String password = SMTP_AUTH_PWD;
			return new PasswordAuthentication(username, password);
		}
	}

}
