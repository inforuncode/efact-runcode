package co.runcode.commons.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.logging.Logger;
import org.jasypt.util.password.StrongPasswordEncryptor;

public class Utilidades {

	private final static Logger log = Logger.getLogger(Utilidades.class.getName());

	/**
	 * Función que retorna la pila de errores de java en una variable tipo
	 * String
	 * 
	 * @param ex
	 * @return
	 */
	public String getLogFromException(Exception e) {
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	/**
	 * Leer un archivo enviando la direccion y codificaciòn
	 * 
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */

	public String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	/**
	 * Generador de IDS alfanumericos
	 * 
	 * @return
	 */

	public String generarIdAlfanumerico() {
		String id = "";
		String uniqueID = UUID.randomUUID().toString();
		String[] parts = uniqueID.split("-");
		id = parts[parts.length - 1] + parts[1];
		return id;
	}

	/**
	 * Función encargada de encryptar un password para almacenarlo en la base de
	 * datos
	 */

	public String encryptPassword(String passs) {
		String passwordEncryp = "";
		try {
			if (!passs.equals("")) {
				StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
				passwordEncryp = passwordEncryptor.encryptPassword(passs);
			} else {
				passwordEncryp = passs;
			}
		} catch (Exception e) {
			log.severe(" XXXXXXXXX Ocurrió un error al encriptar el password");
			e.printStackTrace();
			passwordEncryp = "";
		}
		return passwordEncryp;
	}

	/**
	 * Función encargada de comprobar si un password es igual al password
	 * encriptado
	 * 
	 * @param pass
	 * @param encryptedPassword
	 * @return
	 */
	public boolean checkPassword(String pass, String encryptedPassword) {
		boolean flag = false;
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		if (passwordEncryptor.checkPassword(pass, encryptedPassword)) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;

	}

}
