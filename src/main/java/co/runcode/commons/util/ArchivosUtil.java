package co.runcode.commons.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.logging.Logger;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;

public class ArchivosUtil {
	
	private static final Logger logger = Logger.getLogger(ArchivosUtil.class.getName());
	
	public void comprimirArchivo(String rutaZip, String rutaArchivo) throws IOException {
		
		File archivo = new File(rutaArchivo);
		File zip = new File(rutaZip);
		
		try ( ArchiveOutputStream o = new ZipArchiveOutputStream(zip) ) {
			
			ArchiveEntry entry = o.createArchiveEntry( archivo, archivo.getName() );	        
			o.putArchiveEntry(entry);
			
	        if (archivo.isFile()) {
	            try (InputStream i = Files.newInputStream(archivo.toPath())) {
	                IOUtils.copy(i, o);
	            }
	        }
	        
		    o.closeArchiveEntry();
		    
		} catch (IOException e) {	
			logger.severe("Error al comprimir el archivo: " + rutaArchivo +" en zip: " + rutaZip);
			e.printStackTrace();
			throw e;
		}

	}
	
	public String bytesToHex(byte[] b) {
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

		StringBuffer sb = new StringBuffer();
		for (int j = 0; j < b.length; j++) {
			sb.append(hexDigit[(b[j] >> 4) & 0x0f]);
			sb.append(hexDigit[b[j] & 0x0f]);
		}

		return sb.toString();
	}

}
