package co.runcode.commons.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Fechas {

	public int getYearFromDate(Date date) {
		int year = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		year = cal.get(Calendar.YEAR);
		return year;
	}

	public int getMonthFromDate(Date date) {
		int month = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		month = cal.get(Calendar.MONTH);
		return month;
	}

	public int getDayFromDate(Date date) {
		int day = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		day = cal.get(Calendar.DAY_OF_MONTH);
		return day;
	}


	
	/**
	 * Funcion creada para obtener una fecha formateada segun las necesidades
	 * @param date fecha a formatear
	 * @param formato formato en el cual se necesita la fecha, dd/MM/yyyy , dd-MM-YYYY ,  yyyy/MM/dd HH:mm:ss  etc.
	 * @return
	 */
	public String getFechaFormato(Date date, String formato){
		String fecha = "";
		DateFormat df = new SimpleDateFormat(formato);
		fecha = df.format(date).toString();
		return fecha;
	}

	public boolean fechaVencida(Date fecha) {
		boolean flag = false;
		Date fechaSistema = new Date();
		if (fecha.compareTo(fechaSistema) > 0) {
			flag = false;
		} else if (fecha.compareTo(fechaSistema) < 0) {
			flag = true;
		} else if (fecha.compareTo(fechaSistema) == 0) {
			flag = true;
		}
		return flag;
	}

}
