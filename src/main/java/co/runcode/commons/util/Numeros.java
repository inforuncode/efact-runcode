package co.runcode.commons.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Numeros {

	public boolean numerosIguales(BigDecimal num1, int num2) {
		boolean flag = false;
		BigDecimal bigDecimal2 = new BigDecimal(num2);
		if (bigDecimal2.compareTo(num1) == 0) {
			flag = true;
		}

		/**
		 * if (num1.compareTo(BigDecimal.ZERO) > 0) { flag = false; } else {
		 * flag = true; }
		 */
		return flag;
	}

	public boolean numerosIguales(BigDecimal num1, BigDecimal num2) {
		boolean flag = false;
		if (num1.compareTo(num2) == 0) {
			flag = true;
		}
		return flag;
	}

	public boolean numeroMenor(BigDecimal num1, BigDecimal num2) {
		boolean flag = false;
		if (num1.compareTo(num2) == -1) {
			flag = true;
		}
		return flag;
	}

	public boolean numeroMayor(BigDecimal num1, BigDecimal num2) {
		boolean flag = false;
		if (num1.compareTo(num2) == 1) {
			flag = true;
		}
		return flag;
	}

	public double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
