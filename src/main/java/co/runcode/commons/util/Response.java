package co.runcode.commons.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Objeto respuesta de todos los llamados a funciones java, se debe responder
 * por cada llamado a funciones un objeto de este tipo donde se especifica el
 * estado de la respuesta, el mensaje, el log en caso de ser necesario y por
 * ultimo mensajes especificos
 * 
 * @author Eyder Ascuntar
 * @mail eyder.ascuntar@runcode.co
 * @Date 20/05/2016
 */
public class Response implements Serializable {

	private static final long serialVersionUID = 302175223295998623L;
	private boolean estado;
	private String tituloMensaje;
	private String mensajeGen;
	private String log;
	private String codError;
	private List<String> mensajeEsp;

	public Response() {
		tituloMensaje = "";
		mensajeGen = "";
		log = "";
		codError = "";
		mensajeEsp = new ArrayList<>();
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getTituloMensaje() {
		return tituloMensaje;
	}

	public void setTituloMensaje(String tituloMensaje) {
		this.tituloMensaje = tituloMensaje;
	}

	public String getMensajeGen() {
		return mensajeGen;
	}

	public void setMensajeGen(String mensajeGen) {
		this.mensajeGen = mensajeGen;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getCodError() {
		return codError;
	}

	public void setCodError(String codError) {
		this.codError = codError;
	}

	public List<String> getMensajeEsp() {
		return mensajeEsp;
	}

	public void setMensajeEsp(List<String> mensajeEsp) {
		this.mensajeEsp = mensajeEsp;
	}

}
