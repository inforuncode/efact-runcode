package co.runcode.admin.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import co.runcode.commons.util.Response;
import co.runcode.dm.general.Persona;
import co.runcode.web.security.register.bo.RegisterLBO;
import co.runcode.web.utilities.Utilidad;

@ManagedBean
@ViewScoped
public class AgregarUsuarioBean implements Serializable {

	private static final long serialVersionUID = 7239643628645071391L;
	private final static Logger log = Logger.getLogger(AdminUsuariosBean.class.getName());

	@Inject
	private RegisterLBO registerLBO;

	@Inject
	private Utilidad utilidad;

	private List<String> roles;
	private List<String> grupos;
	private String[] selectedRoles;
	private String[] selectedGrupos;
	private Persona persona;
	private Response response;

	public AgregarUsuarioBean() {
		grupos = new ArrayList<>();
		roles = new ArrayList<>();
		persona = new Persona();
		response = new Response();

	}

	@PostConstruct
	public void init() {
		log.info("  ####### Inicializando AgregarUsuarioBean");
		cargarGruposSistema();
		cargarRolesSistema();
	}

	public void agregarUsuario() {
		log.info(" ######## Entrando a Agregar un Usuario");
		if (selectedGrupos.length > 0) {
			if (selectedRoles.length > 0) {
				if (persona != null) {
					response = registerLBO.registroUsuarioByAdmin(persona, selectedRoles, selectedGrupos);
					if (response.isEstado()) {
						utilidad.notificacionRegular("Proceso Correcto!", response.getMensajeGen(), "success", false);
						persona = new Persona();
						selectedRoles = null;
						selectedGrupos = null;
					} else {
						utilidad.notificacionRegular("Proceso Fallido!", response.getMensajeGen(), "error", false);
					}
				}
			} else {
				utilidad.notificacionRegular("Roles Requeridos",
						"Se debe seleccionar al menos un Rol del Sistema para el usuario", "error", false);
			}
		} else {
			utilidad.notificacionRegular("Grupos Requeridos",
					"Se debe seleccionar al menos un Grupo del Sistema para el usuario", "error", false);
		}

	}

	public void cargarGruposSistema() {
		log.info(" ##### Entrando a Cargar los Grupos del Sistema");
		grupos = new ArrayList<>();
		grupos = registerLBO.getGrupos();

	}

	public void cargarRolesSistema() {
		log.info(" ##### Entrando a Cargar los Roles del Sistema");
		roles = new ArrayList<>();
		roles = registerLBO.getRoles();
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<String> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<String> grupos) {
		this.grupos = grupos;
	}

	public String[] getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(String[] selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	public String[] getSelectedGrupos() {
		return selectedGrupos;
	}

	public void setSelectedGrupos(String[] selectedGrupos) {
		this.selectedGrupos = selectedGrupos;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
