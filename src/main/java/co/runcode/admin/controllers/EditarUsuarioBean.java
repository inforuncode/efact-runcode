package co.runcode.admin.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Inject;

import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;

import co.runcode.commons.util.Response;
import co.runcode.dm.general.Persona;
import co.runcode.persona.bo.PersonaLBO;
import co.runcode.web.security.register.bo.RegisterLBO;
import co.runcode.web.utilities.Utilidad;

@ManagedBean
@ViewScoped
public class EditarUsuarioBean implements Serializable {

	private static final long serialVersionUID = -7908613065814990432L;
	private final static Logger log = Logger.getLogger(EditarUsuarioBean.class.getName());

	@Inject
	private FacesContext facesContext;

	@Inject
	private Utilidad utilidad;

	@Inject
	private RegisterLBO registerLBO;

	@Inject
	private PersonaLBO personaLBO;

	private Persona persona;
	private String[] selectedRoles;
	private String[] selectedGrupos;
	private List<String> roles;
	private List<String> grupos;
	private Response response;
	private boolean renderBotonesAccion;
	private boolean estadoCuenta;

	public EditarUsuarioBean() {
		persona = new Persona();
		grupos = new ArrayList<>();
		roles = new ArrayList<>();
		response = new Response();
		renderBotonesAccion = false;
		estadoCuenta = false;
	}

	@PostConstruct
	public void init() {
		log.info(" ##### Inicializando EditarUsuarioBean");
		obtenerUsuarioEditar();
	}

	public void obtenerUsuarioEditar() {
		facesContext = FacesContext.getCurrentInstance();
		// Obtenemos la persona desde el contexto Externo
		persona = (Persona) facesContext.getExternalContext().getFlash().get("personaUser");
		// Validamos si la persona fue capturada
		if (persona == null) {
			// Obtenemos la persona con su parametro de nombre de usuario
			String nomUsuario = facesContext.getExternalContext().getRequestParameterMap().get("nomUsuario");
			persona = personaLBO.getPersonaByNomUsuario(nomUsuario);
		}

		// Validamos si pudimos obtener la persona desde alguno de los dos
		// medios

		if (persona != null) {
			utilidad.notificacionRegular("Usuario Encontrado",
					"Se va a editar al usuario  " + persona.getUsuario().getNomUsuario(), "success", true);
			estadoCuenta = persona.getUsuario().isEstadoCuenta();
			// cargarGruposRolesUsuario();
			cargarGruposSistema();
			cargarRolesSistema();
			cargarGruposRolesUsuario();

		} else {
			utilidad.notificacionRegular("Usuario No Encontrado", "No se ha podido encontrar al usuario en mención",
					"warning", true);
			facesContext = FacesContext.getCurrentInstance();
			String urlRedirect = getApplicationUri() + "/app/admin/usuarios/";
			try {
				facesContext.getExternalContext().redirect(urlRedirect);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void guardarCambiosEdicion() {
		if (selectedGrupos.length > 0) {
			if (selectedRoles.length > 0) {
				if (persona != null) {
					response = registerLBO.actualizarUsuarioByAdmin(persona, selectedRoles, selectedGrupos);
					if (response.isEstado()) {
						renderBotonesAccion = true;
						utilidad.executeJavaScriptFunction(
								"$('#formEditarUsuario').find('input, textarea, select').attr('disabled','disabled');");
						utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "success",
								false);
					} else {
						utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error",
								false);
					}
				}
			} else {
				utilidad.notificacionRegular("Roles Requeridos",
						"Se debe seleccionar al menos un Rol del Sistema para el usuario", "error", false);
			}
		} else {
			utilidad.notificacionRegular("Grupos Requeridos",
					"Se debe seleccionar al menos un Grupo del Sistema para el usuario", "error", false);
		}

	}

	public void bloquearCuentaUsuario() {
		estadoCuenta = false;
		response = registerLBO.bloqDesCuentaUsuarioByNomUsuario(persona.getUsuario().getNomUsuario(), false);
		if (response.isEstado()) {
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "success", false);
		} else {
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error", false);
		}
	}

	public void desbloquearCuentaUsuario() {
		estadoCuenta = true;
		response = registerLBO.bloqDesCuentaUsuarioByNomUsuario(persona.getUsuario().getNomUsuario(), true);
		if (response.isEstado()) {
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "success", false);
		} else {
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error", false);
		}
	}

	public void cargarGruposSistema() {
		grupos = new ArrayList<>();
		grupos = registerLBO.getGrupos();

	}

	public void cargarRolesSistema() {
		roles = new ArrayList<>();
		roles = registerLBO.getRoles();
	}

	public void cargarGruposRolesUsuario() {

		List<Role> listRoles = new ArrayList<>();
		List<Group> listGrupos = new ArrayList<>();
		listRoles = registerLBO.getRolesByNomUsuario(persona.getUsuario().getNomUsuario());
		listGrupos = registerLBO.getGruposByNombUsuario(persona.getUsuario().getNomUsuario());

		selectedRoles = new String[roles.size()];
		for (int i = 0; i < listRoles.size(); i++) {
			selectedRoles[i] = listRoles.get(i).getName();
		}
		selectedGrupos = new String[grupos.size()];
		for (int i = 0; i < listGrupos.size(); i++) {
			selectedGrupos[i] = listGrupos.get(i).getName();
		}

	}

	public void eliminarUsuario() {
		log.info("  #### Entrando a Eliminar un Usuario");
		response = new Response();
		if (persona != null) {
			response = registerLBO.eliminarPersonaUsuario(persona);
			if (response.isEstado()) {
				utilidad.mensajeInfo("Proceso Correcto!", "La Cuenta de Usuario fue Eliminada Correctamente");
				try {
					FacesContext facesContext = FacesContext.getCurrentInstance();
					Flash flash = facesContext.getExternalContext().getFlash();
					flash.setKeepMessages(true);
					flash.setRedirect(true);
					FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error", false);
			}
		}
	}

	public void restablecerContrasena() {
		response = new Response();
		if (persona != null) {
			response = registerLBO.restablecerPassword(persona);
			if (response.isEstado()) {
				renderBotonesAccion = true;
				utilidad.executeJavaScriptFunction(
						"$('#formEditarUsuario').find('input, textarea, select').attr('disabled','disabled');");
				utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "success", false);
			} else {
				utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error", false);
			}
		}
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String[] getSelectedRoles() {
		return selectedRoles;
	}

	public void setSelectedRoles(String[] selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	public String[] getSelectedGrupos() {
		return selectedGrupos;
	}

	public void setSelectedGrupos(String[] selectedGrupos) {
		this.selectedGrupos = selectedGrupos;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public List<String> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<String> grupos) {
		this.grupos = grupos;
	}

	public boolean isRenderBotonesAccion() {
		return renderBotonesAccion;
	}

	public void setRenderBotonesAccion(boolean renderBotonesAccion) {
		this.renderBotonesAccion = renderBotonesAccion;
	}

	public boolean isEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(boolean estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}

	/**
	 * Funcion encargada de obtener la dirección url de la aplicación
	 * 
	 * @return
	 */

	public String getApplicationUri() {
		try {
			facesContext = FacesContext.getCurrentInstance();
			ExternalContext ext = facesContext.getExternalContext();
			URI uri = new URI(ext.getRequestScheme(), null, ext.getRequestServerName(), ext.getRequestServerPort(),
					ext.getRequestContextPath(), null, null);
			return uri.toASCIIString();
		} catch (URISyntaxException e) {
			throw new FacesException(e);
		}
	}
}
