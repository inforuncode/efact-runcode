package co.runcode.admin.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import co.runcode.commons.util.Response;
import co.runcode.dm.general.Persona;
import co.runcode.persona.bo.PersonaLBO;
import co.runcode.web.security.register.bo.RegisterLBO;
import co.runcode.web.utilities.Utilidad;

@ManagedBean(name = "adminUsuariosBean")
@ViewScoped
public class AdminUsuariosBean implements Serializable {

	private static final long serialVersionUID = -7000739102193034765L;
	private final static Logger log = Logger.getLogger(AdminUsuariosBean.class.getName());

	@Inject
	private PersonaLBO personBO;

	@Inject
	private RegisterLBO registerLBO;

	@Inject
	Utilidad utilidad;

	private List<Persona> listPersonas;
	private List<Persona> listPersonasFiltradas;
	private Response response;

	public AdminUsuariosBean() {
		listPersonas = new ArrayList<>();
		listPersonasFiltradas = new ArrayList<>();

		response = new Response();
	}

	@PostConstruct
	public void init() {
		log.info("  ########## Inicializando el bean AdminUsuariosBean");
		cargarListaPersonasSistema();

	}

	public void cargarListaPersonasSistema() {
		listPersonas = new ArrayList<>();
		listPersonasFiltradas = new ArrayList<>();
		listPersonas = personBO.searchAll(Persona.class);
		if (listPersonas != null && !listPersonas.isEmpty()) {
			for (Persona persona : listPersonas) {
				listPersonasFiltradas.add(persona);
			}
		}

	}

	public String editarUsuario(Persona persona) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("personaUser", persona);
		return "editar?nomUsuario=" + persona.getUsuario().getNomUsuario() + "&faces-redirect=true";
	}

	public void bloquearCuentaUsuario(Persona persona) {

		response = registerLBO.bloqDesCuentaUsuarioByNomUsuario(persona.getUsuario().getNomUsuario(), false);
		if (response.isEstado()) {
			log.info(" >>>>>>>>>>>>> Voy a recargar informacion despues de bloquear la cuenta del usuario");
			cargarListaPersonasSistema();
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "warning", false);

		} else {
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error", false);
		}
		// Esta linea es para poder actualizar la tabla de usuarios si necesidad
		// de
		// refrescar la página
		utilidad.executeJavaScriptFunction("limpiarFiltrosBusquedaDataTable('usuariosTabla');");
	}

	public void desbloquearCuentaUsuario(Persona persona) {

		response = registerLBO.bloqDesCuentaUsuarioByNomUsuario(persona.getUsuario().getNomUsuario(), true);
		if (response.isEstado()) {
			log.info(" >>>>>>>>>>>>> Voy a recargar informacion despues de desbloquear la cuenta del usuario");
			cargarListaPersonasSistema();
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "success", false);
		} else {
			utilidad.notificacionRegular(response.getTituloMensaje(), response.getMensajeGen(), "error", false);
		}
		// Esta linea es para poder actualizar la tabla de usuarios si necesidad
		// de
		// refrescar la página
		utilidad.executeJavaScriptFunction("limpiarFiltrosBusquedaDataTable('usuariosTabla');");
	}

	public List<Persona> getListPersonas() {
		return listPersonas;
	}

	public void setListPersonas(List<Persona> listPersonas) {
		this.listPersonas = listPersonas;
	}

	public List<Persona> getListPersonasFiltradas() {
		return listPersonasFiltradas;
	}

	public void setListPersonasFiltradas(List<Persona> listPersonasFiltradas) {
		this.listPersonasFiltradas = listPersonasFiltradas;
	}

}
