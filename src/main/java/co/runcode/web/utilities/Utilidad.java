package co.runcode.web.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

/**
 * 
 * @author Eyder Albeiro Ascuntar Rosales
 * @year 2014
 */

@Stateless
@Named
public class Utilidad implements Serializable {

	private static final long serialVersionUID = 4251226551916517595L;
	private static Logger logger = Logger.getLogger(Utilidad.class.getName());

	@Inject
	private FacesContext facesContext;

	private String dirServirdor;

	public Utilidad() {
		dirServirdor = "";
	}

	public String getDirServirdor() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		dirServirdor = "http://" + request.getServerName() + ":" + request.getServerPort();

		return dirServirdor;
	}

	public void setDirServirdor(String dirServirdor) {
		this.dirServirdor = dirServirdor;
	}

	public void mensajeError(String titulo, String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, mensaje));
	}

	public void mensajePeligro(String titulo, String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, mensaje));
	}

	public void mensajeInfo(String titulo, String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, mensaje));
	}

	public void mensajeFatal(String titulo, String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, titulo, mensaje));
	}

	public void abrirDialog(String idDialog) {
		String comando = idDialog + ".show()";
		RequestContext.getCurrentInstance().execute(comando);
	}

	public void actualizarElemento(String idElemento) {
		// System.out.println("El id es el siguiente:
		// "+Utilidad.buscarHtmlComponete(idElemento).getClientId());
		RequestContext.getCurrentInstance()
				.update(Utilidad.buscarHtmlComponete(idElemento).getClientId(FacesContext.getCurrentInstance()));
	}

	public static UIComponent buscarHtmlComponete(String idComponete) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (null != context) {
			return buscarHtmlComponete(context.getViewRoot(), idComponete);
		}
		return null;
	}

	public static UIComponent buscarHtmlComponete(UIComponent parent, String idComponete) {
		if (idComponete.equals(parent.getId())) {
			return parent;
		}
		Iterator<UIComponent> kids = parent.getFacetsAndChildren();
		while (kids.hasNext()) {
			UIComponent kid = kids.next();
			UIComponent found = buscarHtmlComponete(kid, idComponete);
			if (found != null) {
				return found;
			}
		}
		return null;
	}

	public static String estilosErrorInput() {
		return "border-color:#e9322d;-webkit-box-shadow:0 0 6px #f8b9b7;-moz-box-shadow: 0 0 6px #f8b9b7;box-shadow: 0 0 6px #f8b9b7";
	}

	public static void copiarArchivo(String fileName, InputStream in, String destination) {
		try {
			logger.log(Level.INFO, "La ruta donde se creara el archivo es la siguiente-->{0}{1}",
					new Object[] { destination, fileName });
			OutputStream out = new FileOutputStream(new File(destination + fileName));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			in.close();
			out.flush();
			out.close();
			logger.info("Archivo creado correctamente");
		} catch (IOException e) {
			logger.log(Level.INFO, "Error al copiar el archivo{0}", e.getMessage());
		}
	}

	/**
	 * Verifica si una cadena esta vacia o no
	 * 
	 * @param cadena
	 *            a evaluar
	 * @return true si esta vacia y false en caso contrario
	 * 
	 * @author David Timana
	 */
	public boolean cadenaVacia(String cadena) {
		boolean cadenaVacia = false;
		if (cadena == null || cadena.trim().isEmpty()) {
			cadenaVacia = true;
		}
		return cadenaVacia;
	}

	@SuppressWarnings("rawtypes")
	public static boolean listaEstaVacia(List lista) {
		return (lista == null || lista.isEmpty());
	}

	/**
	 * Ejecuta una funcion Javascript desde el Controlador
	 * 
	 * @param nameFunction
	 *            , nombre de la funcion a ejecutar
	 */

	public void executeJavaScriptFunction(String nameFunction) {
		RequestContext.getCurrentInstance().execute(nameFunction);
	}

	/**
	 * Funcio que sirve para Colocar las primeras letras de cada palabra en
	 * mayusculas, ejemplo String nombre = "carlos perez" resultado nombre =
	 * "Carlos Perez"
	 * 
	 * @param givenString
	 *            , String a convertir @return, cadena capitalizada
	 */
	public String capitalizarCadena(String givenString) {
		givenString = givenString.toLowerCase();
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	/**
	 * Función encargada de generar las notificaciones estaticas de la
	 * plataforma
	 * 
	 * @param titulo
	 *            de la notificacion
	 * @param mensaje
	 *            cuerpo del mensaje de la notificación
	 * @param tipo
	 *            puede ser de los siguientes tipos: success, info, warning,
	 *            error, dark
	 * 
	 * 
	 */
	public void notificacionAlarma(String titulo, String mensaje, String tipo) {
		executeJavaScriptFunction("notificacionAlarma('" + titulo + "','" + mensaje + "','" + tipo + "');");
	}

	/**
	 * Notificación normal, desaparece automaticamente o se queda fija en la
	 * pantalla hasta que el usuario decida cerrarla.
	 * 
	 * @param titulo
	 * @param texto
	 * @param tipo
	 * @param automatica
	 */

	public void notificacionRegular(String titulo, String texto, String tipo, boolean automatica) {
		executeJavaScriptFunction("notificacionRegular('<strong>" + titulo + "</strong>','" + texto + "','" + tipo + "')");
	}

	/**
	 * Notificación normal, desaparece automaticamente o se queda fija en la
	 * pantalla hasta que el usuario decida cerrarla. (OSCURA)
	 * 
	 * @param titulo
	 * @param texto
	 * @param tipo
	 * @param automatica
	 */

	public void notificacionRegularOscura(String titulo, String texto, String tipo, boolean automatica) {
		executeJavaScriptFunction("notificacionRegularOscura('<strong>" + titulo + "</strong>','" + texto + "','" + tipo+ "')");
	}

	/**
	 * Notificación regular estatica, no se desaparece por acción del usuario.
	 * 
	 * @param titulo
	 * @param texto
	 * @param tipo
	 */

	public void notificacionRegularEstatica(String titulo, String texto, String tipo) {
		executeJavaScriptFunction(
				"notificacionRegularEstatica('<strong>" + titulo + "</strong>','" + texto + "','" + tipo + "')");
	}

	/**
	 * Notificación regular estatica, no se desaparece por acción del usuario.
	 * (Oscura)
	 * 
	 * @param titulo
	 * @param texto
	 * @param tipo
	 */

	public void notificacionRegularEstaticaOscura(String titulo, String texto, String tipo) {
		executeJavaScriptFunction(
				"notificacionRegularEstaticaOscura('<strong>" + titulo + "</strong>','" + texto + "','" + tipo + "')");
	}

	/**
	 * Funcion encargada de obtener la dirección url de la aplicación
	 * 
	 * @return
	 */

	public String getApplicationUri() {
		try {
			facesContext = FacesContext.getCurrentInstance();
			ExternalContext ext = facesContext.getExternalContext();
			URI uri = new URI(ext.getRequestScheme(), null, ext.getRequestServerName(), ext.getRequestServerPort(),
					ext.getRequestContextPath(), null, null);
			return uri.toASCIIString();
		} catch (URISyntaxException e) {
			throw new FacesException(e);
		}
	}

}