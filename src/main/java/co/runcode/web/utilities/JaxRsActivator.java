package co.runcode.web.utilities;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Eyder Albeiro Ascuntar Rosales
 * @email eaar23@gmail.com
 * @date 16-01-2014
 */

@ApplicationPath("/runcodeWebServices")
public class JaxRsActivator extends Application {

}
