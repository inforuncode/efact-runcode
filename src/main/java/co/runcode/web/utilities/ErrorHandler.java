package co.runcode.web.utilities;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * @author Eyder Albeiro Ascuntar Rosales
 * @email eaar23@gmail.com
 * @date 16-01-2014
 */

@ManagedBean
@RequestScoped
public class ErrorHandler {

	public String getStatusCode() {
		String val = String.valueOf((Integer) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestMap()
				.get("javax.servlet.error.status_code"));
		return val;
	}

	public String getMessage() {
		String val = (String) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestMap()
				.get("javax.servlet.error.message");
		return val;
	}

	public String getExceptionType() {
/**
		String val = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get("javax.servlet.error.exception_type")
				.toString();
				*/
		return "";
	}

	public String getException() {
		StringWriter stringWriter = new StringWriter();
		Exception exception = (Exception) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestMap()
				.get("javax.servlet.error.exception");
	//	exception.printStackTrace(new PrintWriter(stringWriter, true));
		String stackTrace = stringWriter.toString();
		return stackTrace.replace(System.getProperty("line.separator"), "\n");

	}

	public String getStackTrace() {
		StringWriter stringWriter = new StringWriter();
		Exception exception = (Exception) FacesContext.getCurrentInstance()
				.getExternalContext().getRequestMap()
				.get("javax.servlet.error.exception");
		exception.printStackTrace(new PrintWriter(stringWriter, true));
		return stringWriter.toString();
	}

	public String getRequestURI() {
		return (String) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get("javax.servlet.error.request_uri");
	}

	public String getServletName() {
		return (String) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get("javax.servlet.error.servlet_name");
	}

}
