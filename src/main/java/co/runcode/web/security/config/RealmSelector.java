package co.runcode.web.security.config;

import org.picketlink.annotations.PicketLink;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.model.basic.Realm;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * <p>
 * We use this class to hold the current realm for a specific user.
 * </p>
 */
@SessionScoped
@Named
public class RealmSelector implements Serializable {

	private static final long serialVersionUID = -2872651368020420981L;

	@Inject
	private PartitionManager partitionManager;

	private Realm realm;

	@Produces
	@PicketLink
	public Realm select() {
		return this.realm;
	}

	public String getRealm() {
		if (this.realm == null) {
			return null;
		}

		return this.realm.getName();
	}

	public void setRealm(String partitionName) {
		this.realm = this.partitionManager.getPartition(Realm.class, partitionName);
	}
}
