package co.runcode.web.security.config;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.picketlink.annotations.PicketLink;

public class AppSecurityConfig {

	@PersistenceContext(unitName = "basePU")
	private EntityManager em;

	@Produces
	@PicketLink
	public EntityManager getPicketLinkEntityManager() {
		return em;
	}

}
