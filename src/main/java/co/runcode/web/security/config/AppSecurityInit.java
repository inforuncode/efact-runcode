package co.runcode.web.security.config;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Realm;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;

import co.runcode.web.security.controllers.PicketLinkBean;

@Singleton
@Startup
public class AppSecurityInit {

	private final static Logger log = Logger.getLogger(AppSecurityInit.class.getName());

	@Inject
	private PicketLinkBean picket;

	public AppSecurityInit() {
		log.info(" ################# Inicializando Seguridad PicketLink");
	}

	@PostConstruct
	public void create() {
//		configuracionInicialApp();

	}

	public void configuracionInicialApp() {
		// Creando Particion Principal
		if (picket.createPartition("RuncodePartition")) {
			log.info(" ################# Particion Creada Correctamente");
		} else {
			log.severe(" XXXXXXXXXXXXX La Particion no pudo ser creada, o ya existe");
		}

		Realm partition = picket.getPartitionByName("RuncodePartition");
		if (partition != null) {

			// Creación de Grupos
			picket.creatGroupOnPartition("Administradores", partition);
			picket.creatGroupOnPartition("Empleados", partition);
			picket.creatGroupOnPartition("Auditores", partition);

			// Creación de Roles
			picket.createRoleOnPartition("SuperAdministrador", partition);
			picket.createRoleOnPartition("Soporte", partition);
			picket.createRoleOnPartition("Auditor", partition);
			picket.createRoleOnPartition("Operador", partition);

			// Creación de Usuario Administrador
			Password password = new Password("admin");
			picket.addUserToPartition(generateUser("admin", "Administrador", "Sistema", "runcode@runcode.co"), password,
					partition);
			// Se obtiene los obtejos creados
			// Grupos
			Group administradores = picket.getGroupByName("Administradores", partition);
			Group empleados = picket.getGroupByName("Empleados", partition);
			Group auditores = picket.getGroupByName("Auditores", partition);

			// Roles
			Role superAdministrador = picket.getRoleByName("SuperAdministrador", partition);
			Role operador = picket.getRoleByName("Operador", partition);
			Role soporte = picket.getRoleByName("Soporte", partition);
			Role auditor = picket.getRoleByName("Auditor", partition);

			// Obteniendo el Usuario Creado
			User admin = picket.getUserByLoginName("admin", partition);

			if (admin != null) {
				// Asignación de Grupos a los usuarios
				picket.addUserToGroup(admin, administradores);
				picket.addUserToGroup(admin, empleados);
				picket.addUserToGroup(admin, auditores);

				// Asignación de Roles a los usuarios
				picket.addRoleToUsuer(admin, superAdministrador);

				picket.addRoleToUsuer(admin, soporte);
				picket.addRoleToUsuer(admin, auditor);
				picket.addRoleToUsuer(admin, operador);
				log.info(" >>>>>>>>>>>>>>> Todo se creó correctamente");
			}

		} else {
			log.severe(" XXXXXXXXXXXXX La Particion solicitada no existe");
		}

	}

	public User generateUser(String loginName, String firstName, String lastName, String email) {
		User user = new User(loginName);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		return user;
	}

}