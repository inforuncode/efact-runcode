/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2012, Red Hat, Inc., and individual contributors
 * as indicated by the @author tags. See the copyright.txt file in the
 * distribution for a full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package co.runcode.web.security.config;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

import javax.enterprise.event.Observes;

/**
 * <p>
 * A simple CDI observer for the
 * {@link org.picketlink.event.SecurityConfigurationEvent}.
 * </p>
 *
 * <p>
 * The event is fired during application startup and allows you to provide any
 * configuration to PicketLink before it is initialized.
 * </p>
 *
 * <p>
 * All the configuration related with Http Security is provided from this bean.
 * </p>
 *
 * @author Pedro Igor
 */
public class HttpSecurityConfiguration {

	

    public void configureHttpSecurity(@Observes SecurityConfigurationEvent event) {
    	System.out.println(" >>>>>>>>>>>>>>>> Activando la Seguridad HTTP Picketlink");
    	  SecurityConfigurationBuilder builder = event.getBuilder();

    	  builder  
    	  	.http()  
    	  		.forGroup("Authentication") // group definition  
    	  			.authenticateWith()  
    	  				.form()  
    	  				.authenticationUri("/login.jsf")  
    	  				.loginPage("/login.jsf")  
    	  				.errorPage("/pages/error/error.jsf?faces-redirect=true")  
    	  	.forPath("/app/*", "Authentication") // specify the group for the given path  
    	  	    	.redirectTo("/login.jsf?faces-redirect=true").whenForbidden()
    	  	    	.redirectTo("/pages/error/error.jsf?faces-redirect=true").whenError()
    	  	.forPath("/app/admin/*", "Authentication")
					.authorizeWith()
						.group("Administradores")
						.redirectTo("/pages/error/accessDenied.jsf?faces-redirect=true").whenForbidden()
						.redirectTo("/pages/error/error.jsf?faces-redirect=true").whenError()
			.forPath("/app/auditor/*", "Authentication")
					.authorizeWith()
						.group("Auditores")
						.redirectTo("/pages/error/accessDenied.jsf?faces-redirect=true").whenForbidden()
						.redirectTo("/pages/error/error.jsf?faces-redirect=true").whenError()
			.forPath("/app/empleado/*", "Authentication")
					.authorizeWith()
						.group("Empleados")
						.redirectTo("/pages/error/accessDenied.jsf?faces-redirect=true").whenForbidden()
						.redirectTo("/pages/error/error.jsf?faces-redirect=true").whenError()			
       ;  
	
    }
}


