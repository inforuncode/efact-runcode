package co.runcode.web.security.register.bo.impl;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.FacesException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Realm;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;
import co.runcode.commons.util.MailSender;
import co.runcode.commons.util.Response;
import co.runcode.commons.util.Utilidades;
import co.runcode.dm.general.Persona;
import co.runcode.persona.bo.PersonaLBO;
import co.runcode.web.security.controllers.PicketLinkBean;
import co.runcode.web.security.register.bo.RegisterLBO;

public class RegisterBOImpl implements Serializable, RegisterLBO {

	private static final long serialVersionUID = 893564360225062276L;
	private final static Logger log = Logger.getLogger(RegisterBOImpl.class.getName());

	private Utilidades utl;
	private MailSender mailSender;

	@Inject
	private PersonaLBO personLBO;

	@Inject
	private FacesContext facesContext;

	@Inject
	private PicketLinkBean picketLinkBean;

	public RegisterBOImpl() {
		utl = new Utilidades();
		mailSender = new MailSender();
	}

	@Override
	public Response registroPersonaUsuario(Persona persona) {
		persona.getUsuario().setCodVerificacion(utl.generarIdAlfanumerico());
		persona.getUsuario().setFechaRegistro(new Date());

		log.info(" ####### Iniciando logica para Registro de Usuarios");
		Response response = new Response();
		String codError = "";
		try {
			if (persona != null) {
				String grupos[] = new String[] { "Academia" };
				String roles[] = new String[] { "Profesor" };
				response = registroUsuario(persona, grupos, roles, false);
				// Verificacion de Registro como Usuario Picketlink
				if (response.isEstado()) {
					response = new Response();
					response = registroPersona(persona);
					// Verificacion de Registro como Persona
					if (response.isEstado()) {
						response = new Response();
						response = enviarCorreoConfirmacionCuenta(persona);
					}
				}
			}
		} catch (Exception e) {
			codError = "ER-ACRUN-REG-01";
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError(codError);
			response.setMensajeGen(
					"Ocurrió un error al Registrar el Usuario, Contacte al Administrador de la Plataforma.");
		}
		return response;
	}

	@Override
	public Response registroUsuarioByAdmin(Persona persona, String[] listRoles, String[] listGrupos) {
		log.info(" ####### Iniciando logica para Registro de Usuarios registroUsuarioByAdmin");
		Response response = new Response();
		persona.getUsuario().setCodVerificacion(utl.generarIdAlfanumerico());
		persona.getUsuario().setFechaRegistro(new Date());
		try {
			if (persona != null) {
				persona.getUsuario().setCuentaVerificada(true);
				persona.getUsuario().setEstadoCuenta(true);
				persona.getUsuario().setPassword(persona.getUsuario().getNomUsuario());
				persona.getUsuario().setPasswordActualizado(false);
				response = registroUsuario(persona, listGrupos, listRoles, true);
				// Verificacion de Registro como Usuario Picketlink
				if (response.isEstado()) {
					response = new Response();
					response = registroPersona(persona);
					response.setMensajeGen("El usuario ha sido agregado correctamente.");
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurrió un error al Registrar el Usuario. Contacte a Soporte Técnico!");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-REGUSUAD-01");
			response.setMensajeGen("Ocurrió un error al Registrar el Usuario. Contacte a Soporte Técnico!");
			e.printStackTrace();
		}

		return response;
	}

	public Response registroPersona(Persona persona) {
		log.info(" ####### Iniciando logica para registroPersona");
		Response response = new Response();
		String codError = "";
		try {
			String passEncryp = utl.encryptPassword(persona.getUsuario().getPassword());
			persona.getUsuario().setPassword(passEncryp);
			if (personLBO.add(persona)) {
				response.setEstado(true);
				response.setMensajeGen(
						"Se ha registrado correctamente en Academic Runcode, por favor recuerde activar su cuenta siguiendo las instrucciones enviadas a su correo electrónico.");
			} else {
				codError = "ER-ACRUN-REG-04";
				response.setEstado(false);
				response.setMensajeGen(
						"Ocurrió un error al Crear la Persona, Contacté al Administrador de la Plataforma.");
				response.setCodError(codError);
			}

		} catch (Exception e) {
			codError = "ER-ACRUN-REG-03";
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError(codError);
			response.setMensajeGen("Ocurrió un error al Crear la Persona, Contacté al Administrador de la Plataforma.");
		}
		return response;
	}

	public Response registroUsuario(Persona persona, String grupos[], String roles[], boolean estadoCuenta) {
		log.info(" ####### Iniciando logica para registroUsuario");
		Response response = new Response();
		String codError = "";
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				// Comprobamos que no haya un usuario registrado con el mismo
				// nombre de usuario o correo electronico
				if (!picketLinkBean.usuarioEstaRegistradoByNomUsuarioCorreo(persona.getUsuario().getNomUsuario(),
						persona.getCorreo(), partition)) {
					User user = new User(persona.getUsuario().getNomUsuario());
					user.setFirstName(persona.getPrimerNombre());
					user.setLastName(persona.getPrimerApellido());
					user.setEmail(persona.getCorreo());
					// Linea creada para dejar por defecto deshabilitado al
					// usuario que se registre, obligandolo a confirmar su
					// cuenta por algun medio
					user.setEnabled(estadoCuenta);
					Password password = new Password(persona.getUsuario().getPassword());
					if (picketLinkBean.addUserToPartition(user, password, partition)) {
						picketLinkBean.agregarUsuarioAGrupos(user, grupos, partition);
						picketLinkBean.agregarRolesAUsuario(user, roles, partition);
						response.setEstado(true);
						response.setMensajeGen(
								"Se ha registrado correctamente en Academic Runcode, por favor recuerde activar su cuenta siguiendo las instrucciones enviadas a su correo electrónico.");
					}
				} else {
					response.setEstado(false);
					response.setMensajeGen(
							"El Nombre de Usuario o Correo Electrónico ingresados ya se encuentran registrados en el sistema, por favor verifíque sus datos.");
				}
			} else {
				response.setEstado(false);
				response.setMensajeGen("No se pudo obtener la Partición de Usuarios, Contacte a Soporte Técnico!");
			}

		} catch (Exception e) {
			log.severe(" XXXXXXXXXXXXXXX Ocurrió un error al Crear Usuario en Seguridad, Contacte a Soporte Técnico!");
			codError = "ER-ACRUN-REG-05";
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError(codError);
			response.setMensajeGen("Ocurrió un error al Crear Usuario en Seguridad, Contacte a Soporte Técnico!");
		}
		return response;
	}

	public Response enviarCorreoConfirmacionCuenta(Persona persona) {
		Response response = new Response();
		log.info(" >>>>>>> Entrando a enviar el correo de verificacion Academic Runcode");
		try {
			String direccionInicioSesion = getApplicationUri() + "/emailConfirm.jsf";
			String destinatarios[];
			destinatarios = new String[] { persona.getCorreo() };
			// Obteniendo la ubicaciòn del archivo plantilla para poder enviar
			// el correo
			String path = Thread.currentThread().getContextClassLoader().getResource("mailtemp/email.html").getPath();
			String content = utl.readFile(path, Charset.defaultCharset());
			content = content.replace("$#$#$#USUARIOEmevasiRuncode#$#$#$",
					persona.getPrimerNombre() + " " + persona.getPrimerApellido());
			content = content.replace("$#$#$#CODVERIFICACION#$#$#$", persona.getUsuario().getCodVerificacion());

			content = content.replace("$#$#$#DIRVERIFICACION#$#$#$", direccionInicioSesion);
			response = mailSender.postMail(destinatarios, "Confirmaciòn Cuenta Academic Runcode", content,
					"info@runcode.co", "Academic Runcode");
			if (response.isEstado()) {
				response.setMensajeGen(
						"Se ha registrado correctamente en Academic Runcode, por favor recuerde activar su cuenta digitando el Código de Verificación enviado a su correo electrónico.");
			} else {
				response.setMensajeGen(
						"Ocurrió un error al enviar el Correo de Confirmación, Contacte a Soporte Técnico!");
			}
		} catch (Exception e) {
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-MAIL-02");
			response.setMensajeGen(
					"Ocurrió un error al leer la plantilla del Correo de Confirmación, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	public Response enviarCorreoContrasenaTmp(Persona persona) {
		Response response = new Response();
		log.info(" >>>>>>> Entrando a enviar el correo de contraseña Temporal Academic Runcode");
		try {

			String direccionInicioSesion = getApplicationUri() + "/login.jsf";
			String destinatarios[];
			destinatarios = new String[] { persona.getCorreo() };
			String path = Thread.currentThread().getContextClassLoader().getResource("mailtemp/resetPassword.html")
					.getPath();
			String content = utl.readFile(path, Charset.defaultCharset());
			content = content.replace("$#$#$#USUARIOEmevasiRuncode#$#$#$",
					persona.getPrimerNombre() + " " + persona.getPrimerApellido());
			content = content.replace("$#$#$#NOMUSUARIO#$#$#$", persona.getUsuario().getNomUsuario());
			content = content.replace("$#$#$#CONTRASENATMP#$#$#$", persona.getUsuario().getPassword());
			content = content.replace("$#$#$#DIRLOGIN#$#$#$", direccionInicioSesion);
			response = mailSender.postMail(destinatarios, "Recuperar datos Acceso!", content, "info@runcode.co",
					"Academic Runcode");
			if (response.isEstado()) {
				response.setMensajeGen(
						"Se ha enviado una Contraseña Temporal a su Correo. Recuerde actualizarla en el proximo Inicio de Sesión.");
			} else {
				response.setMensajeGen(
						"Ocurrió un error al enviar el Correo de Confirmación, Contacte a Soporte Técnico!");
			}
		} catch (Exception e) {
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-MAIL-02");
			response.setMensajeGen(
					"Ocurrió un error al leer la plantilla del Correo de Confirmación, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response confirmarRegistro(String codVerificacion) {
		log.info("   ########## BO Ingresando a Activar el Registro con el còdigo   " + codVerificacion);
		codVerificacion = codVerificacion.replaceAll("\\s", "");
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByCodVerificacion(codVerificacion);
			if (persona != null && !persona.getUsuario().isCuentaVerificada()) {
				log.info("  ###### Nombre de Usuario Encontrado:  " + persona.getUsuario().getNomUsuario());
				Realm partition = new Realm();
				// Obtiene la particion por defecto de la empresa
				partition = picketLinkBean.getPartitionByName("RuncodePartition");
				if (partition != null) {
					User user = picketLinkBean.getUserByLoginName(persona.getUsuario().getNomUsuario(), partition);
					if (user != null) {
						if (picketLinkBean.enableDisableUser(user, partition, true)) {
							persona.getUsuario().setCuentaVerificada(true);
							persona.getUsuario().setEstadoCuenta(true);
							if (personLBO.edit(persona)) {
								response.setEstado(true);
								response.setMensajeGen("La cuenta ha sido verificada y activada correctamente!");
							}
						}
					}

				}
			} else {
				response.setEstado(false);
				response.setMensajeGen(
						"El Còdigo de verificaciòn ingresado es incorrecto o ha sido inactivado, por favor verifique los datos.");

			}

		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al confirmar el Registro");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-CONFIR-01");
			response.setMensajeGen("Ocurrió un error al confirmar su cuenta, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public Response recuperarCuentaUsuario(String correoRecuperaCuenta) {
		log.info("   ######## BO recuperarCuentaUsuario " + correoRecuperaCuenta);
		correoRecuperaCuenta = correoRecuperaCuenta.replaceAll("\\s", "");
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByCorreo(correoRecuperaCuenta);
			if (persona != null) {
				log.info("  ###### Nombre de Usuario Encontrado:  " + persona.getUsuario().getNomUsuario());
				Realm partition = new Realm();
				// Obtiene la particion por defecto de la empresa
				partition = picketLinkBean.getPartitionByName("RuncodePartition");
				if (partition != null) {
					User user = picketLinkBean.getUserByLoginName(persona.getUsuario().getNomUsuario(), partition);
					if (user != null) {
						String passwordTmp = utl.generarIdAlfanumerico();
						String passwordEnc = utl.encryptPassword(passwordTmp);
						persona.getUsuario().setPassword(passwordEnc);
						persona.getUsuario().setPasswordActualizado(false);
						if (picketLinkBean.updatePasswordUser(user, passwordTmp, partition)) {
							if (personLBO.edit(persona)) {
								persona.getUsuario().setPassword(passwordTmp);
								response = enviarCorreoContrasenaTmp(persona);
							}
						}
					}
				}
			} else {
				response.setEstado(false);
				response.setMensajeGen(
						"No se encuentra una cuenta relacionada con el correo ingresado, verifique e intentelo nuevamente.");
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error recuperarCuentaUsuario");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-REACCOUNT-01");
			response.setMensajeGen("Ocurrió un error al confirmar su cuenta, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response estadoPasswordUsuario(String userName) {
		log.info("   ######## BO estadoPasswordUsuario " + userName);
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByNomUsuario(userName);
			if (persona != null) {
				if (persona.getUsuario().isPasswordActualizado()) {
					response.setEstado(true);
					response.setMensajeGen("La contraseña del usuario está actualizada");
				} else {
					response.setEstado(false);
					response.setMensajeGen(
							"La contraseña del usuario No está actualizada, se debe hacer el procedimiento para actualizarla.");
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al verificar estadoPasswordUsuario");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-REACCOUNT-02");
			response.setMensajeGen("Ocurrió un error al verificar estado Contraseña, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public Response actualizarPassword(String userName, String nuevoPassword) {
		log.info("   ######## BO actualizarPassword " + nuevoPassword);
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByNomUsuario(userName);
			if (persona != null) {
				Realm partition = new Realm();
				// Obtiene la particion por defecto de la empresa
				partition = picketLinkBean.getPartitionByName("RuncodePartition");
				if (partition != null) {
					User user = picketLinkBean.getUserByLoginName(userName, partition);
					if (user != null) {
						if (picketLinkBean.updatePasswordUser(user, nuevoPassword, partition)) {
							nuevoPassword = utl.encryptPassword(nuevoPassword);
							persona.getUsuario().setPassword(nuevoPassword);
							persona.getUsuario().setPasswordActualizado(true);
							if (personLBO.edit(persona)) {
								response.setEstado(true);
								response.setMensajeGen("La contraseña se actualizó correctamente");
							} else {
								response.setEstado(false);
								response.setMensajeGen(
										"Ocurrió un error al actualizar la contraseña, en Persona. Contacte a Soporte Técnico! ");
							}
						} else {
							response.setEstado(false);
							response.setMensajeGen(
									"Ocurrió un error al actualizar la contraseña, en Api. Contacte a Soporte Técnico! ");
						}
					}
				} else {
					response.setEstado(false);
					response.setMensajeGen(
							"Ocurrió un error al actualizar la contraseña, no se encontró la partición. Contacte a Soporte Técnico! ");
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al actualizar actualizarPassword");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-REACCOUNT-03");
			response.setMensajeGen("Ocurrió un error al Actualizar la Contraseña General. Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response actualizarPassword(String userName, String nuevoPassword, String actualPassword) {
		log.info("   ######## BO actualziarPassword Para Usuarios con Contraseña Activa" + nuevoPassword);
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByNomUsuario(userName);

			if (persona != null && !nuevoPassword.equals("") && !actualPassword.equals("")) {
				String passEncryp = persona.getUsuario().getPassword();
				if (utl.checkPassword(actualPassword, passEncryp)) {
					response = actualizarPassword(userName, nuevoPassword);
				} else {
					response.setEstado(false);
					response.setMensajeGen(
							"La contraseña digitada como Actual es invalida, por favor verifique sus datos e intentelo nuevamente.");
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al actualizar actualizarPassword");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-REACCOUNT-04");
			response.setMensajeGen("Ocurrió un error al Actualizar la Contraseña General. Contacte a Soporte Técnico!");
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public List<Role> getRolesByNomUsuario(String nomUsuario) {
		List<Role> listRoles = new ArrayList<>();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				User user = picketLinkBean.getUserByLoginName(nomUsuario, partition);
				if (user != null) {
					listRoles = picketLinkBean.getListRoleUserHas(user, partition);
				}
			}

		} catch (Exception e) {
			listRoles = null;
			log.severe("  XXXXXX Ocurrió un error al obtener los Roles por Nombre de Usuario");
			e.printStackTrace();
		}
		return listRoles;
	}

	@Override
	public List<Group> getGruposByNombUsuario(String nomUsuario) {
		List<Group> listGrupos = new ArrayList<>();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				User user = picketLinkBean.getUserByLoginName(nomUsuario, partition);
				if (user != null) {
					listGrupos = picketLinkBean.getListGroupsMeberUser(user, partition);
				}
			}

		} catch (Exception e) {
			listGrupos = null;
			log.severe("  XXXXXX Ocurrió un error al obtener los Grupos a los que pertenece un Usuario");
			e.printStackTrace();
		}
		return listGrupos;
	}

	@Override
	public List<String> getRoles() {
		List<String> roles = new ArrayList<>();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				List<Role> listRoles = picketLinkBean.getListRolesByPartition(partition);
				if (listRoles != null && !listRoles.isEmpty()) {
					for (Role role : listRoles) {
						roles.add(role.getName());
					}
				}
			}

		} catch (Exception e) {
			roles = null;
			log.severe("  XXXXXX Ocurrió un error al obtener los Roles del Sistema");
			e.printStackTrace();
		}
		return roles;
	}

	@Override
	public List<String> getGrupos() {
		log.info("  ##### Entrando a obtener el listado de grupos del Sistema");
		List<String> grupos = new ArrayList<>();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				log.info("  ### La paraticion es diferente de Null");
				List<Group> listGrupos = picketLinkBean.getListGroupsByPartition(partition);
				if (listGrupos != null && !listGrupos.isEmpty()) {
					log.info("   #### La lista de grupos es diferente de vacia en el sistema");

					for (Group group : listGrupos) {
						grupos.add(group.getName());
					}
				}
			}

		} catch (Exception e) {
			grupos = null;
			log.severe("  XXXXXX Ocurrió un error al obtener los Grupos del Sistema");
			e.printStackTrace();
		}
		return grupos;
	}

	@Override
	public Response actualizarFechaUltimoIngresoUsuario(String nomUsuario, Date fechaUltimoIngreso) {
		log.info("   ######## BO actualizarFechaUltimoIngresoUsuario " + nomUsuario);
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByNomUsuario(nomUsuario);
			if (persona != null) {
				persona.getUsuario().setFechaUltimoIngreso(fechaUltimoIngreso);
				if (personLBO.edit(persona)) {
					response.setEstado(true);
					response.setMensajeGen("Ultimo Ingreso Usuario Actualizado");
				} else {
					response.setEstado(false);
					response.setMensajeGen(
							"Ocurrió un error al  actualizar Ultimo Ingreso, Contacte a Soporte Técnico!");
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al actualizar Ultimo Ingreso Usuario");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-ACCUSU-01");
			response.setMensajeGen("Ocurrió un error al  actualizar Ultimo Ingreso, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response actualizarFechaUltimaSincroMovilUsuario(String nomUsuario, Date fechaUltimaSincro) {
		log.info("   ######## BO actualizarFechaUltimoIngresoUsuario " + nomUsuario);
		Response response = new Response();
		try {
			Persona persona = new Persona();
			persona = personLBO.getPersonaByNomUsuario(nomUsuario);
			if (persona != null) {
				persona.getUsuario().setFechaUltimaSincronziacion(fechaUltimaSincro);
				if (personLBO.edit(persona)) {
					response.setEstado(true);
					response.setMensajeGen("Ultima Sincronización Movil Usuario Actualizada");
				} else {
					response.setEstado(false);
					response.setMensajeGen(
							"Ocurrió un error al  actualizar Ultimo Ingreso, Contacte a Soporte Técnico!");
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al actualizar Ultimo Ingreso Usuario");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-ACCUSU-01");
			response.setMensajeGen("Ocurrió un error al  actualizar Ultimo Ingreso, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response actualizarUsuarioByAdmin(Persona persona, String[] listRoles, String[] listGrupos) {
		log.info("   ######## BO actualizarUsuarioByAdmin " + persona.getUsuario().getNomUsuario());
		Response response = new Response();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				User user = picketLinkBean.getUserByLoginName(persona.getUsuario().getNomUsuario(), partition);
				if (user != null) {
					if (picketLinkBean.editarGruposUsuario(user, partition, listGrupos)) {
						if (picketLinkBean.editarRolesUsuario(user, partition, listRoles)) {
							if (personLBO.edit(persona)) {
								response.setTituloMensaje("Proceso Correcto");
								response.setEstado(true);
								response.setMensajeGen("La información del Usuario fue correctamente Actualizada");
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al actualizar el Usuario");
			response.setTituloMensaje("Proceso Fallido");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-ACCUSU-02");
			response.setMensajeGen("Ocurrió un error al  actualizar el Usuario, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Funcion encargada de obtener la dirección url de la aplicación
	 * 
	 * @return
	 */

	public String getApplicationUri() {
		try {
			facesContext = FacesContext.getCurrentInstance();
			ExternalContext ext = facesContext.getExternalContext();
			URI uri = new URI(ext.getRequestScheme(), null, ext.getRequestServerName(), ext.getRequestServerPort(),
					ext.getRequestContextPath(), null, null);
			return uri.toASCIIString();
		} catch (URISyntaxException e) {
			throw new FacesException(e);
		}
	}

	@Override
	public Response bloqDesCuentaUsuarioByNomUsuario(String nomUsuario, boolean estado) {
		log.info("   ######## BO bloqDesCuentaUsuarioByNomUsuario " + nomUsuario);
		Response response = new Response();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				User user = picketLinkBean.getUserByLoginName(nomUsuario, partition);
				if (user != null) {
					if (picketLinkBean.enableDisableUser(user, partition, estado)) {
						Persona persona = personLBO.getPersonaByNomUsuario(nomUsuario);
						persona.getUsuario().setEstadoCuenta(estado);
						if (personLBO.edit(persona)) {
							response.setTituloMensaje("Proceso Correcto");
							response.setEstado(true);
							response.setMensajeGen("La Cuenta del Usuario fue correctamente Actualizada");
						} else {
							response.setTituloMensaje("Proceso Fallido");
							response.setEstado(false);

							response.setCodError("ER-ACRUN-ACCUSU-03");
							response.setMensajeGen(
									"Ocurrió un error al  actualizar el Usuario, Contacte a Soporte Técnico!");
						}
					} else {
						response.setTituloMensaje("Proceso Fallido");
						response.setEstado(false);
						response.setCodError("ER-ACRUN-ACCUSU-03");
						response.setMensajeGen(
								"Ocurrió un error al  actualizar el Usuario, Contacte a Soporte Técnico!");
					}
				}
			}

		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al actualizar el Usuario");
			response.setTituloMensaje("Proceso Fallido");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-ACCUSU-03");
			response.setMensajeGen("Ocurrió un error al  actualizar el Usuario, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response eliminarPersonaUsuario(Persona persona) {
		log.info("   ######## BO eliminarPersonaUsuario " + persona.getUsuario().getNomUsuario());
		Response response = new Response();
		try {
			Realm partition = new Realm();
			// Obtiene la particion por defecto de la empresa
			partition = picketLinkBean.getPartitionByName("RuncodePartition");
			if (partition != null) {
				User user = picketLinkBean.getUserByLoginName(persona.getUsuario().getNomUsuario(), partition);
				if (user != null) {
					if (picketLinkBean.removeUser(user, partition)) {
						if (personLBO.delete(persona)) {
							response.setTituloMensaje("Proceso Correcto");
							response.setMensajeGen("El Usuario fue eliminado correctamente!");
							response.setEstado(true);
						}
					}
				}
			}
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al eliminarPersonaUsuario");
			response.setTituloMensaje("Proceso Fallido");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-ELIM-01");
			response.setMensajeGen("Ocurrió un error al  eliminar el Usuario, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public Response restablecerPassword(Persona persona) {
		log.info("   ######## BO restablecerPassword " + persona.getUsuario().getNomUsuario());
		Response response = new Response();
		try {
			response = actualizarPassword(persona.getUsuario().getNomUsuario(), persona.getUsuario().getNomUsuario());
		} catch (Exception e) {
			log.severe("  XXXXXX Ocurriò un error al eliminarPersonaUsuario");
			response.setTituloMensaje("Proceso Fallido");
			response.setEstado(false);
			response.setLog(utl.getLogFromException(e));
			response.setCodError("ER-ACRUN-ELIM-01");
			response.setMensajeGen("Ocurrió un error al  eliminar el Usuario, Contacte a Soporte Técnico!");
			e.printStackTrace();
		}
		return response;
	}

}
