package co.runcode.web.security.register.bo;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;
import co.runcode.commons.util.Response;
import co.runcode.dm.general.Persona;

@Local
public interface RegisterLBO {

	/**
	 * Función encargada de crear una persona la cual será un usuario dentro de
	 * la plataforma
	 * 
	 * @param persona
	 * @return
	 */
	public Response registroPersonaUsuario(Persona persona);

	/**
	 * Función encargada de conrimar el registro de una cuenta a través de el
	 * codigo de verificación enviado al correo electronico
	 * 
	 * @param codVerificacion
	 * @return
	 */

	public Response confirmarRegistro(String codVerificacion);

	/**
	 * Función encargada de recuperar los datos de la cuenta de un usuario los
	 * cuales serán enviados al correo con el cual se inscribió el usuario
	 * 
	 * @param correoRecuperaCuenta
	 * @return
	 */

	public Response recuperarCuentaUsuario(String correoRecuperaCuenta);

	/**
	 * Función encargada de consultar el estado del password de un usuario
	 * 
	 * @param userName
	 * @return
	 */
	public Response estadoPasswordUsuario(String userName);

	/**
	 * Función encargada de actualizar el password de un usuario
	 * 
	 * @param userName
	 * @param nuevoPassword
	 * @return
	 */

	public Response actualizarPassword(String userName, String nuevoPassword);

	public Response actualizarPassword(String userName, String nuevoPassword, String actualPassword);

	/**
	 * Función encargada de obtener la lista de Roles que tiene un usuario por
	 * nombre de usuario
	 * 
	 * @param nomUsuario
	 * @return
	 */

	public List<Role> getRolesByNomUsuario(String nomUsuario);

	/**
	 * Funciòn encargada de retornar los roles definidos en el sistema
	 * 
	 * @return
	 */

	public List<String> getRoles();

	/**
	 * Función encargada de obtener la lista de grupos a los cuales pertenece un
	 * usuario por nombre de usuario
	 * 
	 * @param nomUsuario
	 * @return
	 */
	public List<Group> getGruposByNombUsuario(String nomUsuario);

	/**
	 * Funciòn encargada de retornar los grupos definidos en el sistema
	 * 
	 * @return
	 */

	public List<String> getGrupos();

	/**
	 * Función encargada de crear un Usuario en el sistema, este metodo es usado
	 * solo por usuarios administradores.
	 * 
	 * @param persona
	 * @param listRoles
	 * @param listGrupos
	 * @return
	 */
	public Response registroUsuarioByAdmin(Persona persona, String[] listRoles, String[] listGrupos);

	/**
	 * Función encargada de actualizar la fecha del Ultimo Ingreso de un usuario
	 * en la plataforma Web
	 * 
	 * @param nomUsuario
	 * @param fechaUltimoIngreso
	 * @return
	 */
	public Response actualizarFechaUltimoIngresoUsuario(String nomUsuario, Date fechaUltimoIngreso);

	/**
	 * Función encargada de actualizar la fecha de la ultima sincronización de
	 * un usuario por la app movil
	 * 
	 * @param nomUsuario
	 * @param fechaUltimaSincro
	 * @return
	 */

	public Response actualizarFechaUltimaSincroMovilUsuario(String nomUsuario, Date fechaUltimaSincro);

	/**
	 * 
	 * @param persona
	 * @param listRoles
	 * @param listGrupos
	 * @return
	 */
	public Response actualizarUsuarioByAdmin(Persona persona, String[] listRoles, String[] listGrupos);

	/**
	 * Funciòn encargada de bloquear o desbloquear una cuenta de Usuario por su
	 * nombre de usuario
	 * 
	 * @param nomUsuario
	 * @param estado
	 * @return
	 */
	public Response bloqDesCuentaUsuarioByNomUsuario(String nomUsuario, boolean estado);

	/**
	 * Función encargad de eliminar un usuario del sistema incluyento la
	 * partición de Picketlink y la persona y usuario en la base de datos del
	 * sistema runcode
	 * 
	 * @param persona
	 * @return
	 */
	public Response eliminarPersonaUsuario(Persona persona);
	
	/**
	 * Función encargada de restablecer la contraseña de un usuario,
	 * esta por defecto quedará como el nombre de usuario y se solicitará que se
	 * actualice en el proximo inicio de sesión
	 * @param persona
	 * @return
	 */
	public Response restablecerPassword(Persona persona);
}
