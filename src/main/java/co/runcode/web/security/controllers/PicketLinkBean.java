package co.runcode.web.security.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import static org.picketlink.idm.model.basic.BasicModel.*;
import org.picketlink.idm.IdentityManager;
import org.picketlink.idm.PartitionManager;
import org.picketlink.idm.RelationshipManager;
import org.picketlink.idm.credential.Password;
import org.picketlink.idm.model.basic.BasicModel;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Realm;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;
import org.picketlink.idm.query.Condition;
import org.picketlink.idm.query.IdentityQuery;
import org.picketlink.idm.query.IdentityQueryBuilder;

/**
 * @author Eyder Albeiro Ascuntar Rosales
 * @date 14-08-2014
 */

@Stateless
public class PicketLinkBean implements Serializable {

	private static final long serialVersionUID = 3703702001798300095L;
	private final static Logger log = Logger.getLogger(PicketLinkBean.class.getName());

	@Inject
	private PartitionManager partitionManager;

	public PicketLinkBean() {
	}

	@PostConstruct
	public void init() {
		log.info(" >>>>>>>>>>>>>>>> Iniciando PicketLinkBean  ");
	}

	/**
	 * Función que se encarga de crear una partición (Empresa) en el sistema
	 * PicketLink
	 * 
	 * @param namePartition
	 *            nombre de la partición a crear
	 * @return true si se creó la partición, false en caso contrario.
	 */
	public boolean createPartition(String namePartition) {
		boolean flag = false;
		try {
			Realm partition = this.partitionManager.getPartition(Realm.class, namePartition);
			if (partition == null) {
				partition = new Realm(namePartition);
				this.partitionManager.add(partition);
				flag = true;
			}

		} catch (Exception e) {
			log.severe(" >>>>>>>>>>> Ocurrío un error al crear la Particion");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función que se encarga de obtener una partición (Empresa) del sistema
	 * PicketLink según el nombre
	 * 
	 * @param namePartition
	 *            nombre de la partición a buscar
	 * @return partition encontrada, null en caso contrario.
	 */
	public Realm getPartitionByName(String namePartition) {
		Realm partition = new Realm();
		try {
			partition = this.partitionManager.getPartition(Realm.class, namePartition);
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>> Ocurrío un error al obtener la Particion");
			e.printStackTrace();
			partition = null;
		}
		return partition;
	}

	/**
	 * Función encargada de crear un usuarion en una partición
	 * 
	 * @param user
	 *            usuario a ser creado en la partición
	 * @param passwordUser
	 *            contraseña del usuario
	 * @param partition
	 *            partición en la cual será creado
	 * @return true si se creo, false en caso contrario
	 */
	public boolean addUserToPartition(User user, Password passwordUser, Realm partition) {
		boolean flag = false;
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			identityManager.add(user);
			identityManager.updateCredential(user, passwordUser);

			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Agregar el Usuario a la partición");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public boolean updatePasswordUser(User user, String passwordUser, Realm partition) {
		log.info(" >>>>>>>>>>> El nuevo Password  " + passwordUser);
		boolean flag = false;
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			Password password = new Password(passwordUser);
			identityManager.updateCredential(user, password);
			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Actualizar el password del Usuario");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función encargada de crear un grupo en una partición
	 * 
	 * @param groupName
	 *            nombre del grupo a crear
	 * @param partition
	 *            partición en la cual se creará el grupo
	 * @return true si se creo, false en caso contrario
	 */
	public boolean creatGroupOnPartition(String groupName, Realm partition) {
		boolean flag = false;
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			Group group = new Group(groupName);
			identityManager.add(group);
			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Agregar Un Grupo a la partición");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función encargada de crear un role en una partición
	 * 
	 * @param roleName
	 *            nombre del rol a crear
	 * @param partition
	 *            partición en la cual se creará el rol
	 * @return true si se creo, false en caso contrario
	 */
	public boolean createRoleOnPartition(String roleName, Realm partition) {
		boolean flag = false;
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			Role role = new Role(roleName);
			identityManager.add(role);
			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Agregar Un Rol a la partición");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función encargada de buscar un usuario en una partición
	 * 
	 * @param loginName
	 *            nombre de usuario a buscar
	 * @param partition
	 *            partición en la cual se va a buscar
	 * @return user encontrado, null en caso contrario
	 */
	public User getUserByLoginName(String loginName, Realm partition) {
		User user = new User();
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			user = BasicModel.getUser(identityManager, loginName);
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al obtener el Usuario");
			user = null;
		}

		return user;
	}

	public User getUserByCorreo(String email, Realm partition) {

		log.info(" >>>>>>>>>> Entrando a obtener el Usuario mediante Correo");
		User usuario = new User();
		IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
		// here we get the query builder
		IdentityQueryBuilder builder = identityManager.getQueryBuilder();
		// create a condition
		Condition condition = builder.equal(User.EMAIL, email);
		// create a query for a specific identity type using the previously
		// created condition
		IdentityQuery query = builder.createIdentityQuery(User.class).where(condition);
		// execute the query
		List<User> listUsuarios = new ArrayList<>();
		listUsuarios = query.getResultList();
		if (!listUsuarios.isEmpty()) {
			usuario = listUsuarios.get(0);
		} else {
			usuario = null;
		}
		return usuario;
	}

	/**
	 * Funcion encargada de validar si un usuario esta registrado en el sistema
	 * ya sea por su cuenta de correo electronico o su nombre de usuario
	 * 
	 * @param nomUsuario
	 * @param email
	 * @param partition
	 * @return true si esta registrado, false en caso contrario
	 */
	public boolean usuarioEstaRegistradoByNomUsuarioCorreo(String nomUsuario, String email, Realm partition) {
		boolean flag = false;
		User usuario = new User();
		usuario = getUserByLoginName(nomUsuario, partition);
		if (usuario == null) {
			usuario = getUserByCorreo(email, partition);
			if (usuario == null) {
				flag = false;
			} else {
				flag = true;
			}
		} else {
			flag = true;
		}
		return flag;
	}

	/**
	 * Función encargada de hacer a un usuario miembro de un grupo
	 * 
	 * @param user
	 *            usuario a ser miembro
	 * @param group
	 *            grupo al cual se asignará al usuario
	 * @return true si se agregó el usuario, false en caso contrario
	 */
	public boolean addUserToGroup(User user, Group group) {
		boolean flag = false;
		try {
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			addToGroup(relationshipManager, user, group);
			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Asignar a un Grupo un Usuario");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función encargada de asignarle un rol a un usuario
	 * 
	 * @param user
	 *            usuario al cual se le asignará el rol
	 * @param role
	 *            rol a asignar
	 * @return true si se asignó el rol, false en caso contrario
	 */
	public boolean addRoleToUsuer(User user, Role role) {
		boolean flag = false;
		try {
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			grantRole(relationshipManager, user, role);
			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Asignar un Rol al Usuario");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función encargada de hacer miembro de un grupo a un usuario y asignarle
	 * un rol
	 * 
	 * @param user
	 *            usuario a ser tratado
	 * @param role
	 *            rol a asignar
	 * @param group
	 *            grupo al cual será asignado
	 * @return true si se realizó la operación correctamente, false en caso
	 *         contrario
	 */
	public boolean addRoleGroupToUser(User user, Role role, Group group) {
		boolean flag = false;
		try {
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			grantGroupRole(relationshipManager, user, role, group);
			flag = true;
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al Asignar un Rol y Hacer miembro del grupo a un usuario");
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}

	/**
	 * Función encargada de retornar un grupo de una partición
	 * 
	 * @param groupName
	 *            nombre del grupo a buscar
	 * @param partition
	 *            partición a la cual pertenece el grupo
	 * @return grupo si fue encontrado, null en caso contrario
	 */

	public Group getGroupByName(String groupName, Realm partition) {
		Group group = new Group();
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			group = BasicModel.getGroup(identityManager, groupName);

		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al obtener el Grupo");
			e.printStackTrace();
			group = null;
		}
		return group;
	}

	/**
	 * Función encargada de retornar un rol de una partición
	 * 
	 * @param roleName
	 *            nombre del rol a buscar
	 * @param partition
	 *            partición a la cual pertenece el rol
	 * @return rol si fue econtrado, null en caso contrario
	 */
	public Role getRoleByName(String roleName, Realm partition) {
		Role role = new Role();
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			role = BasicModel.getRole(identityManager, roleName);
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al obtener el Grupo");
			e.printStackTrace();
			role = null;
		}
		return role;
	}

	@SuppressWarnings("deprecation")
	public List<Group> getListGroupsByPartition(Realm partition) {
		List<Group> listGroups = new ArrayList<>();
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			IdentityQuery<Group> query = identityManager.createIdentityQuery(Group.class);
			listGroups = query.getResultList();
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al obtener el getListGroupsByPartition");
			e.printStackTrace();
		}
		return listGroups;
	}

	@SuppressWarnings("deprecation")
	public List<Role> getListRolesByPartition(Realm partition) {
		List<Role> listRoles = new ArrayList<>();
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			IdentityQuery<Role> query = identityManager.createIdentityQuery(Role.class);
			listRoles = query.getResultList();
		} catch (Exception e) {
			log.severe(" >>>>>>>>>>>>>>> Ocurrío un error al obtener el getListGroupsByPartition");
			e.printStackTrace();
		}
		return listRoles;
	}

	public User generateUser(String loginName, String firstName, String lastName, String email) {
		User user = new User(loginName);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		return user;
	}

	public boolean revokeRoleToUser(User user, Role role) {
		boolean bandera = false;
		if (role != null && user != null) {
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			revokeRole(relationshipManager, user, role);
			bandera = true;

		}
		return bandera;
	}

	public boolean revokeGroupToUser(User user, Group group) {
		boolean flag = false;
		log.info(">>> entes del if ");
		if (group != null && user != null) {
			log.info(">>> dentro del if ");
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			removeFromGroup(relationshipManager, user, group);
			flag = true;
		}
		return flag;
	}

	public boolean userHasRole(User user, Role role) {
		boolean flag = false;
		if (user != null && role != null) {
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			flag = hasRole(relationshipManager, user, role);
		}
		return flag;
	}

	public boolean userIsMemberGroup(User user, Group group) {
		boolean flag = false;
		if (user != null && group != null) {
			RelationshipManager relationshipManager = this.partitionManager.createRelationshipManager();
			flag = isMember(relationshipManager, user, group);
		}
		return flag;
	}

	public List<Group> getListGroupsMeberUser(User user, Realm partition) {
		List<Group> listGroupsUser = new ArrayList<>();
		List<Group> listGroupsPartition = getListGroupsByPartition(partition);
		for (Group group : listGroupsPartition) {
			if (userIsMemberGroup(user, group)) {
				listGroupsUser.add(group);
			}
		}
		return listGroupsUser;
	}

	public List<Role> getListRoleUserHas(User user, Realm partition) {
		List<Role> listRoleUser = new ArrayList<>();
		List<Role> listRolePartition = getListRolesByPartition(partition);
		for (Role role : listRolePartition) {
			if (userHasRole(user, role)) {
				listRoleUser.add(role);
			}
		}
		return listRoleUser;
	}

	public boolean enableDisableUser(User user, Realm partition, boolean state) {
		boolean flag = false;
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			user.setEnabled(state);
			identityManager.update(user);
			flag = true;
		} catch (Exception e) {
			log.severe("  XXXXX Ocurriò un error al actualizar estado de la cuenta del Usuario");
			e.printStackTrace();
		}
		return flag;
	}

	public boolean agregarUsuarioAGrupos(User user, String grupos[], Realm partition) {
		boolean flag = false;
		try {
			for (int i = 0; i < grupos.length; i++) {
				Group group = new Group();
				group = getGroupByName(grupos[i], partition);
				if (group != null) {
					if (addUserToGroup(user, group)) {
						flag = true;
					} else {
						flag = false;
					}
				} else {
					flag = false;
				}
			}
		} catch (Exception e) {
			log.severe(" Ocurrió un error al agregar los Roles al Usuario");
			e.printStackTrace();
		}
		return flag;
	}

	public boolean agregarRolesAUsuario(User user, String roles[], Realm partition) {
		boolean flag = false;
		try {
			for (int i = 0; i < roles.length; i++) {
				Role role = new Role();
				role = getRoleByName(roles[i], partition);
				if (role != null) {
					if (addRoleToUsuer(user, role)) {
						flag = true;
					} else {
						flag = false;
					}
				} else {
					flag = false;
				}
			}
		} catch (Exception e) {
			log.severe(" Ocurrió un error al agregar los Roles al Usuario");
			e.printStackTrace();
		}
		return flag;
	}

	public boolean editarGruposUsuario(User user, Realm partition, String gruposActuales[]) {
		boolean flag = false;
		try {
			List<Group> listGrupos = getListGroupsMeberUser(user, partition);
			for (Group group : listGrupos) {
				revokeGroupToUser(user, group);
			}

			for (int i = 0; i < gruposActuales.length; i++) {
				Group group = new Group();
				group = getGroupByName(gruposActuales[i], partition);
				addUserToGroup(user, group);
			}
			flag = true;

		} catch (Exception e) {
			log.severe("  Ocurrió un error al editar los grupos del usuario");
			e.printStackTrace();
		}
		return flag;
	}

	public boolean editarRolesUsuario(User user, Realm partition, String rolesActuales[]) {
		boolean flag = false;
		try {
			List<Role> listRoles = getListRoleUserHas(user, partition);
			for (Role role : listRoles) {
				revokeRoleToUser(user, role);
			}

			for (int i = 0; i < rolesActuales.length; i++) {
				Role role = new Role();
				role = getRoleByName(rolesActuales[i], partition);
				addRoleToUsuer(user, role);
			}
			flag = true;

		} catch (Exception e) {
			log.severe("  Ocurrió un error al editar los roles del usuario");
			e.printStackTrace();
		}
		return flag;
	}

	public boolean removeUser(User user, Realm partition) {
		boolean flag = false;
		try {
			IdentityManager identityManager = this.partitionManager.createIdentityManager(partition);
			identityManager.remove(user);
			flag = true;
		} catch (Exception e) {
			log.severe("  Ocurrió un error al eliminar el usuario en Seguridad");
			flag = false;
		}
		return flag;

	}

}
