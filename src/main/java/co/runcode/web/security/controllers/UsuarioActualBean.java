package co.runcode.web.security.controllers;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import co.runcode.dm.general.Persona;
import co.runcode.persona.bo.PersonaLBO;

@ManagedBean
@SessionScoped
public class UsuarioActualBean implements Serializable {

	private static final long serialVersionUID = -7245011419097009610L;
	private final static Logger log = Logger.getLogger(UsuarioActualBean.class.getName());

	@Inject
	private UserBean userBean;

	@Inject
	private PersonaLBO personaBO;

	private Persona usuarioActual;

	public UsuarioActualBean() {
		usuarioActual = new Persona();
	}

	@PostConstruct
	public void init() {
		log.info("  ###### Inicializando UsuarioActualBean");
	}

	public void cargarUsuarioActual() {
		log.info("   ##### Entrando a Obtener el Usuario Actual");
		usuarioActual = new Persona();
		usuarioActual = personaBO.getPersonaByNomUsuario(userBean.getCurrentUserPicketLink().getLoginName());
		if (usuarioActual != null) {
			log.info("  ######### El usuario actual es  " + usuarioActual.getPrimerNombre() + "   "
					+ usuarioActual.getPrimerApellido());
		}
	}

	public Persona getUsuarioActual() {
		return usuarioActual;
	}

	public void setUsuarioActual(Persona usuarioActual) {
		this.usuarioActual = usuarioActual;
	}

}
