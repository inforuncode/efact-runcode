package co.runcode.web.security.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import org.picketlink.Identity;
import org.picketlink.Identity.AuthenticationResult;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.model.Account;
import org.picketlink.idm.model.basic.Group;
import org.picketlink.idm.model.basic.Role;
import org.picketlink.idm.model.basic.User;

import co.runcode.commons.util.Response;
import co.runcode.dm.general.Persona;
import co.runcode.persona.bo.PersonaLBO;
import co.runcode.web.security.config.RealmSelector;
import co.runcode.web.security.register.bo.RegisterLBO;
import co.runcode.web.utilities.Utilidad;

/**
 * Bean cread para gestionar toda la funcionalidad de usuarios del Sistema tal
 * como registro, login, actualizacion de contraseñas etc.
 * 
 * @author sistemas
 * @date 20/05(2016
 * @email eyder.ascuntar@runcode.co
 *
 */

@ManagedBean
@ViewScoped
public class UserBean implements Serializable {

	private final static Logger log = Logger.getLogger(UserBean.class.getName());
	private static final long serialVersionUID = -1446964015733312677L;

	@Inject
	private RegisterLBO registerLBO;

	@Inject
	private Utilidad utilidad;

	@Inject
	private Identity identity;

	@Inject
	private DefaultLoginCredentials loginCredentials;

	@Inject
	private FacesContext facesContext;

	@Inject
	private RealmSelector realmSelector;

	private Persona persona;
	private Response response;
	private boolean activoRegistroUsuario;
	private boolean inactivoRegistroUsuario;
	private boolean inactivoCuentaConfirmada;
	private boolean recuperaCuenta;
	private boolean estadoNuevoPassword;
	private String urlRedirect;
	private String codVerificacion;
	private String correoRecuperaCuenta;
	private String nuevoPassword;
	private String passwordActual;

	public UserBean() {

		persona = new Persona();
		response = new Response();
		activoRegistroUsuario = true;
		urlRedirect = "";
		codVerificacion = "";
		correoRecuperaCuenta = "";
		nuevoPassword = "";
		passwordActual = "";

	}

	@PostConstruct
	public void init() {
		log.info(" ########### Inicializando UserBean");
		log.info(" >>>>>>>>>>>>>>>> Inicializando el LoginController PostConstruct, by default RuncodePartition");
		realmSelector.setRealm("RuncodePartition");
	}

	/**
	 * Función encargada de enviar el objeto que creara un usuario y posteriormente
	 * una persona en la plataforma.
	 */

	public void registroPersonaUsuario() {
		log.info("  ########## Entrando a Registrar la persona Usuario");
		response = new Response();
		if (persona != null) {
			log.info("  ########## El usuario esta listo para ser persistido");
			response = registerLBO.registroPersonaUsuario(persona);
			if (response != null) {
				if (response.isEstado()) {
					log.info(" ######### Todo el registro se realizó correctamente");
					utilidad.notificacionRegular("Proceso Correcto", response.getMensajeGen(), "success", false);
					utilidad.notificacionAlarma("Proceso Correcto", response.getMensajeGen(), "success");
					activoRegistroUsuario = false;
					inactivoRegistroUsuario = true;
					persona = new Persona();
					utilidad.actualizarElemento("panelRegistroUsuarioAcademicRuncode");

				} else {
					log.info(" XXXXXXXX " + response.getMensajeGen());
					utilidad.notificacionAlarma("Proceso Fallido",
							response.getMensajeGen() + " " + response.getCodError(), "error");

				}
			}
		}
	}

	/**
	 * Función encargada de loggear un usuario en el sistema, verifica si las
	 * credenciales son correctas, verifica si la cuenta esta bloqueada o inactiva
	 * 
	 * @throws IOException
	 */

	public void login() throws IOException {
		log.info("    ########## Entrando a loggear un usuario");
		response = new Response();
		try {
			AuthenticationResult result = identity.login();
			log.info("    ######### Finalizando proceso de loggeo");
			if (AuthenticationResult.FAILED.equals(result)) {
				utilidad.notificacionRegular("Datos Incorrectos",
						"Los datos de ingreso son incorrectos, por favor verifiquelos e intentelo nuevamente.", "error",
						false);
			} else {
				response = registerLBO.estadoPasswordUsuario(loginCredentials.getUserId());
				if (response.isEstado()) {
					registerLBO.actualizarFechaUltimoIngresoUsuario(loginCredentials.getUserId(), new Date());

					log.info(" ##### La dirección de la App " + getApplicationUri());
					/*
					 * List<Role> listRoles = new ArrayList<>(); List<Group> listGrupos = new
					 * ArrayList<>(); listRoles = registerLBO.getRolesByNomUsuario(loginCredentials.
					 * getUserId()); listGrupos =
					 * registerLBO.getGruposByNombUsuario(loginCredentials. getUserId()); for (Group
					 * group : listGrupos) { log.info( " >>>>>>> Grupos de  " +
					 * loginCredentials.getUserId() + "    " + group.getName()); }
					 * 
					 * for (Role role : listRoles) { log.info( " >>>>>>> Roles de  " +
					 * loginCredentials.getUserId() + "    " + role.getName()); }
					 */

					facesContext = FacesContext.getCurrentInstance();
					urlRedirect = getApplicationUri() + "/app/index.jsf";
					facesContext.getExternalContext().redirect(urlRedirect);
				} else {
					log.info(response.getMensajeGen());
				}
			}
		} catch (Exception e) {
			log.severe("Cuenta Bloqueada");
			e.printStackTrace();
			while (e.getCause() != null)
				e = (Exception) e.getCause();
			String causaRuncod = "Account [" + loginCredentials.getUserId() + "] is disabled.";
			String causaPicket = e.getMessage();
			if (causaRuncod.equals(causaPicket)) {
				causaRuncod = "<Strong>" + loginCredentials.getUserId()
						+ "</Strong>, su cuenta ha sido dehabilitada, por favor contacte al administrador del sistema.";
				utilidad.notificacionRegular("Cuenta Deshabilitada", causaRuncod, "warning", false);
			}

		}
	}

	/**
	 * Función encargada de realizar la confirmación del registro de la cuenta en el
	 * sistema utiliza un codigo de verificación enviado al correo electronico y
	 * actualiza el estado de la cuenta.
	 */

	public void confirmarRegistro() {
		log.info("   ########## Bean Ingresando a Activar el Registro con el còdigo   " + codVerificacion);
		response = new Response();
		response = registerLBO.confirmarRegistro(codVerificacion);
		if (response.isEstado()) {
			utilidad.notificacionRegular("Proceso Correcto", response.getMensajeGen(), "success", false);
			activoRegistroUsuario = false;
			inactivoRegistroUsuario = false;
			inactivoCuentaConfirmada = true;
			codVerificacion = "";
			utilidad.actualizarElemento("panelRegistroUsuarioAcademicRuncode");
		} else {
			utilidad.notificacionRegular("Proceso Fallido", response.getMensajeGen(), "error", false);
			utilidad.notificacionAlarma("Proceso Fallido", response.getMensajeGen(), "error");
		}
	}

	/**
	 * Función encargada de recuperar los datos de ingreso de un usuario a la
	 * plataforma mediante el envio de un codigo de verificación al correo
	 * electrónico.
	 */

	public void recuperarCuentaUsuario() {
		log.info("  ########  Bean recuperarCuentaUsuario con  " + correoRecuperaCuenta);
		response = new Response();
		response = registerLBO.recuperarCuentaUsuario(correoRecuperaCuenta);
		if (response.isEstado()) {
			utilidad.notificacionRegular("Proceso Correcto", response.getMensajeGen(), "success", false);
			correoRecuperaCuenta = "";
			recuperaCuenta = true;
			utilidad.actualizarElemento("panelRegistroUsuarioAcademicRuncode");
		} else {
			utilidad.notificacionRegular("Proceso Fallido", response.getMensajeGen(), "error", false);
		}
	}

	/**
	 * Funcion encargada de obtener la dirección url de la aplicación
	 * 
	 * @return
	 */

	public String getApplicationUri() {
		try {
			facesContext = FacesContext.getCurrentInstance();
			ExternalContext ext = facesContext.getExternalContext();
			URI uri = new URI(ext.getRequestScheme(), null, ext.getRequestServerName(), ext.getRequestServerPort(),
					ext.getRequestContextPath(), null, null);
			return uri.toASCIIString();
		} catch (URISyntaxException e) {
			throw new FacesException(e);
		}
	}

	/**
	 * Función encargada de verificar si la cuenta de un usuario esta activa o no
	 * 
	 * @return
	 */
	public boolean isActiveAccount() {
		Account account = this.identity.getAccount();
		if (account != null) {
			if (account.isEnabled())
				return true;
		}
		return false;
	}

	/**
	 * Funcion encargada de redireccionar los usuarios loggeados en el sistema pero
	 * que tienen la contraseña sin actualizar
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void redirectLoggedInUsers(ComponentSystemEvent event) throws IOException {
		facesContext = FacesContext.getCurrentInstance();
		response = new Response();
		User user = getCurrentUserPicketLink();
		if (user != null) {
			log.info(" >>>>>>>>>>>>> En Redirect LoggedInUsers  " + user.getLoginName());
			registerLBO.actualizarFechaUltimoIngresoUsuario(loginCredentials.getUserId(), new Date());
			response = registerLBO.estadoPasswordUsuario(user.getLoginName());
			if (response.isEstado()) {
				facesContext = FacesContext.getCurrentInstance();
				urlRedirect = getApplicationUri() + "/app/";
				facesContext.getExternalContext().redirect(urlRedirect);
			} else {
				log.info(" >>>>>>>>>>> Voy a enviarlo al reset para el password");
				facesContext = FacesContext.getCurrentInstance();
				urlRedirect = getApplicationUri() + "/resetPassword.jsf";
				facesContext.getExternalContext().redirect(urlRedirect);
			}
		}
	}

	/**
	 * Función encargada de redireccionar usuarios en el sistema con la contraseña
	 * actualizada
	 * 
	 * @param event
	 * @throws IOException
	 */

	public void redirectLoggedInUsersApp(ComponentSystemEvent event) throws IOException {
		log.info(" >>>>>>>>>> Entrando en el rederictLoggedInUsers");
		facesContext = FacesContext.getCurrentInstance();
		response = new Response();
		User user = getCurrentUserPicketLink();

		if (user == null) {
			System.out.println("is isLogged out");
			String result = getApplicationUri() + "/login.jsf";
			facesContext.getExternalContext().redirect(result);
		}
	}

	/**
	 * Funcion encargada de redireccionar los usuarios que no estan loggeados en el
	 * sistema
	 * 
	 * @param event
	 * @throws IOException
	 */

	public void redirectLoggedOutUsers(ComponentSystemEvent event) throws IOException {
		facesContext = FacesContext.getCurrentInstance();
		if (!(this.identity.isLoggedIn())) {
			System.out.println("is isLogged out");
			String result = getApplicationUri() + "/login.jsf";
			facesContext.getExternalContext().redirect(result);
		}
	}

	/**
	 * Función encargada de cerrar la sesión de un usuario
	 * 
	 * @throws IOException
	 */

	public void logout() throws IOException {
		this.identity.logout();
		facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().invalidateSession();
		facesContext.getExternalContext().redirect(getApplicationUri() + "/login.jsf");
	}

	/**
	 * Función encargada de actualizar el password de un usuario
	 */

	public void actualizarPassword(int tipo) {
		log.info("  ####### Este es el tipo " + tipo);
		response = new Response();
		User user = getCurrentUserPicketLink();
		if (user != null) {
			if (tipo == 1) {
				response = registerLBO.actualizarPassword(user.getLoginName(), nuevoPassword, passwordActual);
				if (response.isEstado()) {
					utilidad.notificacionRegular("Proceso Correcto", response.getMensajeGen(), "success", false);
					nuevoPassword = "";
					estadoNuevoPassword = true;
					utilidad.actualizarElemento("panelRegistroUsuarioAcademicRuncode");
				} else {
					utilidad.notificacionRegular("Proceso Fallido", response.getMensajeGen(), "error", false);
				}
			} else {
				response = registerLBO.actualizarPassword(user.getLoginName(), nuevoPassword);
				if (response.isEstado()) {
					utilidad.notificacionRegular("Proceso Correcto", response.getMensajeGen(), "success", false);
					nuevoPassword = "";
					estadoNuevoPassword = true;
					utilidad.actualizarElemento("panelRegistroUsuarioAcademicRuncode");
				} else {
					utilidad.notificacionRegular("Proceso Fallido", response.getMensajeGen(), "error", false);
				}
			}

		}
	}

	/**
	 * Función encargada de obtener el usuario actualmente loggeado en el sistema
	 * 
	 * @return
	 */

	public User getCurrentUserPicketLink() {
		User user = new User();
		if (this.identity.isLoggedIn()) {
			user = ((User) identity.getAccount());
		} else {
			user = null;
		}
		return user;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public boolean isActivoRegistroUsuario() {
		return activoRegistroUsuario;
	}

	public void setActivoRegistroUsuario(boolean activoRegistroUsuario) {
		this.activoRegistroUsuario = activoRegistroUsuario;
	}

	public boolean isInactivoRegistroUsuario() {
		return inactivoRegistroUsuario;
	}

	public void setInactivoRegistroUsuario(boolean inactivoRegistroUsuario) {
		this.inactivoRegistroUsuario = inactivoRegistroUsuario;
	}

	public String getCodVerificacion() {
		return codVerificacion;
	}

	public void setCodVerificacion(String codVerificacion) {
		this.codVerificacion = codVerificacion;
	}

	public boolean isInactivoCuentaConfirmada() {
		return inactivoCuentaConfirmada;
	}

	public void setInactivoCuentaConfirmada(boolean inactivoCuentaConfirmada) {
		this.inactivoCuentaConfirmada = inactivoCuentaConfirmada;
	}

	public boolean isRecuperaCuenta() {
		return recuperaCuenta;
	}

	public void setRecuperaCuenta(boolean recuperaCuenta) {
		this.recuperaCuenta = recuperaCuenta;
	}

	public String getCorreoRecuperaCuenta() {
		return correoRecuperaCuenta;
	}

	public void setCorreoRecuperaCuenta(String correoRecuperaCuenta) {
		this.correoRecuperaCuenta = correoRecuperaCuenta;
	}

	public String getNuevoPassword() {
		return nuevoPassword;
	}

	public void setNuevoPassword(String nuevoPassword) {
		this.nuevoPassword = nuevoPassword;
	}

	public boolean isEstadoNuevoPassword() {
		return estadoNuevoPassword;
	}

	public void setEstadoNuevoPassword(boolean estadoNuevoPassword) {
		this.estadoNuevoPassword = estadoNuevoPassword;
	}

	public String getPasswordActual() {
		return passwordActual;
	}

	public void setPasswordActual(String passwordActual) {
		this.passwordActual = passwordActual;
	}

}
