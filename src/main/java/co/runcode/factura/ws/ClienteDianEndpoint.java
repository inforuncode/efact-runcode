package co.runcode.factura.ws;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import co.gov.dian.servicios.facturaelectronica.reportarfactura.AcuseRecibo;
import co.gov.dian.servicios.facturaelectronica.reportarfactura.EnvioFacturaElectronica;
import co.gov.dian.servicios.facturaelectronica.reportarfactura.FacturaElectronicaPortName;
import co.gov.dian.servicios.facturaelectronica.reportarfactura.FacturaElectronicaPortNameService;
import co.runcode.factura.dm.jpa.RegistroFactura;
import co.runcode.factura.dm.jpa.RegistroNotaCredito;
import co.runcode.factura.dm.jpa.RegistroNotaDebito;
import co.runcode.factura.reportefactura.HeaderHandlerResolver;

@Path("/enviador")
public class ClienteDianEndpoint {
	
	@POST
	@Path("/factura")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response enviarFacturaDian(RegistroFactura regFac) throws DatatypeConfigurationException {
		               
        EnvioFacturaElectronica factura = new EnvioFacturaElectronica();

        DataSource fds = new FileDataSource( regFac.getArchivo() );
        DataHandler handler = new DataHandler(fds);

        factura.setDocument( handler );
        factura.setInvoiceNumber( regFac.getNumFac() );
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime( regFac.getFecFac() );
                
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        xgcal.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
        xgcal.setFractionalSecond( null );
        
        factura.setIssueDate(xgcal);
        factura.setNIT( regFac.getNitOFE() );
        
        try {
        	envioFacturaElectronica(factura);
        	regFac.setFecEnvio( new Date() );
        	regFac.setEstado("ENVIADO");
        	
        }catch (Exception e) {
        	e.printStackTrace();
        	return  Response.ok("ERROR AL ENVIAR FACTURA").build();
		}
        
        return  Response.ok(regFac).build();
        
    }
	
	@POST
	@Path("/notacredito")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response enviarNotaCreditoDian(RegistroNotaCredito regNc) throws DatatypeConfigurationException {
		               
        EnvioFacturaElectronica documento = new EnvioFacturaElectronica();
        
        DataSource fds = new FileDataSource( regNc.getArchivo() );
        DataHandler handler = new DataHandler(fds);

        documento.setDocument( handler );
        documento.setInvoiceNumber( regNc.getNumFac() ); // TODO Duda no se si enviar el número de la factura o NC
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime( regNc.getFecNc() );
                
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        xgcal.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
        xgcal.setFractionalSecond( null );
        
        documento.setIssueDate(xgcal);
        documento.setNIT( regNc.getNitOFE() );
        
        try {
        	envioFacturaElectronica(documento);
        	regNc.setFecEnvio( new Date() );
        	regNc.setEstado("ENVIADO");
        
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return  Response.ok("ERROR AL ENVIAR NOTA CREDITO").build();
		}
        
        return  Response.ok(regNc).build();
        
    }
    	
	@POST
	@Path("/notadebito")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response enviarNotaCreditoDian(RegistroNotaDebito regNd) throws DatatypeConfigurationException {
		               
        EnvioFacturaElectronica documento = new EnvioFacturaElectronica();
        
        DataSource fds = new FileDataSource( regNd.getArchivo() );
        DataHandler handler = new DataHandler(fds);

        documento.setDocument( handler );
        documento.setInvoiceNumber( regNd.getNumFac() ); // TODO Duda no se si enviar el número de la factura o ND
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime( regNd.getFecNd() );
                
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        xgcal.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
        xgcal.setFractionalSecond( null );
        
        documento.setIssueDate(xgcal);
        documento.setNIT( regNd.getNitOFE() );
        
        try {
        	envioFacturaElectronica(documento);
        	regNd.setFecEnvio( new Date() );
        	regNd.setEstado("ENVIADO");
        
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return  Response.ok("ERROR AL ENVIAR NOTA DEBITO").build();
		}
        
        return  Response.ok(regNd).build();
        
    }
    	
    private static AcuseRecibo envioFacturaElectronica(EnvioFacturaElectronica envioFacturaElectronicaPeticion) {

        try {

            HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();

            FacturaElectronicaPortNameService service = new FacturaElectronicaPortNameService();
            service.setHandlerResolver(handlerResolver);

            FacturaElectronicaPortName port = service.getFacturaElectronicaPortNameSoap11();

            /* Set NEW Endpoint Location */
            String endpointURL = "https://facturaelectronica.dian.gov.co/habilitacion/B2BIntegrationEngine/FacturaElectronica/facturaElectronica";
            // BindingProvider bp = (BindingProvider)port;
            // bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
            // endpointURL);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);

            return port.envioFacturaElectronica(envioFacturaElectronicaPeticion);
        } catch (Exception e) {
            System.out.println(" El error:  " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
}
