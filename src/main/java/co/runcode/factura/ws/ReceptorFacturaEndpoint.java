package co.runcode.factura.ws;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import co.gov.dian.contratos.facturaelectronica.v1.CreditNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.DebitNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.InvoiceType;
import co.runcode.commons.util.ArchivosUtil;
import co.runcode.factura.bo.DatosFacturadorBO;
import co.runcode.factura.bo.DatosProveedorBO;
import co.runcode.factura.bo.FirmadorDocumentosBO;
import co.runcode.factura.bo.GeneradorDocumentosUblBO;
import co.runcode.factura.bo.GeneradorRegistroDocumentosUblBO;
import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.factura.dm.jpa.DatosProveedor;
import co.runcode.factura.dm.jpa.RegistroFactura;
import co.runcode.factura.dm.jpa.RegistroNotaCredito;
import co.runcode.factura.dm.jpa.RegistroNotaDebito;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.NotaCredito;
import co.runcode.factura.dm.json.NotaDebito;

@Path("/receptor")
public class ReceptorFacturaEndpoint {
	
	private static final Logger logger = Logger.getLogger(ReceptorFacturaEndpoint.class.getName());

	@Inject
	private GeneradorDocumentosUblBO generadorDocumentosUblBO;
	
	@Inject
	private FirmadorDocumentosBO firmadorDocumentosBO;
	
	@Inject
	private GeneradorRegistroDocumentosUblBO generadorRegistroDocumentosUblBO;
	
	@Inject
	private DatosFacturadorBO datosFacturadorBO;
	
	@Inject
	private DatosProveedorBO datosProveedorBO;
	
	@Inject
	private ArchivosUtil archivosUtil;

	public ReceptorFacturaEndpoint() {
	}

	@GET
	public Response test() {
		return Response.ok("Prueba receptor OK!").build();
	}

	@POST
	@Path("/factura")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response registrarFactura(Factura f) {
		
		// Obtener datos facturador y proveedor
		DatosFacturador df = datosFacturadorBO.searchElementWithOneArg(
				DatosFacturador.class, "nit", f.getFacturador().getPersona().getIdentificacion());		
		logger.info(df.toString());

		DatosProveedor dp = datosProveedorBO.searchElementWithOneArg(
				DatosProveedor.class, "nit", f.getFacturador().getPersona().getIdentificacion());	
		
		logger.info("is null: " + dp); 
		logger.info(dp.toString());
				
		InvoiceType fe = null;
		RegistroFactura regFac = null;
	
		// Definir nombres de arhivo XML y ZIP		
		String prefijo = df.getPrefijo();
		String codFacStrg =  f.getId();
		
		int codFac;
		
		if ( prefijo != null && !prefijo.trim().equals("") ) {
			codFac = Integer.valueOf( f.getId().replace(prefijo, "") );
		} else {
			codFac = Integer.valueOf( f.getId() );
		}			
					
		int nit = Integer.valueOf( f.getFacturador().getPersona().getIdentificacion() );
		
		codFacStrg = String.format("%010x", codFac);
        String nitStrg = String.format("%010d", nit);
        
        String nombreCompletoArchivo = df.getFacturasRuta() + "/" + "face_f" + nitStrg + codFacStrg +".xml";	        
        String nombreCompletoZip = df.getFacturasRuta() + "/"  + "ws_f" + nitStrg + codFacStrg +".zip";

        // Generar factura electrónica (InvoiceType)
		try {			
			fe = generadorDocumentosUblBO.generarFacturaUbl(f, df, dp);

		} catch (DatatypeConfigurationException | JAXBException e) {
			e.printStackTrace();
			return Response.ok("Error al generar factura electronica").build();
			
		} 
		
		// Firmar factura electrónica		
		try {
			// File rutaRepositorioFacturas = new File( df.getFacturasRuta() );
			// rutaRepositorioFacturas.mkdirs();			
			firmadorDocumentosBO.firmarFactura(fe, nombreCompletoArchivo, df.getCertRuta(), df.getCertPwd());
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok("Error al firmar factura electronica").build();
			
		}
		
		// Comprimir factura
		try {
			archivosUtil.comprimirArchivo(nombreCompletoZip, nombreCompletoArchivo);
			
		} catch (IOException e) {
			e.printStackTrace();
			return Response.ok("Error al comprimir factura electronica").build();
		}
		
		regFac = generadorRegistroDocumentosUblBO.generarRegistroFactura(f, df.getClaveTecnica() );    		
		regFac.setArchivo( nombreCompletoZip );
		regFac.setFecRecibo( new Date() );
		regFac.setEstado("RECIBIDO");

		return Response.ok(regFac).build();
	}
	
	@POST
	@Path("/notacredito")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response registrarNotaCredito(NotaCredito nc) {
		
		// Obtener datos facturador y proveedor
		DatosFacturador df = datosFacturadorBO.searchElementWithOneArg(
				DatosFacturador.class, "nit", nc.getFacturador().getPersona().getIdentificacion());		
		logger.info(df.toString());

		DatosProveedor dp = datosProveedorBO.searchElementWithOneArg(
				DatosProveedor.class, "nit", nc.getFacturador().getPersona().getIdentificacion());	
				
		logger.info(dp.toString());
				
		CreditNoteType nce = null;
		RegistroNotaCredito regNc = null;
	
		// Definir nombres de arhivo XML y ZIP		
		String prefijo = df.getPrefijo();
		String codFacStrg =  nc.getId();
		
		int codFac;
		
		if ( prefijo != null && !prefijo.trim().equals("") ) {
			codFac = Integer.valueOf( nc.getId().replace(prefijo, "") );
		} else {
			codFac = Integer.valueOf( nc.getId() );
		}			
					
		int nit = Integer.valueOf( nc.getFacturador().getPersona().getIdentificacion() );
		
		codFacStrg = String.format("%010x", codFac);
        String nitStrg = String.format("%010d", nit);
        
        String nombreCompletoArchivo = df.getFacturasRuta() + "/" + "face_c" + nitStrg + codFacStrg +".xml";	        
        String nombreCompletoZip = df.getFacturasRuta() + "/"  + "ws_c" + nitStrg + codFacStrg +".zip";

        // Generar nota credito (CreditNoteType)
		try {			
			nce = generadorDocumentosUblBO.generarNotaCreditoUbl(nc, df, dp);

		} catch (DatatypeConfigurationException | JAXBException e) {
			e.printStackTrace();
			return Response.ok("Error al generar nota credito electronica").build();
			
		} 
		
		// Firmar nota credito		
		try {
			// File rutaRepositorioFacturas = new File( df.getFacturasRuta() );
			// rutaRepositorioFacturas.mkdirs();			
			firmadorDocumentosBO.firmarNotaCredito(nce, nombreCompletoArchivo, df.getCertRuta(), df.getCertPwd());
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok("Error al firmar nota credito electronica").build();
			
		}
		
		// Comprimir nota credito
		try {
			archivosUtil.comprimirArchivo(nombreCompletoZip, nombreCompletoArchivo);
			
		} catch (IOException e) {
			e.printStackTrace();
			return Response.ok("Error al comprimir nota credito electronica").build();
		}
		
		regNc = generadorRegistroDocumentosUblBO.generarRegistroNotaCredito(nc);    		
		regNc.setArchivo( nombreCompletoZip );
		regNc.setFecRecibo( new Date() );
		regNc.setEstado("RECIBIDO");

		return Response.ok(regNc).build();
	}

	@POST
	@Path("/notadebito")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response registrarNotaDebito(NotaDebito nd) {
		
		// Obtener datos facturador y proveedor
		DatosFacturador df = datosFacturadorBO.searchElementWithOneArg(
				DatosFacturador.class, "nit", nd.getFacturador().getPersona().getIdentificacion());		
		logger.info(df.toString());

		DatosProveedor dp = datosProveedorBO.searchElementWithOneArg(
				DatosProveedor.class, "nit", nd.getFacturador().getPersona().getIdentificacion());	
				
		logger.info(dp.toString());
				
		DebitNoteType nde = null;
		RegistroNotaDebito regNd = null;
	
		// Definir nombres de arhivo XML y ZIP		
		String prefijo = df.getPrefijo();
		String codFacStrg =  nd.getId();
		
		int codFac;
		
		if ( prefijo != null && !prefijo.trim().equals("") ) {
			codFac = Integer.valueOf( nd.getId().replace(prefijo, "") );
		} else {
			codFac = Integer.valueOf( nd.getId() );
		}			
					
		int nit = Integer.valueOf( nd.getFacturador().getPersona().getIdentificacion() );
		
		codFacStrg = String.format("%010x", codFac);
        String nitStrg = String.format("%010d", nit);
        
        String nombreCompletoArchivo = df.getFacturasRuta() + "/" + "face_d" + nitStrg + codFacStrg +".xml";	        
        String nombreCompletoZip = df.getFacturasRuta() + "/"  + "ws_d" + nitStrg + codFacStrg +".zip";

        // Generar nota débito (CreditNoteType)
		try {			
			nde = generadorDocumentosUblBO.generarNotaDebitoUbl(nd, df, dp);

		} catch (DatatypeConfigurationException | JAXBException e) {
			e.printStackTrace();
			return Response.ok("Error al generar nota débito electronica").build();
			
		} 
		
		// Firmar nota débito		
		try {
			// File rutaRepositorioFacturas = new File( df.getFacturasRuta() );
			// rutaRepositorioFacturas.mkdirs();			
			firmadorDocumentosBO.firmarNotaDebito(nde, nombreCompletoArchivo, df.getCertRuta(), df.getCertPwd());
			
		} catch (Exception e) {
			e.printStackTrace();
			return Response.ok("Error al firmar nota débito electronica").build();
			
		}
		
		// Comprimir nota débito
		try {
			archivosUtil.comprimirArchivo(nombreCompletoZip, nombreCompletoArchivo);
			
		} catch (IOException e) {
			e.printStackTrace();
			return Response.ok("Error al comprimir nota débito electronica").build();
		}
		
		regNd = generadorRegistroDocumentosUblBO.generarRegistroNotaDebito(nd);    		
		regNd.setArchivo( nombreCompletoZip );
		regNd.setFecRecibo( new Date() );
		regNd.setEstado("RECIBIDO");

		return Response.ok(regNd).build();
	}	
	
	
	@POST
	@Path("/factura-sin-firma")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML})
	public Response registrarFacturaSinFirma(Factura f) {
		DatosFacturador df = (DatosFacturador) datosFacturadorBO.searchElementWithOneArg(DatosFacturador.class, "nit",
				f.getFacturador().getPersona().getIdentificacion());
		logger.info(df.toString());

		DatosProveedor dp = (DatosProveedor) datosProveedorBO.searchElementWithOneArg(DatosProveedor.class, "nit",
				f.getFacturador().getPersona().getIdentificacion());
		logger.info(df.toString());

		InvoiceType fe = null;
		try {
			fe = generadorDocumentosUblBO.generarFacturaUbl(f, df, dp);

		} catch (DatatypeConfigurationException | JAXBException e) {
			e.printStackTrace();
			Optional.empty();

			return Response.ok("Error al generar factura electronica").build();
		}

		return Response.ok(fe).build();
	}
	
}