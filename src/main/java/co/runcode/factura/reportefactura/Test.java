package co.runcode.factura.reportefactura;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import co.gov.dian.servicios.facturaelectronica.reportarfactura.AcuseRecibo;
import co.gov.dian.servicios.facturaelectronica.reportarfactura.EnvioFacturaElectronica;
import co.gov.dian.servicios.facturaelectronica.reportarfactura.FacturaElectronicaPortName;
import co.gov.dian.servicios.facturaelectronica.reportarfactura.FacturaElectronicaPortNameService;

public class Test {
    
    private static final String RUTA_FATURAS = "D:\\eebp\\facturas\\";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws DatatypeConfigurationException {
                
        int nit = 846000553;
        int codFactura = 980000015;
        String prefijo = "PRUE";
        GregorianCalendar fecFac = new GregorianCalendar(2018, Calendar.SEPTEMBER, 27, 00, 00, 55);

        // ======================================================================================
        String codFacturaStrg = String.format("%010x", codFactura);
        String nitStrg = String.format("%010d", nit);
                
        String nombreCompletoZip = RUTA_FATURAS + "ws_f" + nitStrg + codFacturaStrg +".zip";
                               
        EnvioFacturaElectronica factura = new EnvioFacturaElectronica();
        DataSource fds = new FileDataSource(nombreCompletoZip);
        DataHandler handler = new DataHandler(fds);

        factura.setDocument(handler);
        factura.setInvoiceNumber(prefijo + codFactura);
                    
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(fecFac);
        xgcal.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
        xgcal.setFractionalSecond( null );
        
        factura.setIssueDate(xgcal);
        factura.setNIT("" + nit);
        
        envioFacturaElectronica(factura);
        
    }

    private static AcuseRecibo envioFacturaElectronica(EnvioFacturaElectronica envioFacturaElectronicaPeticion) {

        try {

            HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();

            FacturaElectronicaPortNameService service = new FacturaElectronicaPortNameService();
            service.setHandlerResolver(handlerResolver);

            FacturaElectronicaPortName port = service.getFacturaElectronicaPortNameSoap11();

            /* Set NEW Endpoint Location */
            String endpointURL = "https://facturaelectronica.dian.gov.co/habilitacion/B2BIntegrationEngine/FacturaElectronica/facturaElectronica";
            // BindingProvider bp = (BindingProvider)port;
            // bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
            // endpointURL);
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);

            return port.envioFacturaElectronica(envioFacturaElectronicaPeticion);
        } catch (Exception e) {
            System.out.println(" El error:  " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
}
