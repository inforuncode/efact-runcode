/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.runcode.factura.reportefactura;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author www.javadb.com
 */
public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {

    private static SecureRandom secureRandom;

    public HeaderHandler() {
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
            // to properly initialize it needs to be used once
            final byte[] ar = new byte[64];
            secureRandom.nextBytes(ar);
            Arrays.fill(ar, (byte) 0);
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean handleMessage(SOAPMessageContext smc) {

        SOAPHelper printSoap = new SOAPHelper();

        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (outboundProperty.booleanValue()) {

            SOAPMessage message = smc.getMessage();
            System.out.println("  =============== ORIGINAL");
            System.out.println(printSoap.getSOAPMessageAsString(message));
            System.out.println("\n\n");
            try {

                // message.getSOAPPart().getEnvelope().setAttributeNS("http://www.w3.org/2000/xmlns/",
                // "xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
                // message.getSOAPPart().getEnvelope().removeAttributeNS("http://schemas.xmlsoap.org/soap/envelope/",
                // "SOAP-ENV");
                // message.getSOAPPart().getEnvelope().removeAttribute("xmlns:env");
                message.getSOAPPart().getEnvelope().getHeader().detachNode();
                message.getSOAPBody().setPrefix("soapenv");

                SOAPEnvelope envelope = smc.getMessage().getSOAPPart().getEnvelope();
                envelope.setPrefix("soapenv");

                envelope.removeAttribute(new QName("xmlns:S"));
                envelope.removeAttribute(new QName("xmlns:SOAP-ENV"));

                SOAPHeader header = envelope.addHeader();

                SOAPElement security = header.addChildElement("Security", "wsse",
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                security.addAttribute(new QName("xmlns:wsu"),
                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
                security.addAttribute(envelope.createName("S:mustUnderstand"), "0");

                SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
                SOAPElement username = usernameToken.addChildElement("Username", "wsse");
                username.addTextNode("b73ec1ef-b215-4405-8bdd-bfd3f08bcc5a");

                SOAPElement password = usernameToken.addChildElement("Password", "wsse");
                password.addTextNode("05ad3ed7b0ce75f807558dc265652932566f46de890e055566f66671a5fd99b9");

                SOAPElement nonce = usernameToken.addChildElement("Nonce", "wsse");
                nonce.addTextNode("ajNUCBiQRnEwku24W9Su6-UFX3b6PcRXP5EkWrYGhQh_XHjKWX56lRw_UArpsJUI");

                Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                String s = formatter.format(new Date());

                SOAPElement created = usernameToken.addChildElement("Created", "wsu");
                created.addTextNode(s);

                // ============================
                // ============================
                // Print out the outbound SOAP message to System.out
                System.out.println("  =============== MODIFICADO");
                System.out.println(printSoap.getSOAPMessageAsString(message));
                System.out.println("\n\n");

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            try {

                // This handler does nothing with the response from the Web Service so
                // we just print out the SOAP message.
                SOAPMessage message = smc.getMessage();
                System.out.println("  =============== respuesta");
                System.out.println(printSoap.getSOAPMessageAsString(message));
                System.out.println("\n");

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return outboundProperty;

    }

    public Set getHeaders() {
        // throw new UnsupportedOperationException("Not supported yet.");
        final QName securityHeader = new QName(
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse");

        final QName securityHeader2 = new QName(
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd", "Security", "wsu");

        final HashSet headers = new HashSet();
        headers.add(securityHeader);
        headers.add(securityHeader2);
        return headers;
    }

    public boolean handleFault(SOAPMessageContext context) {
        // throw new UnsupportedOperationException("Not supported yet.");
        return true;
    }

    public void close(MessageContext context) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

}
