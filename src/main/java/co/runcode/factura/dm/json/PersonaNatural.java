package co.runcode.factura.dm.json;


public class PersonaNatural extends Persona {

	private String nombres;
	private String primerApellido;
	private String segundoApellido;
	
	public PersonaNatural(){
		
	}	
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	
	

}
