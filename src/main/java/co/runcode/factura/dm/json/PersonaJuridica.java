package co.runcode.factura.dm.json;


public class PersonaJuridica extends Persona {
	
	private String razonSocial;
	
	public PersonaJuridica(){
		
	}
	
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

}
