package co.runcode.factura.dm.json;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME,
include = JsonTypeInfo.As.PROPERTY,
property = "tipoPersona")

@JsonSubTypes({
    @Type(value = PersonaNatural.class, name = "PERSONA_NATURAL"),
    @Type(value = PersonaJuridica.class, name = "PERSONA_JURIDICA"),
})
public abstract class Persona {
	
	public static final String SCHEMA_AGENCY_ID = "195";
	public static final String SCHEMA_AGENCY_NAME = "CO, DIAN (Dirección de Impuestos y Aduanas)";
		
	private String identificacion;
	private String tipoIdentificacion;
	private String tipoRegimen;
	private Direccion direccion;
	
	public Persona() { }
	
	public String getIdentificacion() {
		return identificacion;
	}
	
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public String getTipoRegimen() {
		return tipoRegimen;
	}

	public void setTipoRegimen(String tipoRegimen) {
		this.tipoRegimen = tipoRegimen;
	}

	public Direccion getDireccion() {
		return direccion;
	}
	
	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	} 
	
}
