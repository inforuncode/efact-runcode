package co.runcode.factura.dm.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NotaDebito {

	private String id;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fechaHoraEmision;
	private List<String> notas;
	private String moneda;	
		
	//cac:InvoicePeriod	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fechaHoraInicio;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fechaHoraFin;	
	
	/**
	 * /fe:CreditNote/cac:DiscrepancyResponse/cbc:ResponseCode
	 * Ver Resolución 000019 DIAN. Anexo 1. Sección 3.2-Concepto de Notas crédito
	 * 
	 */
	private String concepto;
	
	
	// Datos factura de referencia
	/**
	 * /fe:CreditNote/cac:BillingReference/cac:InvoiceDocumentReference/cbc:ID
	 * Número de la factura a afectar
	 */
	private String numeroFactura;
	
	/**
	 * /fe:CreditNote/cac:BillingReference/cac:InvoiceDocumentReference/cbc:UUID
	 * CUFE de la factura de venta || factura de exportación
	 * No aplica si es factura de contingencia
	 */
	private String cufeFactura;
	
	/**
	 * /fe:CreditNote/cac:BillingReference/cac:InvoiceDocumentReference/cbc:IssueDate
	 * Fecha de la factura
	 */
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Bogota")
	private Date fechaFactura;	
	
	
	@NotNull
	@JsonProperty(required=true)
	private Facturador facturador;
	
	@NotNull
	@JsonProperty(required=true)
	private Adquiriente adquiriente;

	private List<ItemND> listaItems = new ArrayList<>();
	
	/**
	 * MonetaryTotalType - cbc:LineExtensionAmount
	 * 9.4 - Total Importe bruto antes de impuestos: 
	 *       Total importe bruto, suma de los importes 
	 *       brutos de las líneas de la factura.
	 */
	private BigDecimal totalImporteBruto;
	
	/**
	 * MonetaryTotalType - cbc:TaxExclusiveAmount 
	 * 
	 * 9.5 - Total Base Imponible (Importe Bruto+Cargos-Descuentos): 
	 *       Base imponible para el cálculo de los impuestos.
	 */
	private BigDecimal totalBaseImponible;
	
	/**
	 * MonetaryTotalType - cbc:PayableAmount
	 * 
	 * 9.8 - Total de Factura: Total importe bruto + 
	 *       Total Impuestos - Total Impuesto Retenidos
	 */
	private BigDecimal total;
	
	/**
	 * [Opcional]
	 * MonetaryTotalType - cbc:AllowanceTotalAmount
	 * 
	 * 9.1 - Total de Descuentos: Suma de todos los
	 *		 descuentos presentes
	 * 
	 */
	private BigDecimal totalDescuentos;
	
	/**
	 * [Opcional]
	 * MonetaryTotalType - cbc:ChargeTotalAmount
	 * 
	 * 9.2 - Total de Cargos: Suma de todos los cargos
	 *		 presentes
	 * 
	 */
	private BigDecimal totalCargos;
	
	/**
	 * [Opcional]
	 * MonetaryTotalType - cbc:PrepaidAmount
	 * 
	 * 9.3 - Total de Anticipos: Suma de todos los
	 *		 anticipos presentes
	 * 
	 */
	private BigDecimal totalAnticipos;
	
	
	private List<Impuesto> totalImpuestos = new ArrayList<>();

	// Getters & setters...
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getFechaHoraEmision() {
		return fechaHoraEmision;
	}

	public void setFechaHoraEmision(Date fechaHoraEmision) {
		this.fechaHoraEmision = fechaHoraEmision;
	}

	public List<String> getNotas() {
		return notas;
	}

	public void setNotas(List<String> notas) {
		this.notas = notas;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}

	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}

	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getCufeFactura() {
		return cufeFactura;
	}

	public void setCufeFactura(String cufeFactura) {
		this.cufeFactura = cufeFactura;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public Facturador getFacturador() {
		return facturador;
	}

	public void setFacturador(Facturador facturador) {
		this.facturador = facturador;
	}

	public Adquiriente getAdquiriente() {
		return adquiriente;
	}

	public void setAdquiriente(Adquiriente adquiriente) {
		this.adquiriente = adquiriente;
	}

	public List<ItemND> getListaItems() {
		return listaItems;
	}

	public void setListaItems(List<ItemND> listaItems) {
		this.listaItems = listaItems;
	}

	public BigDecimal getTotalImporteBruto() {
		return totalImporteBruto;
	}

	public void setTotalImporteBruto(BigDecimal totalImporteBruto) {
		this.totalImporteBruto = totalImporteBruto;
	}


	public BigDecimal getTotalBaseImponible() {
		return totalBaseImponible;
	}

	public void setTotalBaseImponible(BigDecimal totalBaseImponible) {
		this.totalBaseImponible = totalBaseImponible;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalDescuentos() {
		return totalDescuentos;
	}

	public void setTotalDescuentos(BigDecimal totalDescuentos) {
		this.totalDescuentos = totalDescuentos;
	}

	public BigDecimal getTotalCargos() {
		return totalCargos;
	}

	public void setTotalCargos(BigDecimal totalCargos) {
		this.totalCargos = totalCargos;
	}

	public BigDecimal getTotalAnticipos() {
		return totalAnticipos;
	}

	public void setTotalAnticipos(BigDecimal totalAnticipos) {
		this.totalAnticipos = totalAnticipos;
	}

	public List<Impuesto> getTotalImpuestos() {
		return totalImpuestos;
	}

	public void setTotalImpuestos(List<Impuesto> totalImpuestos) {
		this.totalImpuestos = totalImpuestos;
	}
	
}
