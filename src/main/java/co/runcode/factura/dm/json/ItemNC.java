package co.runcode.factura.dm.json;

import java.math.BigDecimal;

/**
 * Clase que mapea concepto /fe:CreditNote/cac:CreditNoteLine
 * en Notas de crédito
 */
public class ItemNC {
	
	/**
	 * /fe:CreditNote/cac:CreditNoteLine/cbc:ID
	 * 
	 */
	private String numeroLinea;
	
	/**
	 * /fe:CreditNote/cac:CreditNoteLine/cbc:LineExtensionAmount
	 * 
	 */
	private BigDecimal costoItem;
	
	/**
	 * /fe:CreditNote/cac:CreditNoteLine/cac:Item/cbc:Description
	 * 
	 */
	private String descripcion;	

	// Getters & setters...
	public String getNumeroLinea() {
		return numeroLinea;
	}

	public void setNumeroLinea(String numeroLinea) {
		this.numeroLinea = numeroLinea;
	}

	public BigDecimal getCostoItem() {
		return costoItem;
	}

	public void setCostoItem(BigDecimal costo) {
		this.costoItem = costo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
