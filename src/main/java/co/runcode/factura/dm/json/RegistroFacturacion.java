package co.runcode.factura.dm.json;


import java.util.ArrayList;
import java.util.List;

import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.factura.dm.jpa.DatosProveedor;


public class RegistroFacturacion {
	
	private DatosProveedor datosProveedor;
	private DatosFacturador datosFacturador;
	
	private List<Factura> facturas = new ArrayList<>();
	
	public RegistroFacturacion() {
				
	}

	public DatosProveedor getDatosProveedor() {
		return datosProveedor;
	}

	public void setDatosProveedor(DatosProveedor datosProveedor) {
		this.datosProveedor = datosProveedor;
	}
	
	public DatosFacturador getDatosFacturador() {
		return datosFacturador;
	}

	public void setDatosFacturador(DatosFacturador datosFacturador) {
		this.datosFacturador = datosFacturador;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}

}