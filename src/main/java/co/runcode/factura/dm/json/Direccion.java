package co.runcode.factura.dm.json;


public class Direccion {
	
	/**
	 * 2.2.6.8 - Sub-División del País: Nombre de la
	 * 			 Subdivisión territorial de un país Estado,
	 * 			 Departamento.
	 */	
	private String departamento;
	
	/**
	 * 2.2.6.10 - Municipio: Nombre de la ciudad,
	 * 			  municipio, etc.
	 */
	private String municipio;
	
	/**
	 * 2.2.6.11 - Sub-División Municipio: Subdivisión
	 * 			  de una ciudad, municipio, etc., por ejemplo un
	 *       	  distrito administrativo o similar.
	 */
	private String centroPoblado;
	

	/**
	 * 2.2.6.3 - Dirección: Texto Libre para establecer
						una dirección. No se recomienda detallar en
						otros campos más detallados conceptos como
						número de edificio, letra, escalera, etc. Mejor
						en una línea libre porque la diversidad de datos
						es muy grande. / 2.2.6.4 - Dirección 2: Texto
						Libre para establecer una dirección (extensión)
						/ 2.2.6.5 - Dirección 3: Texto Libre para
						establecer una dirección (extensión)
	 */	
	private String direccion;
	
	/**
	 * 2.2.6.6 - Código País: Código del País relativo
	 *			 a la dirección, se recomienda establecer como
	 *			 Obligatorio si es distinto del nacional
	 *			(Colombia). Utilizar una lista de códigos
	 *			 externa y estandarizada, como la iso ISO 3166-1
	 *			 alfa2 o alfa3. / 2.2.6.7 - Nombre del País:
	 *			 Nombre del país relativo a la dirección.
	 */
	private String codigoPais;
	
	/**
	 * 2.2.6.12 - Código Postal: Código Postal
	 */
	private String codigoPostal;
	
	/**
	 * cbc:Postbox
	 * 
	 * 2.2.6.2 - Identificador postal: Identificador
	 *  		 Postal, Apartado postal o aéreo, PO Box
	 */
	private String identificadorPostal;

	
	public Direccion() {
		
	}
	
	
	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCentroPoblado() {
		return centroPoblado;
	}

	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPais() {
		return codigoPais;
	}

	public void setCodigoPais(String pais) {
		this.codigoPais = pais;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getIdentificadorPostal() {
		return identificadorPostal;
	}

	public void setIdentificadorPostal(String identificadorPostal) {
		this.identificadorPostal = identificadorPostal;
	}
	

}
