package co.runcode.factura.dm.json;


public class Adquiriente {
	
	private Persona persona;	
	
	public Adquiriente() { }
		
	public Adquiriente(Persona persona) {
		this.persona = persona;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
}
