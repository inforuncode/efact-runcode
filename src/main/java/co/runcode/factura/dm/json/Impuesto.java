package co.runcode.factura.dm.json;


import java.math.BigDecimal;

public class Impuesto {
	
	
	// <complexType name="TaxTotalType">	
	/**
	 * cbc:TaxAmount
	 * 
	 * 7.1.1.4 - Importe Impuesto: Importe del impuesto
	 * retenido
	 */
	// private Float importeImpuesto;
	
	/**
	 * cbc:TaxEvidenceIndicator
	 * 
	 * Indica que el elemento es un Impuesto retenido
	 * (7.1.1) y no un impuesto (8.1.1)
	 */
	private Boolean impuestoRetenido;
	
	
	// <complexType name="TaxSubtotalType">
	
	/**
	 * cbc:TaxableAmount
	 * 
	 * 7.1.1.1 / 8.1.1.1 - Base Imponible: Base
	 * Imponible sobre la que se calcula la retención
	 * de impuesto
	 * 
	 * (Valor sin impuestos)
	 */
	private BigDecimal baseImponible;
	
	/**
	 * cbc:TaxAmount
	 * 
	 * 7.1.1.4 / 8.1.1.4 - Importe Impuesto (detalle):
	 * Importe del impuesto retenido
	 * 
	 * (Valor impuestos)
	 */
	private BigDecimal importeImpuesto;
	
	/**
	 * cbc:Percent
	 * 
	 * 7.1.1.3 / 8.1.1.3 - Porcentaje: Porcentaje a
	 * aplicar
	 */
	private BigDecimal porcentaje;
	
	/**
	 * cac:TaxCategory
	 * 
	 * 7.1.1.2 - Tipo: Tipo o clase impuesto. Concepto
	 * fiscal por el que se tributa. Debería si un
	 * campo que referencia a una lista de códigos. En
	 * la lista deberían aparecer los impuestos
	 * estatales o nacionales
	 */
	private String categoria;

	
	public Impuesto() {
		
	}	
	
	public Boolean getImpuestoRetenido() {
		return impuestoRetenido;
	}

	public void setImpuestoRetenido(Boolean impuestoRetenido) {
		this.impuestoRetenido = impuestoRetenido;
	}

	public BigDecimal getBaseImponible() {
		return baseImponible;
	}

	public void setBaseImponible(BigDecimal baseImponible) {
		this.baseImponible = baseImponible;
	}

	public BigDecimal getImporteImpuesto() {
		return importeImpuesto;
	}

	public void setImporteImpuesto(BigDecimal importeImpuesto) {
		this.importeImpuesto = importeImpuesto;
	}

	public BigDecimal getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	
}
