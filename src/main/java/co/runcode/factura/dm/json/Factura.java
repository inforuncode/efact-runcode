package co.runcode.factura.dm.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Factura {
	
	/**	
	 * cbc:ID 
	 * 
	 * 1.1 Número de documento: Número de factura o factura cambiaria. 
	 * Incluye prefijo + consecutivo de factura. 
	 * No se permiten caracteres adicionales como espacios o guiones.
	 */
	private String id;
	
	/**
	 * cbc:UUID
	 *  
	 * 1.10 - CUFE: Obligatorio si es factura nacional.
	 * Codigo Unico de Facturacion Electronica. Campo
	 * que verifica la integridad de la información
	 * recibida, es un campo generado por el sistema
	 * Numeración de facturación, está pendiente
	 * definir su contenido. Esta Encriptado. La
	 * factura electrónica tiene una firma electrónica
	 * basada en firma digital según la política de
	 * firma propuesta. La integridad de la factura ya
	 * viene asegurada por la firma digital, no es
	 * necesaria ninguna medida más para asegurar que
	 * ningún campo ha sido alterado.
	 */
	private String cufe;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fechaHoraFactura;
	private String tipoFactura;
	private List<String> notas;
	private String moneda;
	
	// FIXME: Pasar tipos Date a Calendar
	//cac:InvoicePeriod
	private Date fechaHoraInicio;
	private Date fechaHoraFin;
		
	@JsonProperty(required=true)
	private Facturador facturador;
	
	@JsonProperty(required=true)
	private Adquiriente adquiriente;
	
	private List<Item> listaItems;	
	private List<Impuesto> totalImpuestos;
	
	/**
	 * MonetaryTotalType - cbc:LineExtensionAmount
	 * 9.4 - Total Importe bruto antes de impuestos: 
	 *       Total importe bruto, suma de los importes 
	 *       brutos de las líneas de la factura.
	 */
	private BigDecimal totalImporteBruto;
	
	/**
	 * MonetaryTotalType - cbc:TaxExclusiveAmount 
	 * 
	 * 9.5 - Total Base Imponible (Importe Bruto+Cargos-Descuentos): 
	 *       Base imponible para el cálculo de los impuestos.
	 */
	private BigDecimal totalBaseImponible;
	
	/**
	 * MonetaryTotalType - cbc:PayableAmount
	 * 
	 * 9.8 - Total de Factura: Total importe bruto + 
	 *       Total Impuestos - Total Impuesto Retenidos
	 */
	private BigDecimal totalFactura;
	
	/**
	 * [Opcional]
	 * MonetaryTotalType - cbc:AllowanceTotalAmount
	 * 
	 * 9.1 - Total de Descuentos: Suma de todos los
	 *		 descuentos presentes
	 * 
	 */
	private BigDecimal totalDescuentos;
	
	/**
	 * [Opcional]
	 * MonetaryTotalType - cbc:ChargeTotalAmount
	 * 
	 * 9.2 - Total de Cargos: Suma de todos los cargos
	 *		 presentes
	 * 
	 */
	private BigDecimal totalCargos;
	
	/**
	 * [Opcional]
	 * MonetaryTotalType - cbc:PrepaidAmount
	 * 
	 * 9.3 - Total de Anticipos: Suma de todos los
	 *		 anticipos presentes
	 * 
	 */
	private BigDecimal totalAnticipos;
	
	
	public Factura() {
		listaItems = new ArrayList<>();
		totalImpuestos = new ArrayList<>();
		adquiriente = null;
		facturador = null;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCufe() {
		return cufe;
	}

	public void setCufe(String cufe) {
		this.cufe = cufe;
	}

	public Date getFechaHoraFactura() {
		return fechaHoraFactura;
	}

	public void setFechaHoraFactura(Date fechaHoraFactura) {
		this.fechaHoraFactura = fechaHoraFactura;
	}

	public String getTipoFactura() {
		return tipoFactura;
	}

	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}

	public List<String> getNotas() {
		return notas;
	}

	public void setNotas(List<String> notas) {
		this.notas = notas;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Date getFechaHoraInicio() {
		return fechaHoraInicio;
	}

	public void setFechaHoraInicio(Date fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	public Date getFechaHoraFin() {
		return fechaHoraFin;
	}

	public void setFechaHoraFin(Date fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

	public Facturador getFacturador() {
		return facturador;
	}

	public void setFacturador(Facturador facturador) {
		this.facturador = facturador;
	}

	public Adquiriente getAdquiriente() {
		return adquiriente;
	}

	public void setAdquiriente(Adquiriente adquiriente) {
		this.adquiriente = adquiriente;
	}

	public List<Item> getListaItems() {
		return listaItems;
	}

	public void setListaItems(List<Item> listaItems) {
		this.listaItems = listaItems;
	}

	public List<Impuesto> getTotalImpuestos() {		
		totalImpuestos = (totalImpuestos == null) ?  new ArrayList<Impuesto>() : totalImpuestos;		
		return totalImpuestos;
	}

	public void setTotalImpuestos(List<Impuesto> totalImpuestos) {
		this.totalImpuestos = totalImpuestos;
	}

	public BigDecimal getTotalImporteBruto() {
		return totalImporteBruto;
	}

	public void setTotalImporteBruto(BigDecimal totalImporteBruto) {
		this.totalImporteBruto = totalImporteBruto;
	}

	public BigDecimal getTotalBaseImponible() {
		return totalBaseImponible;
	}

	public void setTotalBaseImponible(BigDecimal totalBaseImponible) {
		this.totalBaseImponible = totalBaseImponible;
	}

	public BigDecimal getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(BigDecimal totalFactura) {
		this.totalFactura = totalFactura;
	}

	public BigDecimal getTotalDescuentos() {
		return totalDescuentos;
	}

	public void setTotalDescuentos(BigDecimal totalDescuentos) {
		this.totalDescuentos = totalDescuentos;
	}

	public BigDecimal getTotalCargos() {
		return totalCargos;
	}

	public void setTotalCargos(BigDecimal totalCargos) {
		this.totalCargos = totalCargos;
	}

	public BigDecimal getTotalAnticipos() {
		return totalAnticipos;
	}

	public void setTotalAnticipos(BigDecimal totalAnticipos) {
		this.totalAnticipos = totalAnticipos;
	}

}