package co.runcode.factura.dm.json;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class Item {
	
	
// Propiedades <complexType name="InvoiceLineType">		
	/**
	 * cbc:ID
	 * 
	 * Número de Línea
	 */
	private String numeroLinea;
	
	/**
	 * cbc:Note
	 * 
	 * 13.1.1.16 - Información Adicional: Texto libre
	 * para añadir información adicional al artículo.
	 */
	private String nota;
	
	/**
	 * cbc:InvoicedQuantity
	 * 
	 * 13.1.1.9 - Cantidad: Cantidad del artículo
	 * solicitado. Número de unidades
	 * servidas/prestadas.
	 */
	
	private BigDecimal cantidad;
	
	/**
	 * cbc:LineExtensionAmount
	 * 
	 * 13.1.1.12 - Costo Total: Coste Total. Resultado:
	 * Unidad de Medida x Precio Unidad.
	 */
	private BigDecimal costoTotal;
	
	
	// cac:OrderLineReference
	//
	// 13.1.1.2 - Número de Línea de Pedido: Número de
	// la Línea de Pedido al que hace pertenece el
	// artículo que corresponde a la línea de factura
	// 
	//private String numeroLineaPedido;
	
	/**
	 * tns:Price
	 * 
	 * Estructura necesaria para describir los precios
	 * de artículo o servicio
	 */
	private BigDecimal precioUnitario;
	
// Propiedades <complexType name="ItemType">
	
	/**
	 * cbc:Description
	 * 
	 * 13.1.1.3 - Descripción: Descripción del artículo
	 * que pertenece a la línea de la factura
	 */
	private String descripcion;
		
	/**
	 * Marca del artículo, obligatorio si la factura es internacional.
	 */
	private String marca;
	
	/**
	 * Modelo del artículo, obligatorio si la factura es internacional.
	 */
	private String modelo; 
	
	/**
	 * Identificador de artículo: Código
	 * identificador del artículo, por ejemplo el GTIN
	 * (Global Trade Item Number). Este campo debería
	 * referenciar a una lista de códigos. Se admite
	 * varias ocurrencias, por si se quieren incluir
	 * varias codificaciones. Ver composición en la
	 * estructura común Identificador (REF-ID). Valores
	 * Ejemplo: • GTIN UCC(Uniform Commercial Code)-12
	 * dígitos • GTIN EAN(European Article
	 * Number)/UCC-13 dígitos • GTIN EAN/UCC-14 dígitos
	 * • GTIN EAN/UCC-8 dígitos • Otros
	 */
	private String identificacionEstandar;
	
	/**
	 * Opcional no usado por la DIAN, las partes pueden
	 * definir un significado o simplemente omitirlo.
	 * 
	 * Se pretende usar como Identificado de Catalogo 
	 * para referenciar la identificación interna del 
	 * producto en el sistema del facturador. 
	 */
	private String identificacionCatalogo;
	
	/**
	 * Estado de la Mercancía: Estado de la
	 * mercancía, podría ser una referencia a una lista
	 * de códigos o un texto libre. Obligatorio si el
	 * estado es distinto a nueva
	 */
	private String identificacionAdicional;
	
	/**
	 * 13.1.1.14- Periodo Servicio: Información sobre
	 * el periodo de prestación de un servicio y
	 * 
	 * 13.1.1.15 - Fecha Transacción: Fecha concreta de
	 * prestación del servicio o entrega del bien.
	 * 
	 * Se utiliza igualmente, para agregar propiedades 
	 * adicionales al Item. P. ej.: ancho, alto, color, etc.
	 */
	private List<ItemPropiedad> propiedadAdicional;

	
	public Item() {
		super();		
	}

	
	public String getNumeroLinea() {
		return numeroLinea;
	}

	public void setNumeroLinea(String numeroLinea) {
		this.numeroLinea = numeroLinea;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(BigDecimal costoTotal) {
		this.costoTotal = costoTotal;
	}

	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}


	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getIdentificacionEstandar() {
		return identificacionEstandar;
	}

	public void setIdentificacionEstandar(String identificacionEstandar) {
		this.identificacionEstandar = identificacionEstandar;
	}

	public String getIdentificacionCatalogo() {
		return identificacionCatalogo;
	}

	public void setIdentificacionCatalogo(String identificacionCatalogo) {
		this.identificacionCatalogo = identificacionCatalogo;
	}

	public String getIdentificacionAdicional() {
		return identificacionAdicional;
	}

	public void setIdentificacionAdicional(String identificacionAdicional) {
		this.identificacionAdicional = identificacionAdicional;
	}

	public List<ItemPropiedad> getPropiedadAdicional() {
		
		if(propiedadAdicional == null) {
			propiedadAdicional = new ArrayList<>();
			System.out.println("Iniciar propiedad adicional");
		}
				
		return propiedadAdicional;
	}

	public void setPropiedadAdicional(List<ItemPropiedad> propiedadAdicional) {
		this.propiedadAdicional = propiedadAdicional;
	}

}
