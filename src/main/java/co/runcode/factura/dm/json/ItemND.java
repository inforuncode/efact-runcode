package co.runcode.factura.dm.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ItemND {

	/**
	 * /fe:DebitNote/cac:DebitNoteLine/cbc:ID
	 * 
	 */
	private String numeroLinea;
	
	/**
	 * /fe:DebitNote/cac:DebitNoteLine/cbc:DebitedQuantity
	 * 
	 */
	private BigDecimal cantidadDebitada;
	
	/**
	 * /fe:DebitNote/cac:DebitNoteLine/cbc:LineExtensionAmount
	 * 
	 */
	private BigDecimal costoItem;
	
	/**
	 * /fe:DebitNote/cac:DebitNoteLine/cac:Item/cbc:Description
	 * 
	 */
	private String descripcion;
	
	/**
	 * /fe:DebitNote/cac:DebitNoteLine/cac:Item/cbc:PriceAmount
	 * 
	 */
	private BigDecimal precio;

	
	/**
	 * cac:TaxTotal
	 * 
	 * Se usa para desagregar los impuestos por cada Item.
	 * Conforme ejemplos ofrecidos por la DIAN para Notas Débito
	 * 
	 */
	private List<Impuesto> totalImpuestos = new ArrayList<>();
	
	// Getters & setters...
	public String getNumeroLinea() {
		return numeroLinea;
	}

	public void setNumeroLinea(String numeroLinea) {
		this.numeroLinea = numeroLinea;
	}

	public BigDecimal getCantidadDebitada() {
		return cantidadDebitada;
	}

	public void setCantidadDebitada(BigDecimal cantidadDebitada) {
		this.cantidadDebitada = cantidadDebitada;
	}

	public BigDecimal getCostoItem() {
		return costoItem;
	}

	public void setCostoItem(BigDecimal costoItem) {
		this.costoItem = costoItem;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public List<Impuesto> getTotalImpuestos() {
		return totalImpuestos;
	}

	public void setTotalImpuestos(List<Impuesto> totalImpuestos) {
		this.totalImpuestos = totalImpuestos;
	}	
	
}
