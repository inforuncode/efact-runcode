package co.runcode.factura.dm.json;


public class Facturador {
	
	private Persona persona;
		
	public Facturador() { }
	
	public Facturador(Persona persona) {		
		this.persona = persona;
	}
	
	public Persona getPersona() {
		return persona;
	}
	
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
		
}
