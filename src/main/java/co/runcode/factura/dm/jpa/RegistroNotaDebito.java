package co.runcode.factura.dm.jpa;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RegistroNotaDebito {
	
	private String numNd;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fecNd;
	
	private String concepto;

	private String valNd;

	private String nitOFE;
	private String tipAdq;
	private String numAdq;
	
	private String numFac;
	
	private String archivo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fecRecibo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fecEnvio;

	private String estado;

	public String getNumNd() {
		return numNd;
	}

	public void setNumNd(String numNd) {
		this.numNd = numNd;
	}

	public Date getFecNd() {
		return fecNd;
	}

	public void setFecNd(Date fecNd) {
		this.fecNd = fecNd;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getValNd() {
		return valNd;
	}

	public void setValNd(String valNd) {
		this.valNd = valNd;
	}

	public String getNitOFE() {
		return nitOFE;
	}

	public void setNitOFE(String nitOFE) {
		this.nitOFE = nitOFE;
	}

	public String getTipAdq() {
		return tipAdq;
	}

	public void setTipAdq(String tipAdq) {
		this.tipAdq = tipAdq;
	}

	public String getNumAdq() {
		return numAdq;
	}

	public void setNumAdq(String numAdq) {
		this.numAdq = numAdq;
	}

	public String getNumFac() {
		return numFac;
	}

	public void setNumFac(String numFac) {
		this.numFac = numFac;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public Date getFecRecibo() {
		return fecRecibo;
	}

	public void setFecRecibo(Date fecRecibo) {
		this.fecRecibo = fecRecibo;
	}

	public Date getFecEnvio() {
		return fecEnvio;
	}

	public void setFecEnvio(Date fecEnvio) {
		this.fecEnvio = fecEnvio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
