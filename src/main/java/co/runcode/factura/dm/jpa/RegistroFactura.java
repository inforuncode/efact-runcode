package co.runcode.factura.dm.jpa;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RegistroFactura {
	
	private String numFac;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fecFac;
	private String valFac;
	private String valImp1;
	private String valImp2;
	private String valImp3;
	private String valImp;
	private String nitOFE;
	private String tipAdq;
	private String numAdq;
	private String cufe;
	private String archivo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fecRecibo;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "America/Bogota")
	private Date fecEnvio;
	
	private String estado;

	public RegistroFactura() {
	}

	public RegistroFactura(String numFac, Date fecFac, String valFac, String valImp1, String valImp2, String valImp3,
			String valImp, String nitOFE, String tipAdq, String numAdq) {
		this.numFac = numFac;
		this.fecFac = fecFac;
		this.valFac = valFac;
		this.valImp1 = valImp1;
		this.valImp2 = valImp2;
		this.valImp3 = valImp3;
		this.valImp = valImp;
		this.nitOFE = nitOFE;
		this.tipAdq = tipAdq;
		this.numAdq = numAdq;
		cufe = null;
		fecRecibo = null;
		fecEnvio = null;
	}

	public String getNumFac() {
		return numFac;
	}

	public void setNumFac(String numFac) {
		this.numFac = numFac;
	}

	public Date getFecFac() {
		return fecFac;
	}

	public void setFecFac(Date fecFac) {
		this.fecFac = fecFac;
	}

	public String getValFac() {
		return valFac;
	}

	public void setValFac(String valFac) {
		this.valFac = valFac;
	}

	public String getValImp1() {
		return valImp1;
	}

	public void setValImp1(String valImp1) {
		this.valImp1 = valImp1;
	}

	public String getValImp2() {
		return valImp2;
	}

	public void setValImp2(String valImp2) {
		this.valImp2 = valImp2;
	}

	public String getValImp3() {
		return valImp3;
	}

	public void setValImp3(String valImp3) {
		this.valImp3 = valImp3;
	}

	public String getValImp() {
		return valImp;
	}

	public void setValImp(String valImp) {
		this.valImp = valImp;
	}

	public String getNitOFE() {
		return nitOFE;
	}

	public void setNitOFE(String nitOFE) {
		this.nitOFE = nitOFE;
	}

	public String getTipAdq() {
		return tipAdq;
	}

	public void setTipAdq(String tipAdq) {
		this.tipAdq = tipAdq;
	}

	public String getNumAdq() {
		return numAdq;
	}

	public void setNumAdq(String numAdq) {
		this.numAdq = numAdq;
	}

	public String getCufe() {
		return cufe;
	}

	public void setCufe(String cufe) {
		this.cufe = cufe;
	}

	public Date getFecRecibo() {
		return fecRecibo;
	}

	public void setFecRecibo(Date fecRecibo) {
		this.fecRecibo = fecRecibo;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public Date getFecEnvio() {
		return fecEnvio;
	}

	public void setFecEnvio(Date fecEnvio) {
		this.fecEnvio = fecEnvio;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}