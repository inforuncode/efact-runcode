package co.runcode.factura.dm.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_datos_proveedor")
public class DatosProveedor {
	
	@Id
	private String idSoftware;
	
	private String nit;
	private String pin;
	private String nombre;
	private String estado;

	public DatosProveedor() {
		
	}

	public DatosProveedor(String idSoftware, String nit, String pin) {
		this.idSoftware = idSoftware;
		this.nit = nit;
		this.pin = pin;
	}

	public DatosProveedor(String idSoftware, String nit, String pin, String nombre, String estado) {
		this.idSoftware = idSoftware;
		this.nit = nit;
		this.pin = pin;
		this.nombre = nombre;
		this.estado = estado;
	}

	public String toString() {
		return "DatosProveedor{nit:" + nit + " pin:" + pin + " nombre: " + nombre + " estado" + estado + "}";
	}
	
	public String getIdSoftware() {
		return idSoftware;
	}

	public void setIdSoftware(String idSoftware) {
		this.idSoftware = idSoftware;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}
