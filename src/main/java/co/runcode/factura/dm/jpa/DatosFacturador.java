package co.runcode.factura.dm.jpa;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_datos_facturador")
public class DatosFacturador {
	
	@Id
	private String nit;
	
	private String resolucion;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Bogota")
	private LocalDate fchResolucion;
	
	private String prefijo;
	
	private Integer rangoDesde;
	
	private Integer rangoHasta;
	
	private String claveTecnica;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Bogota")
	private LocalDate fchInicialVigencia;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Bogota")
	private LocalDate fchFinalVigencia;
	
	private String certRuta;
	
	private String certPwd;
	
	private String facturasRuta;

	public DatosFacturador() {
	}

	public DatosFacturador(String resolucion, String prefijo, Integer rangoDesde, Integer rangoHasta,
			LocalDate fchInicialVigencia, LocalDate fchFinalVigencia) {
		this.resolucion = resolucion;
		this.prefijo = prefijo;
		this.rangoDesde = rangoDesde;
		this.rangoHasta = rangoHasta;
		this.fchInicialVigencia = fchInicialVigencia;
		this.fchFinalVigencia = fchFinalVigencia;
	}

	public DatosFacturador(String nit, String resolucion, LocalDate fchResolucion, String prefijo, Integer rangoDesde,
			Integer rangoHasta, String claveTecnica, LocalDate fchInicialVigencia, LocalDate fchFinalVigencia,
			String certRuta, String certPwd) {
		this.nit = nit;
		this.resolucion = resolucion;
		this.fchResolucion = fchResolucion;
		this.prefijo = prefijo;
		this.rangoDesde = rangoDesde;
		this.rangoHasta = rangoHasta;
		this.claveTecnica = claveTecnica;
		this.fchInicialVigencia = fchInicialVigencia;
		this.fchFinalVigencia = fchFinalVigencia;
		this.certRuta = certRuta;
		this.certPwd = certPwd;
	}

	public String toString() {
		return "DatosFacturador{nit:" + nit + " resolucion:" + resolucion + " fchResolucion: " + fchResolucion
				+ " prefijo" + prefijo + "}";
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public LocalDate getFchResolucion() {
		return fchResolucion;
	}

	public void setFchResolucion(LocalDate fchResolucion) {
		this.fchResolucion = fchResolucion;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public Integer getRangoDesde() {
		return rangoDesde;
	}

	public void setRangoDesde(Integer rangoDesde) {
		this.rangoDesde = rangoDesde;
	}

	public Integer getRangoHasta() {
		return rangoHasta;
	}

	public void setRangoHasta(Integer rangoHasta) {
		this.rangoHasta = rangoHasta;
	}

	public String getClaveTecnica() {
		return claveTecnica;
	}

	public void setClaveTecnica(String claveTecnica) {
		this.claveTecnica = claveTecnica;
	}

	public LocalDate getFchInicialVigencia() {
		return fchInicialVigencia;
	}

	public void setFchInicialVigencia(LocalDate fchInicialVigencia) {
		this.fchInicialVigencia = fchInicialVigencia;
	}

	public LocalDate getFchFinalVigencia() {
		return fchFinalVigencia;
	}

	public void setFchFinalVigencia(LocalDate fchFinalVigencia) {
		this.fchFinalVigencia = fchFinalVigencia;
	}

	public String getCertRuta() {
		return certRuta;
	}

	public void setCertRuta(String certRuta) {
		this.certRuta = certRuta;
	}

	public String getCertPwd() {
		return certPwd;
	}

	public void setCertPwd(String certPwd) {
		this.certPwd = certPwd;
	}

	public String getFacturasRuta() {
		return facturasRuta;
	}

	public void setFacturasRuta(String facturasRuta) {
		this.facturasRuta = facturasRuta;
	}
}