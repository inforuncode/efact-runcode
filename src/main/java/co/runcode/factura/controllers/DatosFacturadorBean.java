package co.runcode.factura.controllers;

import co.runcode.factura.bo.DatosFacturadorBO;
import co.runcode.factura.dm.jpa.DatosFacturador;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

@ManagedBean
@ViewScoped
public class DatosFacturadorBean
{
  private static final Logger logger = Logger.getLogger(DatosFacturadorBean.class.getName());
  
  private List<DatosFacturador> listFacturadores;
  
  private List<DatosFacturador> listFacturadoresFiltrados;
  
  @Inject
  private DatosFacturadorBO datosFacturadorBO;
  
  public DatosFacturadorBean() {}
  
  @PostConstruct
  public void init() {
    logger.info("init()");
    cargarListaFacturadores();
  }
  

  public void cargarListaFacturadores()
  {
    listFacturadores = datosFacturadorBO.searchAll(DatosFacturador.class);
    
    listFacturadoresFiltrados = new ArrayList<>();
    if ((listFacturadores != null) && (!listFacturadores.isEmpty())) {
      listFacturadoresFiltrados.addAll(listFacturadores);
    }
    
  }
  

  public List<DatosFacturador> getListFacturadores()
  {
    return listFacturadores;
  }
  
  public void setListFacturadores(List<DatosFacturador> listFacturadores) {
    this.listFacturadores = listFacturadores;
  }
  
  public List<DatosFacturador> getListFacturadoresFiltrados() {
    return listFacturadoresFiltrados;
  }
  
  public void setListFacturadoresFiltrados(List<DatosFacturador> listFacturadoresFiltrados) {
    this.listFacturadoresFiltrados = listFacturadoresFiltrados;
  }
}