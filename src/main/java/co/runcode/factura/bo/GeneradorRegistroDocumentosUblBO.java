package co.runcode.factura.bo;

import co.runcode.factura.dm.jpa.RegistroFactura;
import co.runcode.factura.dm.jpa.RegistroNotaCredito;
import co.runcode.factura.dm.jpa.RegistroNotaDebito;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.NotaCredito;
import co.runcode.factura.dm.json.NotaDebito;

public interface GeneradorRegistroDocumentosUblBO {

	RegistroFactura generarRegistroFactura(Factura f, String claveTecnica);

	String generarCufe(Factura f, String claveTecnica);

	RegistroNotaCredito generarRegistroNotaCredito(NotaCredito nc);

	RegistroNotaDebito generarRegistroNotaDebito(NotaDebito nc);
	
}
