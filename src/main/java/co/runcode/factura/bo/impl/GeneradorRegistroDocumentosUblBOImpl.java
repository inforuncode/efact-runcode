package co.runcode.factura.bo.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import javax.inject.Inject;

import co.runcode.commons.util.ArchivosUtil;
import co.runcode.factura.bo.GeneradorRegistroDocumentosUblBO;
import co.runcode.factura.dm.jpa.RegistroFactura;
import co.runcode.factura.dm.jpa.RegistroNotaCredito;
import co.runcode.factura.dm.jpa.RegistroNotaDebito;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.Impuesto;
import co.runcode.factura.dm.json.NotaCredito;
import co.runcode.factura.dm.json.NotaDebito;

public class GeneradorRegistroDocumentosUblBOImpl implements GeneradorRegistroDocumentosUblBO {
	
	@Inject
	private ArchivosUtil archivosUtil;
	
	@Override
	public RegistroFactura generarRegistroFactura(Factura f, String claveTecnica) {
		
		RegistroFactura regFac = new RegistroFactura();
		regFac.setNumFac( f.getId() );		

		DateFormat dateFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String fecFac = dateFormatter.format( f.getFechaHoraFactura().getTime() );
        
        regFac.setFecFac( f.getFechaHoraFactura() );
                                	
        DecimalFormat decimalFormatter = new DecimalFormat("#0.00");                
        regFac.setValFac( decimalFormatter.format( f.getTotalImporteBruto() ) );
               
        regFac.setNitOFE( f.getFacturador().getPersona().getIdentificacion() );
        regFac.setTipAdq( f.getAdquiriente().getPersona().getTipoIdentificacion() );
        regFac.setNumAdq( f.getAdquiriente().getPersona().getIdentificacion() );
        regFac.setValImp1( "0.00" );
        regFac.setValImp2( "0.00" );
        regFac.setValImp3( "0.00" );               
        
        for( Impuesto i : f.getTotalImpuestos() ) {
        	if( i.getCategoria().equals("01") ) {                		                		
        		regFac.setValImp1( decimalFormatter.format( i.getImporteImpuesto()) );
        	
        	} else if( i.getCategoria().equals("02") ) {
        		regFac.setValImp2( decimalFormatter.format( i.getImporteImpuesto()) );
        	
        	} else if( i.getCategoria().equals("03") ) {
        		regFac.setValImp3( decimalFormatter.format( i.getImporteImpuesto()) );
        	
        	}
        }
        	                
        regFac.setValImp( decimalFormatter.format( f.getTotalFactura()) );
        
        String cufe = this.generarCufe(        		
        		regFac.getNumFac(), fecFac , regFac.getValFac(), 
        		"01", regFac.getValImp1(), "02", regFac.getValImp2(), "03", regFac.getValImp3(), 
        		regFac.getValImp(), regFac.getNitOFE(), regFac.getTipAdq(), regFac.getNumAdq(), 
        		claveTecnica
        	);
        
        regFac.setCufe(cufe);		
		return regFac;
	
	}
	
	@Override
	public RegistroNotaCredito generarRegistroNotaCredito(NotaCredito nc) {
		
		RegistroNotaCredito regNc = new RegistroNotaCredito();
		
		regNc.setNumNc( nc.getId() );
		regNc.setFecNc( nc.getFechaHoraEmision() );
		regNc.setConcepto( nc.getConcepto() );
		regNc.setValNC( nc.getTotal().toString() );
		regNc.setNitOFE( nc.getFacturador().getPersona().getIdentificacion() );
		regNc.setTipAdq( nc.getAdquiriente().getPersona().getTipoIdentificacion() );
		regNc.setNumAdq( nc.getAdquiriente().getPersona().getIdentificacion() );
		//regNc.setArchivo(archivo);
		regNc.setNumFac( nc.getNumeroFactura() );
		
		return regNc;
	}
	
	@Override
	public RegistroNotaDebito generarRegistroNotaDebito(NotaDebito nc) {
		
		RegistroNotaDebito regNd = new RegistroNotaDebito();
		
		regNd.setNumNd( nc.getId() );
		regNd.setFecNd( nc.getFechaHoraEmision() );
		regNd.setConcepto( nc.getConcepto() );
		regNd.setValNd( nc.getTotal().toString() );
		regNd.setNitOFE( nc.getFacturador().getPersona().getIdentificacion() );
		regNd.setTipAdq( nc.getAdquiriente().getPersona().getTipoIdentificacion() );
		regNd.setNumAdq( nc.getAdquiriente().getPersona().getIdentificacion() );
		//regNc.setArchivo(archivo);
		regNd.setNumFac( nc.getNumeroFactura() );
		
		return regNd;
	}
	
	@Override
	public String generarCufe(Factura f, String  claveTecnica)  {
		return this.generarRegistroFactura(f, claveTecnica).getCufe();
	
	}
		
	private String generarCufe(String numFac, String fecFac, String valFac, 
			String codImp1, String valImp1, String codImp2, String valImp2, String codImp3, String valImp3, String valImp,
			String nitOFE, String tipAdq, String numAdq, String  clvTec)  {
			
			String output = null;
			MessageDigest sha1;
			
			try {
				sha1 = MessageDigest.getInstance("SHA1");		
			
				StringBuilder input = new StringBuilder();
				input.append(numFac);
				input.append(fecFac);
				input.append(valFac);
				input.append(codImp1);
				input.append(valImp1);
				input.append(codImp2);
				input.append(valImp2);
				input.append(codImp3);
				input.append(valImp3);
				input.append(valImp);
				input.append(nitOFE);
				input.append(tipAdq);
				input.append(numAdq);
				input.append(clvTec);
				
				byte[] digest = sha1.digest( input.toString().getBytes() );
				//String output = Base64.getEncoder().encodeToString( digest );
				output = archivosUtil.bytesToHex( digest );
			
			} catch (NoSuchAlgorithmException e) {			
				e.printStackTrace();
			}			
			
			return output;
			
		}

}
