package co.runcode.factura.bo.impl;

import co.runcode.factura.bo.DatosFacturadorBO;
import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.generic.bo.impl.GenericBOImpl;
import javax.ejb.Stateless;

@Stateless
public class DatosFacturadorBOImpl extends GenericBOImpl<DatosFacturador> implements DatosFacturadorBO
{
  private static final long serialVersionUID = -2430222789908692199L;
  
  public DatosFacturadorBOImpl()
  {
    this(DatosFacturador.class);
  }
  
  public DatosFacturadorBOImpl(Class<DatosFacturador> entityClass) {
    super(entityClass);
  }
  
}