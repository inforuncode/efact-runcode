package co.runcode.factura.bo.impl;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.dom.DOMResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import co.gov.dian.contratos.facturaelectronica.v1.AddressType;
import co.gov.dian.contratos.facturaelectronica.v1.CreditNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.CustomerPartyType;
import co.gov.dian.contratos.facturaelectronica.v1.DebitNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.InvoiceLineType;
import co.gov.dian.contratos.facturaelectronica.v1.InvoiceType;
import co.gov.dian.contratos.facturaelectronica.v1.LocationType;
import co.gov.dian.contratos.facturaelectronica.v1.MonetaryTotalType;
import co.gov.dian.contratos.facturaelectronica.v1.PartyLegalEntityType;
import co.gov.dian.contratos.facturaelectronica.v1.PartyTaxSchemeType;
import co.gov.dian.contratos.facturaelectronica.v1.PartyType;
import co.gov.dian.contratos.facturaelectronica.v1.PersonType;
import co.gov.dian.contratos.facturaelectronica.v1.PriceType;
import co.gov.dian.contratos.facturaelectronica.v1.SupplierPartyType;
import co.gov.dian.contratos.facturaelectronica.v1.TaxSubtotalType;
import co.gov.dian.contratos.facturaelectronica.v1.TaxTotalType;
import co.gov.dian.contratos.facturaelectronica.v1.structures.AuthrorizedInvoices;
import co.gov.dian.contratos.facturaelectronica.v1.structures.DianExtensionsType;
import co.gov.dian.contratos.facturaelectronica.v1.structures.InvoiceControl;
import co.gov.dian.contratos.facturaelectronica.v1.structures.SoftwareProvider;
import co.runcode.commons.util.ArchivosUtil;
import co.runcode.factura.bo.GeneradorDocumentosUblBO;
import co.runcode.factura.bo.GeneradorRegistroDocumentosUblBO;
import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.factura.dm.jpa.DatosProveedor;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.Impuesto;
import co.runcode.factura.dm.json.ItemNC;
import co.runcode.factura.dm.json.ItemND;
import co.runcode.factura.dm.json.NotaCredito;
import co.runcode.factura.dm.json.NotaDebito;
import co.runcode.factura.dm.json.Persona;
import co.runcode.factura.dm.json.PersonaJuridica;
import co.runcode.factura.dm.json.PersonaNatural;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.AddressLineType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.BillingReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.CountryType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.CreditNoteLineType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DebitNoteLineType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ItemType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyIdentificationType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyNameType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PeriodType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.TaxCategoryType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.TaxSchemeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AdditionalAccountIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.AllowanceTotalAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.BaseQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ChargeTotalAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CityNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.CitySubdivisionNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DebitedQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DepartmentType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentCurrencyCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EndDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FamilyNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.FirstNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IdentificationCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.InvoiceTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.InvoicedQuantityType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueTimeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LineExtensionAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.LineType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.MiddleNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NoteType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PayableAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PercentType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PrepaidAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.PriceAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ProfileIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ReferenceIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.RegistrationNameType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.StartDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxEvidenceIndicatorType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxExclusiveAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxLevelCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TaxableAmountType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.TextType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.UBLVersionIDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.UUIDType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.ExtensionContentType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;
import un.unece.uncefact.codelist.specification._54217._2001.CurrencyCodeContentType;
import un.unece.uncefact.data.specification.unqualifieddatatypesschemamodule._2.IdentifierType;

public class GeneradorDocumentosUblBOImpl implements GeneradorDocumentosUblBO {
	
	private static final Logger logger = Logger.getLogger(GeneradorDocumentosUblBOImpl.class.getName());
	
	@Inject 
	private GeneradorRegistroDocumentosUblBO generadorRegistroFacturaBO;
	
	@Inject
	private ArchivosUtil archivosUtil;
	
	@Override
	public InvoiceType generarFacturaUbl(Factura f, DatosFacturador dFacturador, DatosProveedor dProveedor)
			throws DatatypeConfigurationException, JAXBException {
		
		logger.info("--> GENERAR_FACTURA_UBL: " + f.getId() );
		
		InvoiceType fe = new InvoiceType();
		DatatypeFactory dtFactory = DatatypeFactory.newInstance();
		
		//DatosFacturador datosFacturador = datosFacturadorBO.find( f.getFacturador().getPersona().getIdentificacion() );
						
		/// ===============================================================================================			
		//DianExtension :: Inicio
		DianExtensionsType dianExtensions = new DianExtensionsType();
		
			//InviceControl :: Inicio
			InvoiceControl invoiceControl = new InvoiceControl();
			dianExtensions.setInvoiceControl( invoiceControl );
				
				//Invoice authorization
				//invoiceControl.setInvoiceAuthorization( new BigDecimal( RESOLUCION ) );
				invoiceControl.setInvoiceAuthorization( new BigDecimal( dFacturador.getResolucion() ) );
						
				//Authorization period
				PeriodType periodType = new PeriodType();
				invoiceControl.setAuthorizationPeriod(periodType);									
				    
				    // GregorianCalendar gcInicio = (GregorianCalendar) dFacturador.getFchInicialVigencia(); // TODO Verificar migracion Calendar a LocalDate
					GregorianCalendar gcInicio = GregorianCalendar.from( 
							dFacturador.getFchInicialVigencia().atStartOfDay(ZoneId.systemDefault() ));
				
				    XMLGregorianCalendar startDate = dtFactory.newXMLGregorianCalendar( gcInicio );
				    startDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
				    startDate.setHour( DatatypeConstants.FIELD_UNDEFINED );
				    startDate.setMinute( DatatypeConstants.FIELD_UNDEFINED );
				    startDate.setSecond( DatatypeConstants.FIELD_UNDEFINED );	    
			        
				    StartDateType sdtStart = new StartDateType();	    		
				    sdtStart.setValue( startDate );	    
				    periodType.setStartDate( sdtStart );
			  	    
			  	    // GregorianCalendar gcFin = (GregorianCalendar) dFacturador.getFchFinalVigencia();
				    GregorianCalendar gcFin = GregorianCalendar.from( 
							dFacturador.getFchFinalVigencia().atStartOfDay(ZoneId.systemDefault() ));
				    		
			  	    XMLGregorianCalendar endDate = dtFactory.newXMLGregorianCalendar( gcFin );
			  	    endDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			  	    endDate.setHour( DatatypeConstants.FIELD_UNDEFINED );
			  	    endDate.setMinute( DatatypeConstants.FIELD_UNDEFINED );
			  	    endDate.setSecond( DatatypeConstants.FIELD_UNDEFINED );	
			  	    
			  	    EndDateType sdtEnd = new EndDateType();	    		
			  	    sdtEnd.setValue( endDate );  
			  	    periodType.setEndDate( sdtEnd );
			  	  
			  	
			  	//Authorized invoices
		  	    AuthrorizedInvoices authrorizedInvoices = new AuthrorizedInvoices();			  	    
		  	    invoiceControl.setAuthorizedInvoices(authrorizedInvoices);
		  	    
			  	    TextType prefix  = new TextType(); 			  	    
			  	    //prefix.setValue("PRUE");
			  	    prefix.setValue( dFacturador.getPrefijo() );			  	    
			  	    authrorizedInvoices.setPrefix( prefix );			  	   			  	    			  	    
			  	    //authrorizedInvoices.setFrom(980000000);
			  	    //authrorizedInvoices.setTo(985000000);
			  	    authrorizedInvoices.setFrom( dFacturador.getRangoDesde() );
			  	    authrorizedInvoices.setTo( dFacturador.getRangoHasta() );
			  	    
	        //Invoice Control :: Fin
			
		    //Invoice Source
			CountryType countryType = new CountryType();
			dianExtensions.setInvoiceSource(countryType);
			  	  
			  	  //IdentificationCode			  	    			  	  			 
			  	  IdentificationCodeType identificationCode = new IdentificationCodeType();
			  	  countryType.setIdentificationCode(identificationCode);			  	  
			  	  identificationCode.setListAgencyID("6");
			  	  identificationCode.setListAgencyName("United Nations Economic Commission for Europe");
			  	  identificationCode.setListSchemeURI("urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.0");
			  	  identificationCode.setValue("CO");
						  	  			  	 
			//Software Provider			  	  
			//DatosProveedor proveedor = datosProveedorBO.buscarSoftwareActivo();
			  	  
			SoftwareProvider softwareProvider = new SoftwareProvider();
			dianExtensions.setSoftwareProvider(softwareProvider);
			
				IdentifierType providerID = new IdentifierType();
				softwareProvider.setProviderID(providerID);
				providerID.setSchemeAgencyID("195");
				providerID.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
				providerID.setValue( dProveedor.getNit() );
			
				IdentifierType softwareID = new IdentifierType();
				softwareProvider.setSoftwareID(softwareID); 
				softwareID.setSchemeAgencyID("195");
				softwareID.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
				softwareID.setValue( dProveedor.getIdSoftware() );
								
			//Software Security Code
			String securityCode = null;
			
			try {
				MessageDigest sha384 = MessageDigest.getInstance("SHA-384");
				securityCode = archivosUtil.bytesToHex( sha384.digest(( dProveedor.getIdSoftware() + dProveedor.getPin() ).getBytes() ) );
				
			} catch (NoSuchAlgorithmException e) {				
				e.printStackTrace();
			}
				
			IdentifierType softwareSecurityCode = new IdentifierType();
			dianExtensions.setSoftwareSecurityCode(softwareSecurityCode);
			
			softwareSecurityCode.setSchemeAgencyID("195");
			softwareSecurityCode.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");			
			softwareSecurityCode.setValue( securityCode ); 
		  	    				
		//Generar org.w3c.dom.Element
  	    JAXBContext dianStructuresCtx = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1.structures");
  	    Marshaller dianStructuresMarshaller = dianStructuresCtx.createMarshaller();

	    JAXBElement<DianExtensionsType> dianExtensionsJaxbElement
	    		= (new co.gov.dian.contratos.facturaelectronica.v1.structures.ObjectFactory()).createDianExtensions( dianExtensions );	    
	    
		DOMResult domResuslt = new DOMResult();
	  	dianStructuresMarshaller.marshal(dianExtensionsJaxbElement, domResuslt);	  	
	  	
	  	//org.w3c.dom.Element
	  	Element dianExtentionsDomElement = ( (Document) domResuslt.getNode()).getDocumentElement();
	  	
	  	UBLExtensionsType ublExtensions = new UBLExtensionsType();
	  	
		  	//Crear y asignar UBLExtentions - ublExtension[0] 	  	
		  	UBLExtensionType ublExtension0 = new UBLExtensionType();
		  	ExtensionContentType extensionContent0 = new ExtensionContentType();
		  	
		  	ublExtensions.getUBLExtension().add( ublExtension0 );
		  	ublExtension0.setExtensionContent( extensionContent0 );
		  	extensionContent0.setAny( dianExtentionsDomElement );
		  	
		    //Crear y asignar UBLExtentions - ublExtension[1]
		  	UBLExtensionType ublExtension1 = new UBLExtensionType();
		  	ExtensionContentType extensionContent1 = new ExtensionContentType();	
		  	
		  	ublExtensions.getUBLExtension().add( ublExtension1 );
		  	ublExtension1.setExtensionContent( extensionContent1 );		  
		  	extensionContent1.setAny( null ); //Utilizado para agregar la firma.
		
		  	fe.setUBLExtensions( ublExtensions );
		/// ===============================================================================================
				  	
		//Common Basic Components
		UBLVersionIDType ublVersionId = new UBLVersionIDType();
		ublVersionId.setValue("UBL 2.0");
		fe.setUBLVersionID( ublVersionId );
		
		ProfileIDType profileId = new ProfileIDType();
		profileId.setValue("DIAN 1.0");
		fe.setProfileID( profileId );
		
		IDType invoiceId = new IDType();				
		invoiceId.setValue( f.getId() );						
		fe.setID(invoiceId);
		
		UUIDType uuid = new UUIDType();
		uuid.setSchemeAgencyID("195");
		uuid.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
		uuid.setValue( f.getCufe() ); 
		fe.setUUID( uuid );
		
		// FECHA HORA DE EMISIÓN //
		
		//Date date = new Date();
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
	    gregorianCalendar.setTime( f.getFechaHoraFactura() );

		// fecha emisión //
		IssueDateType issueDateType = new IssueDateType();
		
		 	XMLGregorianCalendar issueDate = dtFactory.newXMLGregorianCalendar(gregorianCalendar);
			issueDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			issueDate.setHour(DatatypeConstants.FIELD_UNDEFINED );
			issueDate.setMinute(DatatypeConstants.FIELD_UNDEFINED );
			issueDate.setSecond(DatatypeConstants.FIELD_UNDEFINED );	    
	        
	        issueDateType.setValue( issueDate );
	        fe.setIssueDate( issueDateType );
                
        // hora de emisión //
        IssueTimeType issueTimeType = new IssueTimeType();
        
	        XMLGregorianCalendar issueTime = dtFactory.newXMLGregorianCalendar(gregorianCalendar);
			issueTime.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setYear( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setMonth( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setDay( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setMillisecond( DatatypeConstants.FIELD_UNDEFINED );
			issueTimeType.setValue( issueTime );
			fe.setIssueTime( issueTimeType );
				
        // tipo de factura //
        InvoiceTypeCodeType objTipoFactura =  new InvoiceTypeCodeType();
        objTipoFactura.setListAgencyID("195");
        objTipoFactura.setListAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
        objTipoFactura.setListSchemeURI("http://www.dian.gov.co/contratos/facturaelectronica/v1/InvoiceType");
        objTipoFactura.setValue(""+f.getTipoFactura());
        fe.setInvoiceTypeCode(objTipoFactura);
	        
        // lista de notas //               
        if(f.getNotas() != null && !f.getNotas().isEmpty() ) 
        	{
        	for( String notaStrg : f.getNotas() )
	        	{
					NoteType nota = new NoteType();
					nota.setValue( notaStrg );
					fe.getNote().add( nota );
	        	}  
        	}        
 
        // DIVISA APLICABLE: 
        DocumentCurrencyCodeType documentCurrencyCode = new DocumentCurrencyCodeType();
	       documentCurrencyCode.setValue( f.getMoneda() );	    
	       fe.setDocumentCurrencyCode( documentCurrencyCode );
       	       
	    // DATOS FACTURADOR: SupplierPartyType 
        SupplierPartyType supplierParty = new SupplierPartyType();
	    fe.setAccountingSupplierParty( supplierParty ); 
             
			// Tipo de persona legal (1=natural, 2=juridica, 3=gran contribuyente, 4=otros) //                  
			String tipo_facturador = "";
			String razon_facturador  = "";
			
			Persona pFacturador = f.getFacturador().getPersona(); 	 
									
			if( pFacturador instanceof PersonaJuridica ){
			  PersonaJuridica pjFacturador = (PersonaJuridica) pFacturador;
			  razon_facturador = pjFacturador.getRazonSocial();
			  tipo_facturador = "PERSONA_JURIDICA";
			                  
			}else if (pFacturador instanceof PersonaNatural){
			  PersonaNatural pnFacturador = (PersonaNatural) pFacturador;
			  razon_facturador = pnFacturador.getNombres()+" "+pnFacturador.getPrimerApellido()+" "+pnFacturador.getSegundoApellido();
			  tipo_facturador = "PERSONA_NATURAL";
			}
			else
			{
			  tipo_facturador = "OTROS";  
			}
									
			switch (tipo_facturador) {
				case "PERSONA_JURIDICA": tipo_facturador = "1"; break;
				case "PERSONA_NATURAL": tipo_facturador = "2"; break;
				case "gran contribuyente": tipo_facturador = "3"; break;
				case "OTROS": tipo_facturador = "4"; break;                         
			}
			 			                  
		 	// Estructura Facturador
		 	AdditionalAccountIDType additionalAccountID = new AdditionalAccountIDType();
		 	additionalAccountID.setValue( tipo_facturador );
		 	supplierParty.setAdditionalAccountID( additionalAccountID );
                                                       
			 	PartyType party = new PartyType();
			 	supplierParty.setParty( party );
                                            
			 		PartyIdentificationType partyIdentification = new PartyIdentificationType();
			 		party.getPartyIdentification().add(partyIdentification);
 
						IDType partyId = new IDType();    
						partyId.setSchemeAgencyID("195");
						partyId.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
						partyId.setSchemeID( f.getFacturador().getPersona().getTipoIdentificacion() );
						partyId.setValue( f.getFacturador().getPersona().getIdentificacion() );
						partyIdentification.setID( partyId );
                         
					PartyNameType partyName = new PartyNameType();
					party.getPartyName().add(partyName);
                    
						NameType namedType = new NameType();
						partyName.setName(namedType);
						namedType.setValue("" + razon_facturador);     
              
					LocationType partyPhysicalLocation = new LocationType();	
					party.setPhysicalLocation( partyPhysicalLocation );   
                         
						AddressType addressPartyType = new AddressType();
						partyPhysicalLocation.setAddress(addressPartyType);
         
							DepartmentType department = new DepartmentType();
							department.setValue(""+f.getFacturador().getPersona().getDireccion().getDepartamento());
							addressPartyType.setDepartment( department );                         

							CityNameType city = new CityNameType();
							city.setValue(""+f.getFacturador().getPersona().getDireccion().getMunicipio());
							addressPartyType.setCityName(city);                                   

							CitySubdivisionNameType citySubdivision = new CitySubdivisionNameType();
							citySubdivision.setValue(""+f.getFacturador().getPersona().getDireccion().getCentroPoblado());
							addressPartyType.setCitySubdivisionName( citySubdivision );
                            
							AddressLineType addresLine = new AddressLineType();
							addressPartyType.getAddressLine().add( addresLine );

								LineType line = new LineType();
								line.setValue(""+f.getFacturador().getPersona().getDireccion().getDireccion());
								addresLine.setLine( line );                                            

							CountryType country = new CountryType();
							addressPartyType.setCountry( country );

								IdentificationCodeType countryIdentification = new IdentificationCodeType();
								countryIdentification.setValue(""+f.getFacturador().getPersona().getDireccion().getCodigoPais());
								country.setIdentificationCode(countryIdentification);
                 
					PartyTaxSchemeType tipoRegimen = new PartyTaxSchemeType();
					party.getPartyTaxScheme().add(tipoRegimen);
					
						TaxLevelCodeType taxLevelCode = new TaxLevelCodeType();
						taxLevelCode.setValue(""+f.getFacturador().getPersona().getTipoRegimen());
						tipoRegimen.setTaxLevelCode(taxLevelCode);

						TaxSchemeType taxScheme = new TaxSchemeType();                                                                
						tipoRegimen.setTaxScheme(taxScheme);												
                     
					if( pFacturador instanceof PersonaJuridica ){
						
						PersonaJuridica pjFacturador = (PersonaJuridica) pFacturador;
						 
						PartyLegalEntityType partyLegalEntity = new PartyLegalEntityType();
						party.getPartyLegalEntity().add(partyLegalEntity);
						
							RegistrationNameType registrationName = new RegistrationNameType();
							registrationName.setValue( pjFacturador.getRazonSocial() );
							partyLegalEntity.setRegistrationName(registrationName);
						

                    }else if (pFacturador instanceof PersonaNatural){
                    	
						PersonaNatural pnFacturador = (PersonaNatural) pFacturador;							
						PersonType person = new PersonType();
						
							FirstNameType firstName = new FirstNameType();
							firstName.setValue(pnFacturador.getNombres());
							person.setFirstName(firstName);
							
							FamilyNameType familyName = new FamilyNameType();
							familyName.setValue(pnFacturador.getPrimerApellido());
							person.setFamilyName(familyName);
							
							MiddleNameType middleName = new MiddleNameType();
							middleName.setValue(pnFacturador.getSegundoApellido());
							person.setMiddleName(middleName);  
															
							party.getPerson().add(person);
							// party.setPerson(person);
                    }
	    
			// DATOS ADQUIRIENTE: CustomerPartyType 				
			CustomerPartyType cutomerParty = new CustomerPartyType();
			fe.setAccountingCustomerParty(cutomerParty);
			            
				String tipo_adquiriente = "";
				String razon_adquiriente  = "";
				
				Persona pAdquiriente = f.getAdquiriente().getPersona();

				if( pAdquiriente instanceof PersonaJuridica ) {
					PersonaJuridica pjAdquiriente = (PersonaJuridica) pAdquiriente;
					razon_adquiriente = pjAdquiriente.getRazonSocial();
					tipo_adquiriente = "PERSONA_JURIDICA";
	  
				} else if (pAdquiriente instanceof PersonaNatural){
					PersonaNatural pnAdquitiente = (PersonaNatural) pAdquiriente;
					razon_adquiriente = pnAdquitiente.getNombres()+" "+pnAdquitiente.getPrimerApellido()+" "+pnAdquitiente.getSegundoApellido();
					tipo_adquiriente = "PERSONA_NATURAL";
				} else
				{
				  tipo_adquiriente = "OTROS";  
				}
				            
				switch (tipo_adquiriente) {
					case "PERSONA_JURIDICA": tipo_adquiriente = "1"; break;
					case "PERSONA_NATURAL": tipo_adquiriente = "2"; break;
					case "gran contribuyente": tipo_adquiriente = "3"; break;
					case "OTROS": tipo_adquiriente = "4"; break;                         
				}			
				
				// Estructura Adquiriente					 
	            AdditionalAccountIDType additionalAccountIDCustomer = new AdditionalAccountIDType();
	            additionalAccountIDCustomer.setValue(tipo_adquiriente);
	            cutomerParty.setAdditionalAccountID( additionalAccountIDCustomer );
	            
		            PartyType partyCutomer = new PartyType();
		            cutomerParty.setParty(partyCutomer);
	                    
	                    PartyIdentificationType partyIdentificationCustomer = new PartyIdentificationType();
	                    partyCutomer.getPartyIdentification().add(partyIdentificationCustomer);
	                        
	                        IDType partyIdCustomer = new IDType();
	                        partyIdCustomer.setSchemeAgencyID("195");
	                        partyIdCustomer.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
	                        partyIdCustomer.setSchemeID( f.getAdquiriente().getPersona().getTipoIdentificacion());
	                        partyIdCustomer.setValue( f.getAdquiriente().getPersona().getIdentificacion() ); 
	                        partyIdentificationCustomer.setID(partyIdCustomer);
	                                
	                    PartyNameType partyNameCustomer = new PartyNameType();
	                    partyCutomer.getPartyName().add(partyNameCustomer);
	                    	
	                    	NameType namedTypeCustomer = new NameType();
	                    	partyNameCustomer.setName(namedTypeCustomer);
	                    	namedTypeCustomer.setValue("" + razon_adquiriente);     
	                     
	                    LocationType partyPhysicalLocationCustomer = new LocationType();	
	                    partyCutomer.setPhysicalLocation( partyPhysicalLocationCustomer );   
	                            
							AddressType addressPartyTypeCustomer = new AddressType();
							partyPhysicalLocationCustomer.setAddress(addressPartyTypeCustomer);
	            
	                            DepartmentType departmentCustomer = new DepartmentType();
								addressPartyTypeCustomer.setDepartment( departmentCustomer );
								departmentCustomer.setValue(""+f.getAdquiriente().getPersona().getDireccion().getDepartamento());
								
								CityNameType cityCustomer = new CityNameType();
								addressPartyTypeCustomer.setCityName(cityCustomer);
								cityCustomer.setValue(""+f.getAdquiriente().getPersona().getDireccion().getMunicipio());
								
								CitySubdivisionNameType citySubdivisionCustomer = new CitySubdivisionNameType();
								addressPartyTypeCustomer.setCitySubdivisionName( citySubdivisionCustomer );
								citySubdivisionCustomer.setValue(""+f.getAdquiriente().getPersona().getDireccion().getCentroPoblado());
								
								AddressLineType addresLineCustomer = new AddressLineType();
								addressPartyTypeCustomer.getAddressLine().add( addresLineCustomer );
						
									LineType lineCustomer = new LineType();
									addresLineCustomer.setLine( lineCustomer );
									lineCustomer.setValue(""+f.getAdquiriente().getPersona().getDireccion().getDireccion());
						
								CountryType countryCustomer = new CountryType();
								addressPartyTypeCustomer.setCountry( countryCustomer );
						
		                            IdentificationCodeType countryIdentificationCustomer = new IdentificationCodeType();
		                            countryCustomer.setIdentificationCode(countryIdentificationCustomer);
		                            countryIdentificationCustomer.setValue(""+f.getAdquiriente().getPersona().getDireccion().getCodigoPais());
	                        
		                PartyTaxSchemeType tipoRegimenCustomer = new PartyTaxSchemeType();	                
		                partyCutomer.getPartyTaxScheme().add(tipoRegimenCustomer);
		                
		                	TaxLevelCodeType taxLevelCodeCustomer = new TaxLevelCodeType();
							taxLevelCodeCustomer.setValue(""+f.getFacturador().getPersona().getTipoRegimen());
							tipoRegimenCustomer.setTaxLevelCode( taxLevelCodeCustomer );
							
							TaxSchemeType taxSchemeCustomer = new TaxSchemeType();                                                                
							tipoRegimenCustomer.setTaxScheme(taxSchemeCustomer);						
	                                                           
						if( pAdquiriente instanceof PersonaJuridica ){
	                	   
							PersonaJuridica pjAdquiriente = (PersonaJuridica) pAdquiriente;
							  
							PartyLegalEntityType partyLegalEntity = new PartyLegalEntityType();
							partyCutomer.getPartyLegalEntity().add(partyLegalEntity);
							  
							RegistrationNameType registrationName = new RegistrationNameType();
							registrationName.setValue(pjAdquiriente.getRazonSocial());
							partyLegalEntity.setRegistrationName(registrationName);
	                    	   

						} else if (pAdquiriente instanceof PersonaNatural) {
							PersonaNatural pnAdquiriente = (PersonaNatural) pAdquiriente;
							PersonType person = new PersonType();
							
								FirstNameType firstName = new FirstNameType();
								firstName.setValue(pnAdquiriente.getNombres());
								person.setFirstName(firstName);
								
								FamilyNameType familyName = new FamilyNameType();
								familyName.setValue(pnAdquiriente.getPrimerApellido());
								person.setFamilyName(familyName);
								
								MiddleNameType middleName = new MiddleNameType();
								middleName.setValue(pnAdquiriente.getSegundoApellido());
								person.setMiddleName(middleName);
								
								partyCutomer.getPerson().add(person);
								//partyCutomer.setPerson(person);
	                       }
        
        //*********************Impuestos Factura *******************
		// TODO Impuestos. Verificar si se pasan al tipo moneda reportado en factura JSON o se deja fijo con CurrencyCodeContentType.COP                   
        // Mapa para marcar categorias faltantes
          Map<String, Boolean> categoriaIncuida = new HashMap<>();
          categoriaIncuida.put("01", false);
          categoriaIncuida.put("02", false);
          categoriaIncuida.put("03", false);
          
          for(Impuesto imp : f.getTotalImpuestos())
          { 
                TaxTotalType objTaxTotalType = new TaxTotalType();
                
                  if( imp.getImporteImpuesto()!=null ) // FIXME Que pasa si este valor se pasa como NULL. Debería gestionarse como error
                  {
                      TaxAmountType objTaxAmount = new TaxAmountType();
                      objTaxAmount.setCurrencyID(CurrencyCodeContentType.COP); 
                      objTaxAmount.setValue( imp.getImporteImpuesto());
                      objTaxTotalType.setTaxAmount(objTaxAmount);
                  }
                  
                   if( imp.getImpuestoRetenido()!=null ) // FIXME Que pasa si este valor se pasa como NULL. Debería gestionarse como error
                  {
                      TaxEvidenceIndicatorType objImpRetnido = new TaxEvidenceIndicatorType();
                      objImpRetnido.setValue( imp.getImpuestoRetenido() );
                      objTaxTotalType.setTaxEvidenceIndicator(objImpRetnido);
                  }
                  
                  TaxSubtotalType objTaxSubtotal = new TaxSubtotalType();
                     
                   if( imp.getBaseImponible()!=null )
                   {
                      TaxableAmountType baseImponible = new TaxableAmountType();
                      baseImponible.setCurrencyID(CurrencyCodeContentType.COP);
                      baseImponible.setValue( imp.getBaseImponible());
                      objTaxSubtotal.setTaxableAmount(baseImponible);
                   }
                   if( imp.getImporteImpuesto()!=null)
                   {  
                      TaxAmountType importeImpuesto = new TaxAmountType();
                      importeImpuesto.setCurrencyID(CurrencyCodeContentType.COP);
                      importeImpuesto.setValue( imp.getImporteImpuesto());
                      objTaxSubtotal.setTaxAmount(importeImpuesto);
                   }
                   if( imp.getPorcentaje()!=null)
                   {    
                      PercentType objporcentaje = new PercentType();
                      objporcentaje.setValue( imp.getPorcentaje());
                      objTaxSubtotal.setPercent(objporcentaje);
                   } 
                   if( imp.getCategoria()!=null)
                   {
                      TaxCategoryType objCategory = new TaxCategoryType();                            
                      TaxSchemeType taxSchemeCategory = new TaxSchemeType();                            
                      IDType objId = new IDType();
                                                
                      objId.setValue( imp.getCategoria() );                            
                      taxSchemeCategory.setID(objId);
                      objCategory.setTaxScheme( taxSchemeCategory );
                      objTaxSubtotal.setTaxCategory( objCategory );
                      
                      if( "01".equals(imp.getCategoria()) || "02".equals(imp.getCategoria()) || "03".equals(imp.getCategoria()) ) {
                    	  categoriaIncuida.put(imp.getCategoria() , true);
                      }
                   }
                   
                  objTaxTotalType.getTaxSubtotal().add(objTaxSubtotal);                      
                  fe.getTaxTotal().add(objTaxTotalType);
          }
          
          // Completar Categorías de impuestos faltantes con ceros
          // TODO Verificar si es necesario o se valida desde el JSON de entrada su obligatoriedad
          for( String cat: categoriaIncuida.keySet() ) {
        	  BigDecimal cero = new BigDecimal("0.00");
        	  BigDecimal ceroSinDecimales = new BigDecimal("0");
        	  
        	  if( !categoriaIncuida.get(cat) ) {
        		  TaxTotalType objTaxTotalType = new TaxTotalType();
        		  
        		  TaxAmountType objTaxAmount = new TaxAmountType();
                  objTaxAmount.setCurrencyID(CurrencyCodeContentType.COP); 
                  objTaxAmount.setValue( cero );
                  objTaxTotalType.setTaxAmount(objTaxAmount);
                  
                  TaxEvidenceIndicatorType objImpRetnido = new TaxEvidenceIndicatorType();
                  objImpRetnido.setValue( false );
                  objTaxTotalType.setTaxEvidenceIndicator(objImpRetnido);
                  
                  TaxSubtotalType objTaxSubtotal = new TaxSubtotalType();
                  	
                      TaxableAmountType baseImponible = new TaxableAmountType();
                      baseImponible.setCurrencyID(CurrencyCodeContentType.COP);
                      baseImponible.setValue( cero );
                      objTaxSubtotal.setTaxableAmount(baseImponible);
                  
                      TaxAmountType importeImpuesto = new TaxAmountType();
                      importeImpuesto.setCurrencyID(CurrencyCodeContentType.COP);
                      importeImpuesto.setValue( cero );
                      objTaxSubtotal.setTaxAmount(importeImpuesto);
                      
                      PercentType objporcentaje = new PercentType();
                      objporcentaje.setValue( ceroSinDecimales );
                      objTaxSubtotal.setPercent( objporcentaje );
                      
                      TaxCategoryType objCategory = new TaxCategoryType();                            
                      TaxSchemeType taxSchemeCategory = new TaxSchemeType();                            
                      IDType objId = new IDType();
                      objId.setValue( cat );                            
                      taxSchemeCategory.setID(objId);
                      objCategory.setTaxScheme( taxSchemeCategory );
                      objTaxSubtotal.setTaxCategory( objCategory );
                      
                      objTaxTotalType.getTaxSubtotal().add(objTaxSubtotal);                      
                      fe.getTaxTotal().add(objTaxTotalType);
        		  
        	  }        	  
          }
          
          /*---------------------------- Datos importes totales ---------------------------*/                               
          MonetaryTotalType objMonetaryTotal = new MonetaryTotalType();
               LineExtensionAmountType objBrutoImporte = new LineExtensionAmountType();
               objBrutoImporte.setCurrencyID(CurrencyCodeContentType.COP);
               objBrutoImporte.setValue(f.getTotalImporteBruto());
               objMonetaryTotal.setLineExtensionAmount(objBrutoImporte);
               
               TaxExclusiveAmountType objImponible = new TaxExclusiveAmountType();
               objImponible.setCurrencyID(CurrencyCodeContentType.COP);
               objImponible.setValue(f.getTotalBaseImponible());
               objMonetaryTotal.setTaxExclusiveAmount(objImponible);
               
               if(f.getTotalDescuentos() != null){
                   AllowanceTotalAmountType objDescuento = new AllowanceTotalAmountType();
                   objDescuento.setCurrencyID(CurrencyCodeContentType.COP);
                   objDescuento.setValue(f.getTotalDescuentos());
                   objMonetaryTotal.setAllowanceTotalAmount(objDescuento);
               }
                                 
               PayableAmountType payableAmount = new PayableAmountType();                
               payableAmount.setCurrencyID(CurrencyCodeContentType.COP);
               payableAmount.setValue( f.getTotalFactura() );
               objMonetaryTotal.setPayableAmount( payableAmount );               
           
            fe.setLegalMonetaryTotal(objMonetaryTotal);
        
            /*---------------------------- Items de la factura ------------------------------------*/               
            for(int i=0; i < f.getListaItems().size(); i++)
            {
              InvoiceLineType invoiceLine = new InvoiceLineType();
              
                IDType iditem = new IDType();
                iditem.setValue(f.getListaItems().get(i).getNumeroLinea());
                invoiceLine.setID(iditem);
                
                InvoicedQuantityType cantidadItems = new InvoicedQuantityType();
                cantidadItems.setValue( f.getListaItems().get(i).getCantidad() );
                invoiceLine.setInvoicedQuantity(cantidadItems);
                
                LineExtensionAmountType CostoTotalItem = new LineExtensionAmountType();
                CostoTotalItem.setCurrencyID(CurrencyCodeContentType.COP);
                CostoTotalItem.setValue(f.getListaItems().get(i).getCostoTotal());
                invoiceLine.setLineExtensionAmount(CostoTotalItem);
                
                co.gov.dian.contratos.facturaelectronica.v1.ItemType objItem
                	= new co.gov.dian.contratos.facturaelectronica.v1.ItemType();
                
                DescriptionType objdescrip = new DescriptionType();
                objdescrip.setValue(f.getListaItems().get(i).getDescripcion());
                objItem.getDescription().add(objdescrip);
                invoiceLine.setItem(objItem);
                
                PriceType precioItem =  new PriceType();
                invoiceLine.setPrice(precioItem);
                
                    PriceAmountType objprice = new PriceAmountType();
                    objprice.setCurrencyID(CurrencyCodeContentType.COP);
                    objprice.setValue(f.getListaItems().get(i).getPrecioUnitario());
                    precioItem.setPriceAmount(objprice);
                
                    BaseQuantityType objCantidad = new BaseQuantityType();
                    objCantidad.setValue( f.getListaItems().get(i).getCantidad() );
                    precioItem.setBaseQuantity(objCantidad);
                                    
                UUIDType uuIdType = new UUIDType();
                uuIdType.setValue(f.getListaItems().get(i).getDescripcion());
                invoiceLine.setUUID(uuIdType);
              
                NoteType noteItem = new NoteType();
                noteItem.setValue(f.getListaItems().get(i).getNota());
                invoiceLine.setNote(noteItem);
                                     
              fe.getInvoiceLine().add(invoiceLine);
            }
	
            //******************** Generar CUFE **************************//            
            //String cufe = generarCufe(f, dFacturador.getClaveTecnica() );
            String cufe = generadorRegistroFacturaBO.generarCufe(f, dFacturador.getClaveTecnica());
            uuid.setValue( cufe );
						
		// =============================================================
		// imprimirFacturaUbl(fe);
		
		return fe;		
		
	}
	
	@Override
	public void imprimirFacturaUbl(InvoiceType fe) throws JAXBException {
		//Generates org.w3c.dom.Element
        JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();
        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        JAXBElement<InvoiceType> feJaxbElement
			= (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createInvoice( fe );	    
               
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");
        dianUBLMarshaller.marshal(feJaxbElement, System.out); //Se puede cambiar sysout por un archivo (e.g. factura.xml) 
	}	
	
	@Override
	public CreditNoteType generarNotaCreditoUbl(NotaCredito nc, DatosFacturador dFacturador, DatosProveedor dProveedor)
			throws DatatypeConfigurationException, JAXBException  {
		
		logger.info("--> GENERAR_NOTA_CREDITO_UBL: " + nc.getId() );
				
		CreditNoteType nce = new CreditNoteType();		
		DatatypeFactory dtFactory = DatatypeFactory.newInstance();
								
		// ====================	EXTENSIONES DIAN :: INICIO ================================	
		// /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions		
		DianExtensionsType dianExtensions = new DianExtensionsType();
					
		    // /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions/sts:InvoiceSource
			CountryType countryType = new CountryType();
			dianExtensions.setInvoiceSource(countryType);
			  	  
			  	  // IdentificationCode			  	    			  	  			 
			  	  IdentificationCodeType identificationCode = new IdentificationCodeType();
			  	  countryType.setIdentificationCode(identificationCode);			  	  
			  	  identificationCode.setListAgencyID("6");
			  	  identificationCode.setListAgencyName("United Nations Economic Commission for Europe");
			  	  identificationCode.setListSchemeURI("urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.0");
			  	  identificationCode.setValue("CO");
						  	  			  	 
			// /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions/sts:SoftwareProvider	  	  
			SoftwareProvider softwareProvider = new SoftwareProvider();
			dianExtensions.setSoftwareProvider(softwareProvider);
			
				IdentifierType providerID = new IdentifierType();
				softwareProvider.setProviderID(providerID);
				providerID.setSchemeAgencyID("195");
				providerID.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
				providerID.setValue( dProveedor.getNit() );
			
				IdentifierType softwareID = new IdentifierType();
				softwareProvider.setSoftwareID(softwareID); 
				softwareID.setSchemeAgencyID("195");
				softwareID.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
				softwareID.setValue( dProveedor.getIdSoftware() );
								
			// /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions/sts:SoftwareSecurityCode
			String securityCode = null;
			
			try {
				MessageDigest sha384 = MessageDigest.getInstance("SHA-384");
				securityCode = archivosUtil.bytesToHex( sha384.digest(( dProveedor.getIdSoftware() + dProveedor.getPin() ).getBytes() ) );
				
			} catch (NoSuchAlgorithmException e) {				
				e.printStackTrace();
			}
				
			IdentifierType softwareSecurityCode = new IdentifierType();
			dianExtensions.setSoftwareSecurityCode(softwareSecurityCode);
			
			softwareSecurityCode.setSchemeAgencyID("195");
			softwareSecurityCode.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");			
			softwareSecurityCode.setValue( securityCode ); 
		  	    	
			
		//Generar org.w3c.dom.Element
  	    JAXBContext dianStructuresCtx = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1.structures");
  	    Marshaller dianStructuresMarshaller = dianStructuresCtx.createMarshaller();

	    JAXBElement<DianExtensionsType> dianExtensionsJaxbElement
	    		= (new co.gov.dian.contratos.facturaelectronica.v1.structures.ObjectFactory()).createDianExtensions( dianExtensions );	    
	    
		DOMResult domResuslt = new DOMResult();
	  	dianStructuresMarshaller.marshal(dianExtensionsJaxbElement, domResuslt);	  	
	  	
	  	//org.w3c.dom.Element
	  	Element dianExtentionsDomElement = ( (Document) domResuslt.getNode()).getDocumentElement();
	  	
	  	UBLExtensionsType ublExtensions = new UBLExtensionsType();
	  	
		  	//Crear y asignar UBLExtentions - ublExtension[0] 	  	
		  	UBLExtensionType ublExtension0 = new UBLExtensionType();
		  	ExtensionContentType extensionContent0 = new ExtensionContentType();
		  	
		  	ublExtensions.getUBLExtension().add( ublExtension0 );
		  	ublExtension0.setExtensionContent( extensionContent0 );
		  	extensionContent0.setAny( dianExtentionsDomElement );
		  	
		    //Crear y asignar UBLExtentions - ublExtension[1]
		  	UBLExtensionType ublExtension1 = new UBLExtensionType();
		  	ExtensionContentType extensionContent1 = new ExtensionContentType();	
		  	
		  	ublExtensions.getUBLExtension().add( ublExtension1 );
		  	ublExtension1.setExtensionContent( extensionContent1 );		  
		  	extensionContent1.setAny( null ); //Utilizado para agregar la firma.
		
		  	nce.setUBLExtensions( ublExtensions );
		
	    // ====================	EXTENSIONES DIAN :: FIN ================================
		
			// VERSIÓN UBL: UBL 2.0
			UBLVersionIDType ublVersionId = new UBLVersionIDType();
			ublVersionId.setValue("UBL 2.0");
			nce.setUBLVersionID( ublVersionId );
			
			// PERFIL: DIAN 1.0
			ProfileIDType profileId = new ProfileIDType();
			profileId.setValue("DIAN 1.0");
			nce.setProfileID( profileId );
			
			// NÚMERO NOTA CRÉDITO
			IDType noteId = new IDType();
			noteId.setValue( nc.getId() );				
			nce.setID(noteId);
			
			// CUFE: Las Notas Credito y Notas Debito no tienen CUFE
			
			// FECHA-HORA EMISIÓN
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
		    gregorianCalendar.setTime( nc.getFechaHoraEmision() );

			// Fecha emisión
			IssueDateType issueDateType = new IssueDateType();
			
			 	XMLGregorianCalendar issueDate = dtFactory.newXMLGregorianCalendar(gregorianCalendar);
				issueDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
				issueDate.setHour(DatatypeConstants.FIELD_UNDEFINED );
				issueDate.setMinute(DatatypeConstants.FIELD_UNDEFINED );
				issueDate.setSecond(DatatypeConstants.FIELD_UNDEFINED );	    
		        
		        issueDateType.setValue( issueDate );
		        nce.setIssueDate( issueDateType );
	                
	        // Hora emisión
	        IssueTimeType issueTimeType = new IssueTimeType();
	        
		        XMLGregorianCalendar issueTime = dtFactory.newXMLGregorianCalendar(gregorianCalendar);
				issueTime.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
				issueTime.setYear( DatatypeConstants.FIELD_UNDEFINED );
				issueTime.setMonth( DatatypeConstants.FIELD_UNDEFINED );
				issueTime.setDay( DatatypeConstants.FIELD_UNDEFINED );
				issueTime.setMillisecond( DatatypeConstants.FIELD_UNDEFINED );
				issueTimeType.setValue( issueTime );
				nce.setIssueTime( issueTimeType );
				
			// LISTA NOTAS               
	        if( nc.getNotas() != null && !nc.getNotas().isEmpty() ) 
	        	{
	        	for( String notaStrg : nc.getNotas() )
		        	{
						NoteType nota = new NoteType();
						nota.setValue( notaStrg );
						nce.getNote().add( nota );
		        	}  
	        	}
	                
	        // CONCEPTO NOTA CRÉDITO
	        ResponseType discrepancyResponse = new ResponseType();
	        
	        	ReferenceIDType referenceID = new ReferenceIDType();
	        	discrepancyResponse.setReferenceID(referenceID);
	        	
	        	ResponseCodeType responseCode = new ResponseCodeType();
	        	responseCode.setValue( nc.getConcepto() );
	        	responseCode.setListName("concepto de notas crédito");
	        	responseCode.setListSchemeURI("http://www.dian.gov.co/micrositios/fac_electronica/documentos/Anexo_Tecnico_001_Formatos_de_los_Documentos_XML_de_Facturacion_Electron.pdf");
	        	//responseCode.setName("2:= anulación de la factura electrónica");
	        	discrepancyResponse.setResponseCode(responseCode);
	                
	        nce.getDiscrepancyResponse().add( discrepancyResponse );
	        
	        
	        // FACTURA DE REFERENCIA
	        // BR: No aplica si concepto -> 3:= Rebaja total aplicada
	        
	        if( !nc.getConcepto().equals("3") ) {
	        	BillingReferenceType billingReference = new BillingReferenceType();
	            nce.getBillingReference().add(billingReference);
	            
	            	DocumentReferenceType invoiceDocumentReference = new DocumentReferenceType();
	            	billingReference.setInvoiceDocumentReference(invoiceDocumentReference);
	            		
	            		IDType idFactura= new IDType();
	            		idFactura.setValue( nc.getNumeroFactura() );
	            		invoiceDocumentReference.setID(idFactura);
	            	
	            		// No aplica si es factura de contingencia
	            		if (nc.getCufeFactura() != null && !nc.getCufeFactura().equals("") ) {
	            			UUIDType uuidFactura = new UUIDType();
	                		uuidFactura.setValue( nc.getCufeFactura() );
	                		invoiceDocumentReference.setUUID(uuidFactura);
	            		}
	            		
	            		IssueDateType fechaFactura = new IssueDateType();
	            		
	            		GregorianCalendar gcFechaFactura = new GregorianCalendar();
	            		gcFechaFactura.setTime( nc.getFechaFactura() );
	            	    
	        		 	XMLGregorianCalendar fechaFacturaXML = dtFactory.newXMLGregorianCalendar(gcFechaFactura);
	        		 	fechaFacturaXML.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
	        		 	fechaFacturaXML.setHour(DatatypeConstants.FIELD_UNDEFINED );
	        		 	fechaFacturaXML.setMinute(DatatypeConstants.FIELD_UNDEFINED );
	        		 	fechaFacturaXML.setSecond(DatatypeConstants.FIELD_UNDEFINED );	    
	        	        
	        		 	fechaFactura.setValue( fechaFacturaXML );       		
	            		invoiceDocumentReference.setIssueDate( fechaFactura );   
	        }	             	        	        	
	        		
	        // DIVISA APLICABLE
	        DocumentCurrencyCodeType documentCurrencyCode = new DocumentCurrencyCodeType();
	        documentCurrencyCode.setValue( nc.getMoneda() );
	        nce.setDocumentCurrencyCode( documentCurrencyCode );
	        
	        // PERIODO FACTURACIÓN (INVOICE PERIOD) : OPCIONAL. TODO Verificar si se debe incluir       
	               		
	        // DATOS FACTURADOR: SupplierPartyType 
	        SupplierPartyType supplierParty = new SupplierPartyType();
		    nce.setAccountingSupplierParty( supplierParty ); 
	             
				// Tipo de persona legal (1=natural, 2=juridica, 3=gran contribuyente, 4=otros) //                  
				String tipo_facturador = "";
				String razon_facturador  = "";
				//String identificacion_facturador = "";
				
				Persona pFacturador = nc.getFacturador().getPersona(); 	 
										
				if( pFacturador instanceof PersonaJuridica ){
				  PersonaJuridica pjFacturador = (PersonaJuridica) pFacturador;
				  razon_facturador = pjFacturador.getRazonSocial();
				  tipo_facturador = "PERSONA_JURIDICA";
				                  
				}else if (pFacturador instanceof PersonaNatural){
				  PersonaNatural pnFacturador = (PersonaNatural) pFacturador;
				  razon_facturador = pnFacturador.getNombres()+" "+pnFacturador.getPrimerApellido()+" "+pnFacturador.getSegundoApellido();
				  tipo_facturador = "PERSONA_NATURAL";
				}
				else
				{
				  tipo_facturador = "OTROS";  
				}
										
				switch (tipo_facturador) {
					case "PERSONA_JURIDICA": tipo_facturador = "1"; break;
					case "PERSONA_NATURAL": tipo_facturador = "2"; break;
					case "gran contribuyente": tipo_facturador = "3"; break;
					case "OTROS": tipo_facturador = "4"; break;                         
				}
				 			                  
			 	// Estructura Facturador
			 	AdditionalAccountIDType additionalAccountID = new AdditionalAccountIDType();
			 	additionalAccountID.setValue( tipo_facturador );
			 	supplierParty.setAdditionalAccountID( additionalAccountID );
	                                                       
				 	PartyType party = new PartyType();
				 	supplierParty.setParty( party );
	                                            
				 		PartyIdentificationType partyIdentification = new PartyIdentificationType();
				 		party.getPartyIdentification().add(partyIdentification);
	 
							IDType partyId = new IDType();    
							partyId.setSchemeAgencyID("195");
							partyId.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
							partyId.setSchemeID( nc.getFacturador().getPersona().getTipoIdentificacion() );
							partyId.setValue( nc.getFacturador().getPersona().getIdentificacion() );
							partyIdentification.setID( partyId );
	                         
						PartyNameType partyName = new PartyNameType();
						party.getPartyName().add(partyName);
	                    
							NameType namedType = new NameType();
							partyName.setName(namedType);
							namedType.setValue("" + razon_facturador);     
	              
						LocationType partyPhysicalLocation = new LocationType();	
						party.setPhysicalLocation( partyPhysicalLocation );   
	                         
							AddressType addressPartyType = new AddressType();
							partyPhysicalLocation.setAddress(addressPartyType);
	         
								DepartmentType department = new DepartmentType();
								department.setValue(""+nc.getFacturador().getPersona().getDireccion().getDepartamento());
								addressPartyType.setDepartment( department );                         

								CityNameType city = new CityNameType();
								city.setValue(""+nc.getFacturador().getPersona().getDireccion().getMunicipio());
								addressPartyType.setCityName(city);                                   

								CitySubdivisionNameType citySubdivision = new CitySubdivisionNameType();
								citySubdivision.setValue(""+nc.getFacturador().getPersona().getDireccion().getCentroPoblado());
								addressPartyType.setCitySubdivisionName( citySubdivision );
	                            
								AddressLineType addresLine = new AddressLineType();
								addressPartyType.getAddressLine().add( addresLine );

									LineType line = new LineType();
									line.setValue(""+nc.getFacturador().getPersona().getDireccion().getDireccion());
									addresLine.setLine( line );                                            

								CountryType country = new CountryType();
								addressPartyType.setCountry( country );

									IdentificationCodeType countryIdentification = new IdentificationCodeType();
									countryIdentification.setValue(""+nc.getFacturador().getPersona().getDireccion().getCodigoPais());
									country.setIdentificationCode(countryIdentification);
	                 
						PartyTaxSchemeType tipoRegimen = new PartyTaxSchemeType();
						party.getPartyTaxScheme().add(tipoRegimen);
						
							TaxLevelCodeType taxLevelCode = new TaxLevelCodeType();
							taxLevelCode.setValue(""+nc.getFacturador().getPersona().getTipoRegimen());
							tipoRegimen.setTaxLevelCode(taxLevelCode);

							TaxSchemeType taxScheme = new TaxSchemeType();                                                                
							tipoRegimen.setTaxScheme(taxScheme);												
	                     
						if( pFacturador instanceof PersonaJuridica ){
							
							PersonaJuridica pjFacturador = (PersonaJuridica) pFacturador;
							 
							PartyLegalEntityType partyLegalEntity = new PartyLegalEntityType();
							party.getPartyLegalEntity().add(partyLegalEntity);
							
								RegistrationNameType registrationName = new RegistrationNameType();
								registrationName.setValue( pjFacturador.getRazonSocial() );
								partyLegalEntity.setRegistrationName(registrationName);
							

	                    }else if (pFacturador instanceof PersonaNatural){
	                    	
							PersonaNatural pnFacturador = (PersonaNatural) pFacturador;							
							PersonType person = new PersonType();
							
								FirstNameType firstName = new FirstNameType();
								firstName.setValue(pnFacturador.getNombres());
								person.setFirstName(firstName);
								
								FamilyNameType familyName = new FamilyNameType();
								familyName.setValue(pnFacturador.getPrimerApellido());
								person.setFamilyName(familyName);
								
								MiddleNameType middleName = new MiddleNameType();
								middleName.setValue(pnFacturador.getSegundoApellido());
								person.setMiddleName(middleName);
								party.getPerson().add(person);
	                    } 
						
			// DATOS ADQUIRIENTE: CustomerPartyType 				
			CustomerPartyType cutomerParty = new CustomerPartyType();
			nce.setAccountingCustomerParty(cutomerParty);
			            
				String tipo_adquiriente = "";
				String razon_adquiriente  = "";
				
				Persona pAdquiriente = nc.getAdquiriente().getPersona();

				if( pAdquiriente instanceof PersonaJuridica ) {
					PersonaJuridica pjAdquiriente = (PersonaJuridica) pAdquiriente;
					razon_adquiriente = pjAdquiriente.getRazonSocial();
					tipo_adquiriente = "PERSONA_JURIDICA";
	  
				} else if (pAdquiriente instanceof PersonaNatural){
					PersonaNatural pnAdquitiente = (PersonaNatural) pAdquiriente;
					razon_adquiriente = pnAdquitiente.getNombres()+" "+pnAdquitiente.getPrimerApellido()+" "+pnAdquitiente.getSegundoApellido();
					tipo_adquiriente = "PERSONA_NATURAL";
				} else
				{
				  tipo_adquiriente = "OTROS";  
				}
				            
				switch (tipo_adquiriente) {
					case "PERSONA_JURIDICA": tipo_adquiriente = "1"; break;
					case "PERSONA_NATURAL": tipo_adquiriente = "2"; break;
					case "gran contribuyente": tipo_adquiriente = "3"; break;
					case "OTROS": tipo_adquiriente = "4"; break;                         
				}			
				
				// Estructura Adquiriente					 
	            AdditionalAccountIDType additionalAccountIDCustomer = new AdditionalAccountIDType();
	            additionalAccountIDCustomer.setValue(tipo_adquiriente);
	            cutomerParty.setAdditionalAccountID( additionalAccountIDCustomer );
	            
		            PartyType partyCutomer = new PartyType();
		            cutomerParty.setParty(partyCutomer);
	                    
	                    PartyIdentificationType partyIdentificationCustomer = new PartyIdentificationType();
	                    partyCutomer.getPartyIdentification().add(partyIdentificationCustomer);
	                        
	                        IDType partyIdCustomer = new IDType();
	                        partyIdCustomer.setSchemeAgencyID("195");
	                        partyIdCustomer.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
	                        partyIdCustomer.setSchemeID(nc.getAdquiriente().getPersona().getTipoIdentificacion());
	                        partyIdCustomer.setValue( nc.getAdquiriente().getPersona().getIdentificacion() ); 
	                        partyIdentificationCustomer.setID(partyIdCustomer);
	                                
	                    PartyNameType partyNameCustomer = new PartyNameType();
	                    partyCutomer.getPartyName().add(partyNameCustomer);
	                    	
	                    	NameType namedTypeCustomer = new NameType();
	                    	partyNameCustomer.setName(namedTypeCustomer);
	                    	namedTypeCustomer.setValue("" + razon_adquiriente);     
	                     
	                    LocationType partyPhysicalLocationCustomer = new LocationType();	
	                    partyCutomer.setPhysicalLocation( partyPhysicalLocationCustomer );   
	                            
							AddressType addressPartyTypeCustomer = new AddressType();
							partyPhysicalLocationCustomer.setAddress(addressPartyTypeCustomer);
	            
	                            DepartmentType departmentCustomer = new DepartmentType();
								addressPartyTypeCustomer.setDepartment( departmentCustomer );
								departmentCustomer.setValue(""+nc.getAdquiriente().getPersona().getDireccion().getDepartamento());
								
								CityNameType cityCustomer = new CityNameType();
								addressPartyTypeCustomer.setCityName(cityCustomer);
								cityCustomer.setValue(""+nc.getAdquiriente().getPersona().getDireccion().getMunicipio());
								
								CitySubdivisionNameType citySubdivisionCustomer = new CitySubdivisionNameType();
								addressPartyTypeCustomer.setCitySubdivisionName( citySubdivisionCustomer );
								citySubdivisionCustomer.setValue(""+nc.getAdquiriente().getPersona().getDireccion().getCentroPoblado());
								
								AddressLineType addresLineCustomer = new AddressLineType();
								addressPartyTypeCustomer.getAddressLine().add( addresLineCustomer );
						
									LineType lineCustomer = new LineType();
									addresLineCustomer.setLine( lineCustomer );
									lineCustomer.setValue(""+nc.getAdquiriente().getPersona().getDireccion().getDireccion());
						
								CountryType countryCustomer = new CountryType();
								addressPartyTypeCustomer.setCountry( countryCustomer );
						
		                            IdentificationCodeType countryIdentificationCustomer = new IdentificationCodeType();
		                            countryCustomer.setIdentificationCode(countryIdentificationCustomer);
		                            countryIdentificationCustomer.setValue(""+nc.getAdquiriente().getPersona().getDireccion().getCodigoPais());
	                        
		                PartyTaxSchemeType tipoRegimenCustomer = new PartyTaxSchemeType();	                
		                partyCutomer.getPartyTaxScheme().add(tipoRegimenCustomer);
		                
		                	TaxLevelCodeType taxLevelCodeCustomer = new TaxLevelCodeType();
							taxLevelCodeCustomer.setValue(""+nc.getFacturador().getPersona().getTipoRegimen());
							tipoRegimenCustomer.setTaxLevelCode( taxLevelCodeCustomer );
							
							TaxSchemeType taxSchemeCustomer = new TaxSchemeType();                                                                
							tipoRegimenCustomer.setTaxScheme(taxSchemeCustomer);						
	                                                           
						if( pAdquiriente instanceof PersonaJuridica ){
	                	   
							PersonaJuridica pjAdquiriente = (PersonaJuridica) pAdquiriente;
							  
							PartyLegalEntityType partyLegalEntity = new PartyLegalEntityType();
							partyCutomer.getPartyLegalEntity().add(partyLegalEntity);
							  
							RegistrationNameType registrationName = new RegistrationNameType();
							registrationName.setValue(pjAdquiriente.getRazonSocial());
							partyLegalEntity.setRegistrationName(registrationName);
	                    	   

						} else if (pAdquiriente instanceof PersonaNatural) {
							PersonaNatural pnAdquiriente = (PersonaNatural) pAdquiriente;
							PersonType person = new PersonType();
							
								FirstNameType firstName = new FirstNameType();
								firstName.setValue(pnAdquiriente.getNombres());
								person.setFirstName(firstName);
								
								FamilyNameType familyName = new FamilyNameType();
								familyName.setValue(pnAdquiriente.getPrimerApellido());
								person.setFamilyName(familyName);
								
								MiddleNameType middleName = new MiddleNameType();
								middleName.setValue(pnAdquiriente.getSegundoApellido());
								person.setMiddleName(middleName);
																
								partyCutomer.getPerson().add(person);
	                       }
						
			// IMPORTES TOTALES: LegalMonetaryTotal
			MonetaryTotalType monetaryTotal = new MonetaryTotalType();
			
				LineExtensionAmountType totalImporteBruto = new LineExtensionAmountType();
				//totalImporteBruto.setCurrencyID(CurrencyCodeContentType.COP);
				totalImporteBruto.setCurrencyID( CurrencyCodeContentType.valueOf(nc.getMoneda()) );
				totalImporteBruto.setValue(nc.getTotalImporteBruto());
				monetaryTotal.setLineExtensionAmount(totalImporteBruto);
			                
				TaxExclusiveAmountType totalBaseImponible = new TaxExclusiveAmountType();
				//totalBaseImponible.setCurrencyID(CurrencyCodeContentType.COP);
				totalBaseImponible.setCurrencyID( CurrencyCodeContentType.valueOf(nc.getMoneda()) );
				totalBaseImponible.setValue(nc.getTotalBaseImponible());
				monetaryTotal.setTaxExclusiveAmount(totalBaseImponible);
				
				PayableAmountType total = new PayableAmountType();                
				//total.setCurrencyID(CurrencyCodeContentType.COP);
				total.setCurrencyID( CurrencyCodeContentType.valueOf(nc.getMoneda()) );
				total.setValue( nc.getTotal() );
				monetaryTotal.setPayableAmount( total );
						                
				if(nc.getTotalDescuentos() != null){
					AllowanceTotalAmountType totalDescuentos = new AllowanceTotalAmountType();
					//totalDescuentos.setCurrencyID(CurrencyCodeContentType.COP);
					totalDescuentos.setCurrencyID( CurrencyCodeContentType.valueOf(nc.getMoneda()) );
					totalDescuentos.setValue(nc.getTotalDescuentos());
					monetaryTotal.setAllowanceTotalAmount(totalDescuentos);
				}
				
				if(nc.getTotalCargos() != null){
					ChargeTotalAmountType totalCargos = new ChargeTotalAmountType();
					//totalCargos.setCurrencyID(CurrencyCodeContentType.COP);
					totalCargos.setCurrencyID( CurrencyCodeContentType.valueOf(nc.getMoneda()) );
					totalCargos.setValue(nc.getTotalCargos());
					monetaryTotal.setChargeTotalAmount(totalCargos);
				}
				
				if(nc.getTotalAnticipos() != null){
					PrepaidAmountType totalAnticipos = new PrepaidAmountType();
					//totalAnticipos.setCurrencyID(CurrencyCodeContentType.COP);
					totalAnticipos.setCurrencyID( CurrencyCodeContentType.valueOf(nc.getMoneda()) );
					totalAnticipos.setValue(nc.getTotalAnticipos());
					monetaryTotal.setPrepaidAmount(totalAnticipos);
				}
			                  				                
			nce.setLegalMonetaryTotal(monetaryTotal);							
			
			// LINEAS DE NOTA CRÉDITO: CreditNoteLine
			// BR: Según modelo UBL, la nota crédito debe tener al menos un línea de nota crédito
			if( nc.getListaItems().isEmpty() ) {
				throw new IllegalArgumentException("La nota crédito debe tener al menonos un Item (CreditNoteLine).");
			}
			
			for( ItemNC item : nc.getListaItems()  ) {
				CreditNoteLineType creditNoteLine = new CreditNoteLineType();
				nce.getCreditNoteLine().add( creditNoteLine );
				
					IDType itemId = new IDType();
					creditNoteLine.setID( itemId );
					itemId.setValue( item.getNumeroLinea() );					
					
					LineExtensionAmountType lineExtensionAmount = new LineExtensionAmountType();
					creditNoteLine.setLineExtensionAmount( lineExtensionAmount );
					lineExtensionAmount.setValue( item.getCostoItem() );
					lineExtensionAmount.setCurrencyID( CurrencyCodeContentType.valueOf( nc.getMoneda()) );
					

					ItemType lineItem = new ItemType();
					creditNoteLine.setItem( lineItem );
						DescriptionType description = new DescriptionType();
						description.setValue( item.getDescripcion() );
						lineItem.getDescription().add( description );
											
			}
		
		return nce;
									
	}
	
	@Override
	public void imprimirNotaCreditoUbl(CreditNoteType nce) throws JAXBException {
		//Generates org.w3c.dom.Element
        JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();
        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        JAXBElement<CreditNoteType> feJaxbElement
			= (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createCreditNote(nce);	    
               
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");
        dianUBLMarshaller.marshal(feJaxbElement, System.out); //Se puede cambiar sysout por un archivo (e.g. factura.xml) 
	}
	
	public DebitNoteType generarNotaDebitoUbl(NotaDebito nd, DatosFacturador dFacturador, DatosProveedor dProveedor)
			throws DatatypeConfigurationException, JAXBException  {
		
		logger.info("--> GENERAR_NOTA_DEBITO_UBL: " + nd.getId() );
				
		DebitNoteType nde = new DebitNoteType();		
		DatatypeFactory dtFactory = DatatypeFactory.newInstance();
								
		// ====================	EXTENSIONES DIAN :: INICIO ================================	
		// /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions		
		DianExtensionsType dianExtensions = new DianExtensionsType();
					
		    // /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions/sts:InvoiceSource
			CountryType countryType = new CountryType();
			dianExtensions.setInvoiceSource(countryType);
			  	  
			  	  // IdentificationCode			  	    			  	  			 
			  	  IdentificationCodeType identificationCode = new IdentificationCodeType();
			  	  countryType.setIdentificationCode(identificationCode);			  	  
			  	  identificationCode.setListAgencyID("6");
			  	  identificationCode.setListAgencyName("United Nations Economic Commission for Europe");
			  	  identificationCode.setListSchemeURI("urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.0");
			  	  identificationCode.setValue("CO");
						  	  			  	 
			// /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions/sts:SoftwareProvider	  	  
			SoftwareProvider softwareProvider = new SoftwareProvider();
			dianExtensions.setSoftwareProvider(softwareProvider);
			
				IdentifierType providerID = new IdentifierType();
				softwareProvider.setProviderID(providerID);
				providerID.setSchemeAgencyID("195");
				providerID.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
				providerID.setValue( dProveedor.getNit() );
			
				IdentifierType softwareID = new IdentifierType();
				softwareProvider.setSoftwareID(softwareID); 
				softwareID.setSchemeAgencyID("195");
				softwareID.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
				softwareID.setValue( dProveedor.getIdSoftware() );
								
			// /fe:CreditNote/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent/sts:DianExtensions/sts:SoftwareSecurityCode
			String securityCode = null;
			
			try {
				MessageDigest sha384 = MessageDigest.getInstance("SHA-384");
				securityCode = archivosUtil.bytesToHex( sha384.digest(( dProveedor.getIdSoftware() + dProveedor.getPin() ).getBytes() ) );
				
			} catch (NoSuchAlgorithmException e) {				
				e.printStackTrace();
			}
				
			IdentifierType softwareSecurityCode = new IdentifierType();
			dianExtensions.setSoftwareSecurityCode(softwareSecurityCode);
			
			softwareSecurityCode.setSchemeAgencyID("195");
			softwareSecurityCode.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");			
			softwareSecurityCode.setValue( securityCode ); 
		  	    	
			
		//Generar org.w3c.dom.Element
  	    JAXBContext dianStructuresCtx = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1.structures");
  	    Marshaller dianStructuresMarshaller = dianStructuresCtx.createMarshaller();

	    JAXBElement<DianExtensionsType> dianExtensionsJaxbElement
	    		= (new co.gov.dian.contratos.facturaelectronica.v1.structures.ObjectFactory()).createDianExtensions( dianExtensions );	    
	    
		DOMResult domResuslt = new DOMResult();
	  	dianStructuresMarshaller.marshal(dianExtensionsJaxbElement, domResuslt);	  	
	  	
	  	//org.w3c.dom.Element
	  	Element dianExtentionsDomElement = ( (Document) domResuslt.getNode()).getDocumentElement();
	  	
	  	UBLExtensionsType ublExtensions = new UBLExtensionsType();
	  	
		  	//Crear y asignar UBLExtentions - ublExtension[0] 	  	
		  	UBLExtensionType ublExtension0 = new UBLExtensionType();
		  	ExtensionContentType extensionContent0 = new ExtensionContentType();
		  	
		  	ublExtensions.getUBLExtension().add( ublExtension0 );
		  	ublExtension0.setExtensionContent( extensionContent0 );
		  	extensionContent0.setAny( dianExtentionsDomElement );
		  	
		    //Crear y asignar UBLExtentions - ublExtension[1]
		  	UBLExtensionType ublExtension1 = new UBLExtensionType();
		  	ExtensionContentType extensionContent1 = new ExtensionContentType();	
		  	
		  	ublExtensions.getUBLExtension().add( ublExtension1 );
		  	ublExtension1.setExtensionContent( extensionContent1 );		  
		  	extensionContent1.setAny( null ); //Utilizado para agregar la firma.
		
		  	nde.setUBLExtensions( ublExtensions );
		
	    // ====================	EXTENSIONES DIAN :: FIN ================================
		
		// VERSIÓN UBL: UBL 2.0
		UBLVersionIDType ublVersionId = new UBLVersionIDType();
		ublVersionId.setValue("UBL 2.0");
		nde.setUBLVersionID( ublVersionId );
		
		// PERFIL: DIAN 1.0
		ProfileIDType profileId = new ProfileIDType();
		profileId.setValue("DIAN 1.0");
		nde.setProfileID( profileId );
		
		// NÚMERO FACTURA
		IDType noteId = new IDType();
		noteId.setValue( nd.getId() );				
		nde.setID(noteId);
		
		// CUFE: Las Notas Credito y Notas Debito no tienen CUFE
		
		// FECHA-HORA EMISIÓN
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
	    gregorianCalendar.setTime( nd.getFechaHoraEmision() );

		// Fecha emisión
		IssueDateType issueDateType = new IssueDateType();
		
		 	XMLGregorianCalendar issueDate = dtFactory.newXMLGregorianCalendar(gregorianCalendar);
			issueDate.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			issueDate.setHour(DatatypeConstants.FIELD_UNDEFINED );
			issueDate.setMinute(DatatypeConstants.FIELD_UNDEFINED );
			issueDate.setSecond(DatatypeConstants.FIELD_UNDEFINED );	    
	        
	        issueDateType.setValue( issueDate );
	        nde.setIssueDate( issueDateType );
                
        // Hora emisión
        IssueTimeType issueTimeType = new IssueTimeType();
        
	        XMLGregorianCalendar issueTime = dtFactory.newXMLGregorianCalendar(gregorianCalendar);
			issueTime.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setYear( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setMonth( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setDay( DatatypeConstants.FIELD_UNDEFINED );
			issueTime.setMillisecond( DatatypeConstants.FIELD_UNDEFINED );
			issueTimeType.setValue( issueTime );
			nde.setIssueTime( issueTimeType );
			
		// LISTA NOTAS               
        if( nd.getNotas() != null && !nd.getNotas().isEmpty() ) 
        	{
        	for( String notaStrg : nd.getNotas() )
	        	{
					NoteType nota = new NoteType();
					nota.setValue( notaStrg );
					nde.getNote().add( nota );
	        	}  
        	} else { // Se incluye por lo menos una nota en blanco 
        		NoteType nota = new NoteType();
        		nota.setValue( "" );
        		nde.getNote().add( nota );
        	}
	        
        // DIVISA APLICABLE
        DocumentCurrencyCodeType documentCurrencyCode = new DocumentCurrencyCodeType();        
        nde.setDocumentCurrencyCode( documentCurrencyCode );	
        
        // CONCEPTO NOTA DÉBITO
        // 1.- Intereses, 2.- Gastos por cobrar, 3.- Cambio de valor
        ResponseType discrepancyResponse = new ResponseType();
        
        	ReferenceIDType referenceID = new ReferenceIDType();
        	discrepancyResponse.setReferenceID(referenceID);
        	
        	ResponseCodeType responseCode = new ResponseCodeType();
        	responseCode.setValue( nd.getConcepto() );
        	responseCode.setListName("concepto de notas débito");
        	responseCode.setListSchemeURI("http://www.dian.gov.co/micrositios/fac_electronica/documentos/Anexo_Tecnico_001_Formatos_de_los_Documentos_XML_de_Facturacion_Electron.pdf");
        	//responseCode.setName("1:= intereses");
        	discrepancyResponse.setResponseCode(responseCode);
                
        nde.getDiscrepancyResponse().add( discrepancyResponse );        
        
        // FACTURA DE REFERENCIA
        BillingReferenceType billingReference = new BillingReferenceType();
        nde.getBillingReference().add(billingReference);
        
        	DocumentReferenceType invoiceDocumentReference = new DocumentReferenceType();
        	billingReference.setInvoiceDocumentReference(invoiceDocumentReference);
        		
        		IDType idFactura= new IDType();
        		idFactura.setValue( nd.getNumeroFactura() );
        		invoiceDocumentReference.setID(idFactura);
        	
        		// No aplica si es factura de contingencia
        		if (nd.getCufeFactura() != null && !nd.getCufeFactura().equals("") ) {
        			UUIDType uuidFactura = new UUIDType();
            		uuidFactura.setValue( nd.getCufeFactura() );
            		invoiceDocumentReference.setUUID(uuidFactura);
        		}
        		
				IssueDateType fechaFactura = new IssueDateType();
				
				GregorianCalendar gcFechaFactura = new GregorianCalendar();
				gcFechaFactura.setTime( nd.getFechaFactura() );
			    
			 	XMLGregorianCalendar fechaFacturaXML = dtFactory.newXMLGregorianCalendar(gcFechaFactura);
			 	fechaFacturaXML.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
			 	fechaFacturaXML.setHour(DatatypeConstants.FIELD_UNDEFINED );
			 	fechaFacturaXML.setMinute(DatatypeConstants.FIELD_UNDEFINED );
			 	fechaFacturaXML.setSecond(DatatypeConstants.FIELD_UNDEFINED );	    
		        
			 	fechaFactura.setValue( fechaFacturaXML );       		
				invoiceDocumentReference.setIssueDate( fechaFactura );

        // DATOS FACTURADOR: SupplierPartyType 
        SupplierPartyType supplierParty = new SupplierPartyType();
	    nde.setAccountingSupplierParty( supplierParty ); 
             
			// Tipo de persona legal (1=natural, 2=juridica, 3=gran contribuyente, 4=otros) //                  
			String tipo_facturador = "";
			String razon_facturador  = "";
			//String identificacion_facturador = "";
			
			Persona pFacturador = nd.getFacturador().getPersona(); 	 
									
			if( pFacturador instanceof PersonaJuridica ){
			  PersonaJuridica pjFacturador = (PersonaJuridica) pFacturador;
			  razon_facturador = pjFacturador.getRazonSocial();
			  tipo_facturador = "PERSONA_JURIDICA";
			                  
			}else if (pFacturador instanceof PersonaNatural){
			  PersonaNatural pnFacturador = (PersonaNatural) pFacturador;
			  razon_facturador = pnFacturador.getNombres()+" "+pnFacturador.getPrimerApellido()+" "+pnFacturador.getSegundoApellido();
			  tipo_facturador = "PERSONA_NATURAL";
			}
			else
			{
			  tipo_facturador = "OTROS";  
			}
									
			switch (tipo_facturador) {
				case "PERSONA_JURIDICA": tipo_facturador = "1"; break;
				case "PERSONA_NATURAL": tipo_facturador = "2"; break;
				case "gran contribuyente": tipo_facturador = "3"; break;
				case "OTROS": tipo_facturador = "4"; break;                         
			}
			 			                  
		 	// Estructura Facturador
		 	AdditionalAccountIDType additionalAccountID = new AdditionalAccountIDType();
		 	additionalAccountID.setValue( tipo_facturador );
		 	supplierParty.setAdditionalAccountID( additionalAccountID );
                                                       
			 	PartyType party = new PartyType();
			 	supplierParty.setParty( party );
                                            
			 		PartyIdentificationType partyIdentification = new PartyIdentificationType();
			 		party.getPartyIdentification().add(partyIdentification);
 
						IDType partyId = new IDType();    
						partyId.setSchemeAgencyID("195");
						partyId.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
						partyId.setSchemeID( nd.getFacturador().getPersona().getTipoIdentificacion() );
						partyId.setValue( nd.getFacturador().getPersona().getIdentificacion() );
						partyIdentification.setID( partyId );
                         
					PartyNameType partyName = new PartyNameType();
					party.getPartyName().add(partyName);
                    
						NameType namedType = new NameType();
						partyName.setName(namedType);
						namedType.setValue("" + razon_facturador);     
              
					LocationType partyPhysicalLocation = new LocationType();	
					party.setPhysicalLocation( partyPhysicalLocation );   
                         
						AddressType addressPartyType = new AddressType();
						partyPhysicalLocation.setAddress(addressPartyType);
         
							DepartmentType department = new DepartmentType();
							department.setValue(""+nd.getFacturador().getPersona().getDireccion().getDepartamento());
							addressPartyType.setDepartment( department );                         

							CityNameType city = new CityNameType();
							city.setValue(""+nd.getFacturador().getPersona().getDireccion().getMunicipio());
							addressPartyType.setCityName(city);                                   

							CitySubdivisionNameType citySubdivision = new CitySubdivisionNameType();
							citySubdivision.setValue(""+nd.getFacturador().getPersona().getDireccion().getCentroPoblado());
							addressPartyType.setCitySubdivisionName( citySubdivision );
                            
							AddressLineType addresLine = new AddressLineType();
							addressPartyType.getAddressLine().add( addresLine );

								LineType line = new LineType();
								line.setValue(""+nd.getFacturador().getPersona().getDireccion().getDireccion());
								addresLine.setLine( line );                                            

							CountryType country = new CountryType();
							addressPartyType.setCountry( country );

								IdentificationCodeType countryIdentification = new IdentificationCodeType();
								countryIdentification.setValue(""+nd.getFacturador().getPersona().getDireccion().getCodigoPais());
								country.setIdentificationCode(countryIdentification);
                 
					PartyTaxSchemeType tipoRegimen = new PartyTaxSchemeType();
					party.getPartyTaxScheme().add(tipoRegimen);
					
						TaxLevelCodeType taxLevelCode = new TaxLevelCodeType();
						taxLevelCode.setValue(""+nd.getFacturador().getPersona().getTipoRegimen());
						tipoRegimen.setTaxLevelCode(taxLevelCode);

						TaxSchemeType taxScheme = new TaxSchemeType();                                                                
						tipoRegimen.setTaxScheme(taxScheme);												
                     
					if( pFacturador instanceof PersonaJuridica ){
						
						PersonaJuridica pjFacturador = (PersonaJuridica) pFacturador;
						 
						PartyLegalEntityType partyLegalEntity = new PartyLegalEntityType();
						party.getPartyLegalEntity().add(partyLegalEntity);
						
							RegistrationNameType registrationName = new RegistrationNameType();
							registrationName.setValue( pjFacturador.getRazonSocial() );
							partyLegalEntity.setRegistrationName(registrationName);
						

                    }else if (pFacturador instanceof PersonaNatural){
                    	
						PersonaNatural pnFacturador = (PersonaNatural) pFacturador;							
						PersonType person = new PersonType();
						
							FirstNameType firstName = new FirstNameType();
							firstName.setValue(pnFacturador.getNombres());
							person.setFirstName(firstName);
							
							FamilyNameType familyName = new FamilyNameType();
							familyName.setValue(pnFacturador.getPrimerApellido());
							person.setFamilyName(familyName);
							
							MiddleNameType middleName = new MiddleNameType();
							middleName.setValue(pnFacturador.getSegundoApellido());
							person.setMiddleName(middleName);  
							party.getPerson().add(person);
                    } 
								
		// DATOS ADQUIRIENTE: CustomerPartyType 				
		CustomerPartyType cutomerParty = new CustomerPartyType();
		nde.setAccountingCustomerParty(cutomerParty);
		            
			String tipo_adquiriente = "";
			String razon_adquiriente  = "";
			
			Persona pAdquiriente = nd.getAdquiriente().getPersona();

			if( pAdquiriente instanceof PersonaJuridica ) {
				PersonaJuridica pjAdquiriente = (PersonaJuridica) pAdquiriente;
				razon_adquiriente = pjAdquiriente.getRazonSocial();
				tipo_adquiriente = "PERSONA_JURIDICA";
  
			} else if (pAdquiriente instanceof PersonaNatural){
				PersonaNatural pnAdquitiente = (PersonaNatural) pAdquiriente;
				razon_adquiriente = pnAdquitiente.getNombres()+" "+pnAdquitiente.getPrimerApellido()+" "+pnAdquitiente.getSegundoApellido();
				tipo_adquiriente = "PERSONA_NATURAL";
			} else
			{
			  tipo_adquiriente = "OTROS";  
			}
			            
			switch (tipo_adquiriente) {
				case "PERSONA_JURIDICA": tipo_adquiriente = "1"; break;
				case "PERSONA_NATURAL": tipo_adquiriente = "2"; break;
				case "gran contribuyente": tipo_adquiriente = "3"; break;
				case "OTROS": tipo_adquiriente = "4"; break;                         
			}			
			
			// Estructura Adquiriente					 
            AdditionalAccountIDType additionalAccountIDCustomer = new AdditionalAccountIDType();
            additionalAccountIDCustomer.setValue(tipo_adquiriente);
            cutomerParty.setAdditionalAccountID( additionalAccountIDCustomer );
            
	            PartyType partyCutomer = new PartyType();
	            cutomerParty.setParty(partyCutomer);
                    
                    PartyIdentificationType partyIdentificationCustomer = new PartyIdentificationType();
                    partyCutomer.getPartyIdentification().add(partyIdentificationCustomer);
                        
                        IDType partyIdCustomer = new IDType();
                        partyIdCustomer.setSchemeAgencyID("195");
                        partyIdCustomer.setSchemeAgencyName("CO, DIAN (Direccion de Impuestos y Aduanas Nacionales)");
                        partyIdCustomer.setSchemeID(nd.getAdquiriente().getPersona().getTipoIdentificacion());
                        partyIdCustomer.setValue( nd.getAdquiriente().getPersona().getIdentificacion() ); 
                        partyIdentificationCustomer.setID(partyIdCustomer);
                                
                    PartyNameType partyNameCustomer = new PartyNameType();
                    partyCutomer.getPartyName().add(partyNameCustomer);
                    	
                    	NameType namedTypeCustomer = new NameType();
                    	partyNameCustomer.setName(namedTypeCustomer);
                    	namedTypeCustomer.setValue("" + razon_adquiriente);     
                     
                    LocationType partyPhysicalLocationCustomer = new LocationType();	
                    partyCutomer.setPhysicalLocation( partyPhysicalLocationCustomer );   
                            
						AddressType addressPartyTypeCustomer = new AddressType();
						partyPhysicalLocationCustomer.setAddress(addressPartyTypeCustomer);
            
                            DepartmentType departmentCustomer = new DepartmentType();
							addressPartyTypeCustomer.setDepartment( departmentCustomer );
							departmentCustomer.setValue(""+nd.getAdquiriente().getPersona().getDireccion().getDepartamento());
							
							CityNameType cityCustomer = new CityNameType();
							addressPartyTypeCustomer.setCityName(cityCustomer);
							cityCustomer.setValue(""+nd.getAdquiriente().getPersona().getDireccion().getMunicipio());
							
							CitySubdivisionNameType citySubdivisionCustomer = new CitySubdivisionNameType();
							addressPartyTypeCustomer.setCitySubdivisionName( citySubdivisionCustomer );
							citySubdivisionCustomer.setValue(""+nd.getAdquiriente().getPersona().getDireccion().getCentroPoblado());
							
							AddressLineType addresLineCustomer = new AddressLineType();
							addressPartyTypeCustomer.getAddressLine().add( addresLineCustomer );
					
								LineType lineCustomer = new LineType();
								addresLineCustomer.setLine( lineCustomer );
								lineCustomer.setValue(""+nd.getAdquiriente().getPersona().getDireccion().getDireccion());
					
							CountryType countryCustomer = new CountryType();
							addressPartyTypeCustomer.setCountry( countryCustomer );
					
	                            IdentificationCodeType countryIdentificationCustomer = new IdentificationCodeType();
	                            countryCustomer.setIdentificationCode(countryIdentificationCustomer);
	                            countryIdentificationCustomer.setValue(""+nd.getAdquiriente().getPersona().getDireccion().getCodigoPais());
                        
	                PartyTaxSchemeType tipoRegimenCustomer = new PartyTaxSchemeType();	                
	                partyCutomer.getPartyTaxScheme().add(tipoRegimenCustomer);
	                
	                	TaxLevelCodeType taxLevelCodeCustomer = new TaxLevelCodeType();
						taxLevelCodeCustomer.setValue(""+nd.getAdquiriente().getPersona().getTipoRegimen());
						tipoRegimenCustomer.setTaxLevelCode( taxLevelCodeCustomer );
						
						TaxSchemeType taxSchemeCustomer = new TaxSchemeType();                                                                
						tipoRegimenCustomer.setTaxScheme(taxSchemeCustomer);						
                                                           
					if( pAdquiriente instanceof PersonaJuridica ){
                	   
						PersonaJuridica pjAdquiriente = (PersonaJuridica) pAdquiriente;
						  
						PartyLegalEntityType partyLegalEntity = new PartyLegalEntityType();
						partyCutomer.getPartyLegalEntity().add(partyLegalEntity);
						  
						RegistrationNameType registrationName = new RegistrationNameType();
						registrationName.setValue(pjAdquiriente.getRazonSocial());
						partyLegalEntity.setRegistrationName(registrationName);
                    	   

					} else if (pAdquiriente instanceof PersonaNatural) {
						PersonaNatural pnAdquiriente = (PersonaNatural) pAdquiriente;
						PersonType person = new PersonType();
						
							FirstNameType firstName = new FirstNameType();
							firstName.setValue(pnAdquiriente.getNombres());
							person.setFirstName(firstName);
							
							FamilyNameType familyName = new FamilyNameType();
							familyName.setValue(pnAdquiriente.getPrimerApellido());
							person.setFamilyName(familyName);
							
							MiddleNameType middleName = new MiddleNameType();
							middleName.setValue(pnAdquiriente.getSegundoApellido());
							person.setMiddleName(middleName);
							
							partyCutomer.getPerson().add(person);
                       }

		// IMPUESTOS TOTALES :: /fe:DebitNote/fe:TaxTotal
		for(Impuesto imp : nd.getTotalImpuestos())
		{ 
			TaxTotalType taxTotal = new TaxTotalType();

			if( imp.getImporteImpuesto()!=null )
			{
				TaxAmountType taxAmount = new TaxAmountType();
				taxAmount.setCurrencyID( CurrencyCodeContentType.valueOf( nd.getMoneda()) ); 
				taxAmount.setValue( imp.getImporteImpuesto());
				taxTotal.setTaxAmount(taxAmount);
			}
              
			if( imp.getImpuestoRetenido()!=null )
			{
				TaxEvidenceIndicatorType taxEvidenceIndicator = new TaxEvidenceIndicatorType();
				taxEvidenceIndicator.setValue( imp.getImpuestoRetenido() );
				taxTotal.setTaxEvidenceIndicator(taxEvidenceIndicator);
			}

			TaxSubtotalType taxSubtotal = new TaxSubtotalType();
                 
				if( imp.getBaseImponible()!=null )
				{
					TaxableAmountType taxableAmount = new TaxableAmountType();
					taxableAmount.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
					taxableAmount.setValue( imp.getBaseImponible());
					taxSubtotal.setTaxableAmount(taxableAmount);
				}
	               
				if( imp.getImporteImpuesto()!=null)
				{  
					TaxAmountType taxAmount = new TaxAmountType();
					taxAmount.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
					taxAmount.setValue( imp.getImporteImpuesto());
					taxSubtotal.setTaxAmount(taxAmount);
				}
				   
				if( imp.getPorcentaje()!=null)
				{    
					PercentType percent = new PercentType();
					percent.setValue( imp.getPorcentaje());
					taxSubtotal.setPercent(percent);
				}
				
				if( imp.getCategoria()!=null)
				{
					TaxCategoryType taxCategory = new TaxCategoryType();                            
					TaxSchemeType taxSchemeCategory = new TaxSchemeType();                            
					IDType objId = new IDType();
		                                
					objId.setValue( imp.getCategoria() );                            
					taxSchemeCategory.setID(objId);
					taxCategory.setTaxScheme( taxSchemeCategory );
					taxSubtotal.setTaxCategory( taxCategory );
				}
               
              taxTotal.getTaxSubtotal().add(taxSubtotal);                      
              nde.getTaxTotal().add(taxTotal);
              
          }
			
		// IMPORTES TOTALES: LegalMonetaryTotal
		MonetaryTotalType monetaryTotal = new MonetaryTotalType();
		
			LineExtensionAmountType totalImporteBruto = new LineExtensionAmountType();
			//totalImporteBruto.setCurrencyID(CurrencyCodeContentType.COP);
			totalImporteBruto.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
			totalImporteBruto.setValue(nd.getTotalImporteBruto());
			monetaryTotal.setLineExtensionAmount(totalImporteBruto);
		                
			TaxExclusiveAmountType totalBaseImponible = new TaxExclusiveAmountType();
			//totalBaseImponible.setCurrencyID(CurrencyCodeContentType.COP);
			totalBaseImponible.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
			totalBaseImponible.setValue(nd.getTotalBaseImponible());
			monetaryTotal.setTaxExclusiveAmount(totalBaseImponible);
			
			PayableAmountType total = new PayableAmountType();                
			//total.setCurrencyID(CurrencyCodeContentType.COP);
			total.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
			total.setValue( nd.getTotal() );
			monetaryTotal.setPayableAmount( total );
					                
			if(nd.getTotalDescuentos() != null){
				AllowanceTotalAmountType totalDescuentos = new AllowanceTotalAmountType();
				//totalDescuentos.setCurrencyID(CurrencyCodeContentType.COP);
				totalDescuentos.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
				totalDescuentos.setValue(nd.getTotalDescuentos());
				monetaryTotal.setAllowanceTotalAmount(totalDescuentos);
			}
			
			if(nd.getTotalCargos() != null){
				ChargeTotalAmountType totalCargos = new ChargeTotalAmountType();
				//totalCargos.setCurrencyID(CurrencyCodeContentType.COP);
				totalCargos.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
				totalCargos.setValue(nd.getTotalCargos());
				monetaryTotal.setChargeTotalAmount(totalCargos);
			}
			
			if(nd.getTotalAnticipos() != null){
				PrepaidAmountType totalAnticipos = new PrepaidAmountType();
				//totalAnticipos.setCurrencyID(CurrencyCodeContentType.COP);
				totalAnticipos.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
				totalAnticipos.setValue(nd.getTotalAnticipos());
				monetaryTotal.setPrepaidAmount(totalAnticipos);
			}
		                  				                
		nde.setLegalMonetaryTotal(monetaryTotal);		
		
		for( ItemND item : nd.getListaItems()  ) {
			DebitNoteLineType debitNoteLine = new DebitNoteLineType();
			nde.getDebitNoteLine().add( debitNoteLine );
			
				IDType itemId = new IDType();
				debitNoteLine.setID( itemId );
				itemId.setValue( item.getNumeroLinea() );
				
				DebitedQuantityType debitedQuantity = new DebitedQuantityType();
				debitedQuantity.setValue( item.getCantidadDebitada() );
				debitNoteLine.setDebitedQuantity( debitedQuantity );
								
				LineExtensionAmountType lineExtensionAmount = new LineExtensionAmountType();
				debitNoteLine.setLineExtensionAmount( lineExtensionAmount );
				lineExtensionAmount.setValue( item.getCostoItem() );
				lineExtensionAmount.setCurrencyID( CurrencyCodeContentType.valueOf( nd.getMoneda()) );
				
				ItemType lineItem = new ItemType();
				debitNoteLine.setItem( lineItem );
					DescriptionType description = new DescriptionType();
					description.setValue( item.getDescripcion() );
					lineItem.getDescription().add( description );
				
				oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PriceType price 
					= new oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PriceType();
				
				debitNoteLine.setPrice( price );
					PriceAmountType priceAmount = new PriceAmountType();
					priceAmount.setValue( item.getPrecio() );
					price.setPriceAmount( priceAmount );					
					
					// IMPUESTOS POR ITEM :: /fe:DebitNote/cac:DebitNoteLine/cac:TaxTotal
					for(Impuesto imp : item.getTotalImpuestos())
					{ 
						oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.TaxTotalType taxTotal
							= new oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.TaxTotalType();

						if( imp.getImporteImpuesto()!=null )
						{
							TaxAmountType taxAmount = new TaxAmountType();
							taxAmount.setCurrencyID( CurrencyCodeContentType.valueOf( nd.getMoneda()) ); 
							taxAmount.setValue( imp.getImporteImpuesto());
							taxTotal.setTaxAmount(taxAmount);
						}
			              
						if( imp.getImpuestoRetenido()!=null )
						{
							TaxEvidenceIndicatorType taxEvidenceIndicator = new TaxEvidenceIndicatorType();
							taxEvidenceIndicator.setValue( imp.getImpuestoRetenido() );
							taxTotal.setTaxEvidenceIndicator(taxEvidenceIndicator);
						}

						oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.TaxSubtotalType taxSubtotal
							= new oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.TaxSubtotalType();
			                 
							if( imp.getBaseImponible()!=null )
							{
								TaxableAmountType taxableAmount = new TaxableAmountType();
								taxableAmount.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
								taxableAmount.setValue( imp.getBaseImponible());
								taxSubtotal.setTaxableAmount(taxableAmount);
							}
				               
							if( imp.getImporteImpuesto()!=null)
							{  
								TaxAmountType taxAmount = new TaxAmountType();
								taxAmount.setCurrencyID( CurrencyCodeContentType.valueOf(nd.getMoneda()) );
								taxAmount.setValue( imp.getImporteImpuesto());
								taxSubtotal.setTaxAmount(taxAmount);
							}
							   
							if( imp.getPorcentaje()!=null)
							{    
								PercentType percent = new PercentType();
								percent.setValue( imp.getPorcentaje());
								taxSubtotal.setPercent(percent);
							}
							
							// Categoria, si no existe se incluye el TAG vacio
							TaxCategoryType taxCategory = new TaxCategoryType();                            
							TaxSchemeType taxSchemeCategory = new TaxSchemeType();                            
											                  
							if( imp.getCategoria()!=null)
							{
								IDType objId = new IDType();
								objId.setValue( imp.getCategoria() );
								taxSchemeCategory.setID(objId);
							}							 							                           							
							taxCategory.setTaxScheme( taxSchemeCategory );
							taxSubtotal.setTaxCategory( taxCategory );
																	               
			              taxTotal.getTaxSubtotal().add(taxSubtotal);                      
			              //nde.getTaxTotal().add(taxTotal);
			              debitNoteLine.getTaxTotal().add(taxTotal);			              
			          }										
		}
				
		return nde;		    
		    
	}
	
	@Override
	public void imprimirNotaDebitooUbl(DebitNoteType nde) throws JAXBException {
		//Generates org.w3c.dom.Element
        JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();
        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        JAXBElement<DebitNoteType> feJaxbElement
			= (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createDebitNote(nde);	    
               
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");
        dianUBLMarshaller.marshal(feJaxbElement, System.out); //Se puede cambiar sysout por un archivo (e.g. factura.xml) 
	}
	
}