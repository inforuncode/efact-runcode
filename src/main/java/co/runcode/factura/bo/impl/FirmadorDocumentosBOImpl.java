package co.runcode.factura.bo.impl;

import java.io.InputStream;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import co.gov.dian.contratos.facturaelectronica.v1.CreditNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.DebitNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.InvoiceType;
import co.runcode.factura.bo.FirmadorDocumentosBO;
import xades4j.UnsupportedAlgorithmException;
import xades4j.algorithms.Algorithm;
import xades4j.algorithms.CanonicalXMLWithoutComments;
import xades4j.algorithms.EnvelopedSignatureTransform;
import xades4j.algorithms.GenericAlgorithm;
import xades4j.production.DataObjectReference;
import xades4j.production.SignedDataObjects;
import xades4j.production.XadesEpesSigningProfile;
import xades4j.production.XadesSigner;
import xades4j.properties.DataObjectDesc;
import xades4j.properties.IdentifierType;
import xades4j.properties.ObjectIdentifier;
import xades4j.properties.SignaturePolicyBase;
import xades4j.properties.SignaturePolicyIdentifierProperty;
import xades4j.properties.SignerRoleProperty;
import xades4j.properties.SigningTimeProperty;
import xades4j.providers.AlgorithmsProviderEx;
import xades4j.providers.BasicSignatureOptionsProvider;
import xades4j.providers.KeyingDataProvider;
import xades4j.providers.SignaturePolicyInfoProvider;
import xades4j.providers.SignaturePropertiesCollector;
import xades4j.providers.SignaturePropertiesProvider;
import xades4j.providers.impl.FileSystemKeyStoreKeyingDataProvider;
import xades4j.utils.DOMHelper;


public class FirmadorDocumentosBOImpl implements FirmadorDocumentosBO {
	
	final static String DOCUMENTO_POLITICAS_DE_FIRMA = "documentos/politicadefirmav1.pdf";
	final static String URI_DOCUMENTO_POLITICAS_DE_FIRMA = "https://facturaelectronica.dian.gov.co/politicadefirma/v1/politicadefirmav1.pdf";
	
	final static Logger logger =Logger.getLogger( "FirmadorDocumentosBOImpl" );
	
	@Override
	public String firmarFactura(InvoiceType fe, String nombreArchivo, String rutaCertificado, String pwdCertificado) throws Exception {
		
		logger.info("   ---> FIRMAR_FACTURA: " + fe.getID().getValue() );
		        
        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        documentBuilder.setNamespaceAware(true);        
        Document doc = documentBuilder.newDocumentBuilder().newDocument();
		
		JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");
        
        // Especifico de factura
        JAXBElement<InvoiceType> jaxbEl = (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createInvoice( fe );        
        dianUBLMarshaller.marshal(jaxbEl, new DOMResult( doc ) );
		        
        return firmarDocumento(doc, nombreArchivo, rutaCertificado, pwdCertificado);
        
	}
	
	@Override
	public String firmarNotaCredito(CreditNoteType cne, String nombreArchivo, String rutaCertificado, String pwdCertificado) throws Exception {
		
		logger.info("   ---> FIRMAR_NOTA_CREDITO: " + cne.getID().getValue() );
        
        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        documentBuilder.setNamespaceAware(true);        
        Document doc = documentBuilder.newDocumentBuilder().newDocument();
		        
		JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");        
        
        // Especifico de nota crédito 
        JAXBElement<CreditNoteType> jaxbEl = (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createCreditNote( cne );      
        dianUBLMarshaller.marshal(jaxbEl, new DOMResult( doc ) );
        
    	return firmarDocumento(doc, nombreArchivo, rutaCertificado, pwdCertificado);
		
	}
	
	@Override
	public String firmarNotaDebito(DebitNoteType dne, String nombreArchivo, String rutaCertificado, String pwdCertificado)
			throws Exception {
		
		logger.info("   ---> FIRMAR_NOTA_DEBITO: " + dne.getID().getValue() );
		
        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        documentBuilder.setNamespaceAware(true);        
        Document doc = documentBuilder.newDocumentBuilder().newDocument();
		        
		JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");        
        
        // Especifico de nota débito 
        JAXBElement<DebitNoteType> jaxbEl = (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createDebitNote( dne );      
        dianUBLMarshaller.marshal(jaxbEl, new DOMResult( doc ) );
        
        return firmarDocumento(doc, nombreArchivo, rutaCertificado, pwdCertificado);        
	}

	private String firmarDocumento(Document doc, String nombreArchivo, String rutaCertificado, String pwdCertificado) throws Exception {
		
		logger.info("   ---> FIRMAR_DOCUMENTO_PERFIL_XADES: Archivo=" + nombreArchivo +" Certificado=" + rutaCertificado );
				
		Node n = doc.getElementsByTagNameNS("urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2", "ExtensionContent").item(1);		
		Element elem = null;
		if (n.getNodeType() == Node.ELEMENT_NODE) {
			elem = (Element) n;
	    }        
				
		DOMHelper.useIdAsXmlId(elem);				
		
		KeyingDataProvider kdp = new FileSystemKeyStoreKeyingDataProvider(
                "pkcs12", // alternativas: "pkcs12", "pkcs10" o "jks"
                rutaCertificado,
                new CertificateSelector(),
                new PasswordProvider( pwdCertificado ),
                new PasswordProvider( pwdCertificado ),
                true );
		
		// DEFINIR PERFIL XAdES-EPES					
		SignaturePolicyInfoProvider policyInfoProvider = new SignaturePolicyInfoProvider()		 			
			{	            
	            public SignaturePolicyBase getSignaturePolicy()
	            {		            	
	                
	            	ClassLoader classLoader = getClass().getClassLoader();
	            	InputStream input = classLoader.getResourceAsStream(DOCUMENTO_POLITICAS_DE_FIRMA);
					
	                return new SignaturePolicyIdentifierProperty(
	                        new ObjectIdentifier(URI_DOCUMENTO_POLITICAS_DE_FIRMA, IdentifierType.URI),
	                        input)
	                ;
	            }
			};
												
		XadesEpesSigningProfile profile = new XadesEpesSigningProfile(kdp, policyInfoProvider);
		
		// DEFINIR ALGORITMOS A USAR PARA CADA SECCIÓN DEL PERDIL XADES: TimeStamp, ReferenceProperties, DataObjectReferences...
		profile.withAlgorithmsProviderEx( new AlgorithmsProviderEx() {
		
			public Algorithm getSignatureAlgorithm(String keyAlgorithmName) throws UnsupportedAlgorithmException {								
				//return new GenericAlgorithm(XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA1 );				
				return new GenericAlgorithm("http://www.w3.org/2000/09/xmldsig#rsa-sha1");
			}
			
			public String getDigestAlgorithmForTimeStampProperties() {				
				return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
			}
			
			public String getDigestAlgorithmForReferenceProperties() {
				return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
			}
			
			public String getDigestAlgorithmForDataObjsReferences() {
				return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
			}
			
			public Algorithm getCanonicalizationAlgorithmForTimeStampProperties() {				
				return  new CanonicalXMLWithoutComments();
			}
			
			public Algorithm getCanonicalizationAlgorithmForSignature() {				
				return  new CanonicalXMLWithoutComments();
			}
		});		 			 
			 			 
		// ASIGNAR PROPIEDAD ROLE EN "SignatureProperties"
		// Conforme requisitos de protocolo XADES_EPES y normativa Res 000019 DIAN. Anexo 2.  
		profile.withSignaturePropertiesProvider(			
			 new SignaturePropertiesProvider() {
				 public void provideProperties(SignaturePropertiesCollector signedPropsCol) {
					 signedPropsCol.setSignerRole(new SignerRoleProperty("supplier"));		
					 signedPropsCol.setSigningTime(new SigningTimeProperty());
				 }
			 });
		
		// DEFINE QUE EN LA FIRMA SE DEBEN INCLUIR:
		// Información del certificado (cadena completa)
		// Firmar información del certificado
		profile.withBasicSignatureOptionsProvider(
				new BasicSignatureOptionsProvider() {
					
					public boolean signSigningCertificate() {						
						return true;
					}
					
					public boolean includeSigningCertificate() {
						return true;
					}
					
					public boolean includePublicKey() {
						return false;
					}
				}			
			);
					
		// INICIAR CLASE QUE GENERA LA FIRMA (SIGNER)
		XadesSigner signer =  profile.newSigner();		
								
		DataObjectDesc obj = new DataObjectReference("")
                .withTransform(new EnvelopedSignatureTransform());
					
		SignedDataObjects dataObjs = new SignedDataObjects().withSignedDataObject(obj);			
        signer.sign(dataObjs, elem);
		
		// Guardar documento en la ruta de archivo suministrada
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    //transformer.setOutputProperty(OutputKeys.INDENT, "no");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");		
				
		DOMSource source = new DOMSource(doc); 	
		StreamResult result = new StreamResult( nombreArchivo );
        transformer.transform(source, result);      

        logger.info("   ---> FIRMAR_DOCUMENTO_PERFIL_XADES.");
	                
        return nombreArchivo;
		
	}
	
	/*
	@Override
	public String firmarFactura(InvoiceType fe, String nombreArchivo, String rutaCertificado, String pwdCertificado)
			throws Exception {
		
		logger.info("   ---> FIRMAR_FACTURA: " + fe.getID().getValue() );
        
        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        documentBuilder.setNamespaceAware(true);
        
        Document doc = documentBuilder.newDocumentBuilder().newDocument();
		
		JAXBContext dianUBL = JAXBContext.newInstance("co.gov.dian.contratos.facturaelectronica.v1");
        Marshaller dianUBLMarshaller = dianUBL.createMarshaller();
        
        dianUBLMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        dianUBLMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,"http://www.dian.gov.co/contratos/facturaelectronica/v1 ../xsd/DIAN_UBL.xsd urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2 ../../ubl2/common/UnqualifiedDataTypeSchemaModule-2.0.xsd urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2 ../../ubl2/common/UBL-QualifiedDatatypes-2.0.xsd");
        
        JAXBElement<InvoiceType> jaxbEl = (new co.gov.dian.contratos.facturaelectronica.v1.ObjectFactory()).createInvoice( fe );        
        dianUBLMarshaller.marshal(jaxbEl, new DOMResult( doc ) );
		        
        Node n = doc.getElementsByTagNameNS("urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2", "ExtensionContent").item(1);		
		Element elem = null;
		if (n.getNodeType() == Node.ELEMENT_NODE) {
			elem = (Element) n;
	    }        
				
		DOMHelper.useIdAsXmlId(elem);
		
		KeyingDataProvider kdp = new FileSystemKeyStoreKeyingDataProvider(
                "pkcs12", // alternativas: "pkcs10" o  "jks"
                rutaCertificado,
                new CertificateSelector(),
                new PasswordProvider( pwdCertificado ),
                new PasswordProvider( pwdCertificado ),
                true );
		
		// DEFINIR PERFIL XAdES-EPES			
		SignaturePolicyInfoProvider policyInfoProvider = new SignaturePolicyInfoProvider()		 			
			{	            
	            public SignaturePolicyBase getSignaturePolicy()
	            {		            	
	            	ClassLoader classLoader = getClass().getClassLoader();
	            	InputStream input = classLoader.getResourceAsStream(DOCUMENTO_POLITICAS_DE_FIRMA);
					
	                return new SignaturePolicyIdentifierProperty(
	                        new ObjectIdentifier(URI_DOCUMENTO_POLITICAS_DE_FIRMA, IdentifierType.URI),
	                        input)
	                ;	                 
	            }
			};
												
		XadesEpesSigningProfile profile = new XadesEpesSigningProfile(kdp, policyInfoProvider);
		
		// DEFINIR ALGORITMOS A USAR PARA CADA SECCIÓN DEL PERDIL XADES: TimeStamp, ReferenceProperties, DataObjectReferences...
		profile.withAlgorithmsProviderEx( new AlgorithmsProviderEx() {
		
			public Algorithm getSignatureAlgorithm(String keyAlgorithmName) throws UnsupportedAlgorithmException {								
				//return new GenericAlgorithm(XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA1 );				
				return new GenericAlgorithm("http://www.w3.org/2000/09/xmldsig#rsa-sha1");
			}
			
			public String getDigestAlgorithmForTimeStampProperties() {				
				return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
			}
			
			public String getDigestAlgorithmForReferenceProperties() {
				return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
			}
			
			public String getDigestAlgorithmForDataObjsReferences() {
				return MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA1;
			}
			
			public Algorithm getCanonicalizationAlgorithmForTimeStampProperties() {				
				return  new CanonicalXMLWithoutComments();
			}
			
			public Algorithm getCanonicalizationAlgorithmForSignature() {				
				return  new CanonicalXMLWithoutComments();
			}
		});		 			 
		
		// ASIGNAR PROPIEDAD ROLE EN "SignatureProperties"
		// Conforme requisitos de protocolo XADES_EPES y normativa Res 000019 DIAN. Anexo 2.  
		profile.withSignaturePropertiesProvider(			
			 new SignaturePropertiesProvider() {
				 public void provideProperties(SignaturePropertiesCollector signedPropsCol) {
					 signedPropsCol.setSignerRole(new SignerRoleProperty("supplier"));		
					 signedPropsCol.setSigningTime(new SigningTimeProperty());
				 }
			 });
		
		// DEFINE QUE EN LA FIRMA SE DEBEN INCLUIR:
		// Información del certificado (cadena completa)
		// Firmar información del certificado	
		profile.withBasicSignatureOptionsProvider(
				new BasicSignatureOptionsProvider() {
					
					public boolean signSigningCertificate() {						
						return true;
					}
					
					public boolean includeSigningCertificate() {
						return true;
					}
					
					public boolean includePublicKey() {
						return false;
					}
				}			
			);
		
		// INICIAR CLASE QUE GENERA LA FIRMA (SIGNER)
		XadesSigner signer =  profile.newSigner();		
								
		DataObjectDesc obj = new DataObjectReference("")
                .withTransform(new EnvelopedSignatureTransform());
					
		SignedDataObjects dataObjs = new SignedDataObjects().withSignedDataObject(obj);			
        signer.sign(dataObjs, elem);

		// Guardar documento en la ruta de archivo suministrada
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    //transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    //transformer.setOutputProperty(OutputKeys.INDENT, "no");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				
		DOMSource source = new DOMSource(doc); 	
		// StreamResult result = new StreamResult( archivo );
		StreamResult result = new StreamResult( nombreArchivo );
        transformer.transform(source, result);      

        logger.info("   ---> FIRMAR_FACTURA: " + nombreArchivo );
        
        return nombreArchivo;		
	}
	*/
	
}
