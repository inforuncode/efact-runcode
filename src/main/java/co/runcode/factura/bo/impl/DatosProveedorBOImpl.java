package co.runcode.factura.bo.impl;

import co.runcode.factura.bo.DatosProveedorBO;
import co.runcode.factura.dm.jpa.DatosProveedor;
import co.runcode.generic.bo.impl.GenericBOImpl;
import javax.ejb.Stateless;

@Stateless
public class DatosProveedorBOImpl extends GenericBOImpl<DatosProveedor> implements DatosProveedorBO
{
  private static final long serialVersionUID = -2430222789908692199L;
  
  public DatosProveedorBOImpl()
  {
    this(DatosProveedor.class);
  }
  
  public DatosProveedorBOImpl(Class<DatosProveedor> entityClass) {
    super(entityClass);
  }
}