package co.runcode.factura.bo;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

import co.gov.dian.contratos.facturaelectronica.v1.CreditNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.DebitNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.InvoiceType;
import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.factura.dm.jpa.DatosProveedor;
import co.runcode.factura.dm.json.Factura;
import co.runcode.factura.dm.json.NotaCredito;
import co.runcode.factura.dm.json.NotaDebito;

public interface GeneradorDocumentosUblBO {

	// Facturas
	InvoiceType generarFacturaUbl(Factura f, DatosFacturador dFacturador, DatosProveedor dProveedor)
			throws DatatypeConfigurationException, JAXBException;
	
	void imprimirFacturaUbl(InvoiceType fe) throws JAXBException;

	// Notas credito
	CreditNoteType generarNotaCreditoUbl(NotaCredito nc, DatosFacturador dFacturador, DatosProveedor dProveedor)
			throws DatatypeConfigurationException, JAXBException;

	void imprimirNotaCreditoUbl(CreditNoteType nce) throws JAXBException;

	
	// Notas debito
	DebitNoteType generarNotaDebitoUbl(NotaDebito nd, DatosFacturador dFacturador, DatosProveedor dProveedor)
			throws DatatypeConfigurationException, JAXBException;

	void imprimirNotaDebitooUbl(DebitNoteType nde) throws JAXBException;
	
	
}