package co.runcode.factura.bo;

import co.gov.dian.contratos.facturaelectronica.v1.CreditNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.DebitNoteType;
import co.gov.dian.contratos.facturaelectronica.v1.InvoiceType;

public interface FirmadorDocumentosBO {
		
	public String firmarFactura(InvoiceType fe, String nombreArchivo, String rutaCertificado, String pwdCertificado ) throws Exception;
	public String firmarNotaCredito(CreditNoteType cne, String nombreArchivo, String rutaCertificado, String pwdCertificado) throws Exception;
	public String firmarNotaDebito(DebitNoteType dne, String nombreArchivo, String rutaCertificado, String pwdCertificado) throws Exception;

}
