

package co.runcode.factura.bo;

import co.runcode.factura.dm.jpa.DatosProveedor;
import co.runcode.generic.bo.GenericBO;
import javax.ejb.Local;

@Local
public abstract interface DatosProveedorBO extends GenericBO<DatosProveedor> {
	
}

