package co.runcode.factura.bo;

import co.runcode.factura.dm.jpa.DatosFacturador;
import co.runcode.generic.bo.GenericBO;
import javax.ejb.Local;

@Local
public abstract interface DatosFacturadorBO extends GenericBO<DatosFacturador>{
	
}