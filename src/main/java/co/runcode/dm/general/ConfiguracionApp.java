package co.runcode.dm.general;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ConfiguracionApp implements Serializable {

	private static final long serialVersionUID = 5580114904341797406L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String estadoApp;
	private boolean estadoAlarmas;

	public ConfiguracionApp() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEstadoApp() {
		return estadoApp;
	}

	public void setEstadoApp(String estadoApp) {
		this.estadoApp = estadoApp;
	}

	public boolean isEstadoAlarmas() {
		return estadoAlarmas;
	}

	public void setEstadoAlarmas(boolean estadoAlarmas) {
		this.estadoAlarmas = estadoAlarmas;
	}

}
