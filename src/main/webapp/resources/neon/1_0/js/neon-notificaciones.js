function notificacionRegular(titulo,mensaje,tipo){
	
	var opts = {
			"closeButton": true,
			"debug": false,
			"onclick": null,
			"showDuration": "600",
			"hideDuration": "1000",
			"timeOut": "9000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

	if(tipo=='success'){
		toastr.success(mensaje,titulo, opts);
	}
	if(tipo=='info'){
		toastr.info(mensaje,titulo, opts);
	}
	if(tipo=='error'){
		toastr.error(mensaje,titulo, opts);
	}
	if(tipo=='warning'){
		toastr.warning(mensaje,titulo, opts);
	}
}


function notificacionRegularOscura(titulo,mensaje,tipo){
	
	var opts = {
			"closeButton": true,
			"debug": false,
			"onclick": null,
			"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
			"toastClass": "black",
			"showDuration": "600",
			"hideDuration": "1000",
			"timeOut": "9000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

	if(tipo=='success'){
		toastr.success(mensaje,titulo, opts);
	}
	if(tipo=='info'){
		toastr.info(mensaje,titulo, opts);
	}
	if(tipo=='error'){
		toastr.error(mensaje,titulo, opts);
	}
	if(tipo=='warning'){
		toastr.warning(mensaje,titulo, opts);
	}
}

function notificacionRegularEstatica(titulo,mensaje,tipo){
	
	var opts = {
			
			"debug": false,
			"onclick": null,
			"showDuration": "600",
			"hideDuration": "1000",
			"timeOut": "0",
			"extendedTimeOut": "0",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"tapToDismiss" : false
		};

	if(tipo=='success'){
		toastr.success(mensaje,titulo, opts);
	}
	if(tipo=='info'){
		toastr.info(mensaje,titulo, opts);
	}
	if(tipo=='error'){
		toastr.error(mensaje,titulo, opts);
	}
	if(tipo=='warning'){
		toastr.warning(mensaje,titulo, opts);
	}
}

function notificacionRegularEstaticaOscura(titulo,mensaje,tipo){
	
	var opts = {
			
			"debug": false,
			"positionClass": rtl() || public_vars.$pageContainer.hasClass('right-sidebar') ? "toast-top-left" : "toast-top-right",
			"toastClass": "black",
			"onclick": null,
			"showDuration": "600",
			"hideDuration": "1000",
			"timeOut": "0",
			"extendedTimeOut": "0",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"tapToDismiss" : false
		};

	if(tipo=='success'){
		toastr.success(mensaje,titulo, opts);
	}
	if(tipo=='info'){
		toastr.info(mensaje,titulo, opts);
	}
	if(tipo=='error'){
		toastr.error(mensaje,titulo, opts);
	}
	if(tipo=='warning'){
		toastr.warning(mensaje,titulo, opts);
	}
}