$(document).ready(function() {

	$('form').trigger("reset");
	$(':text[title]').tooltip({
		placement : "right",
		trigger : "focus"
	});
	$(".clsNroIdentificacion").focus();
	$('#divProcensandoInformacion').hide();
	$('#divModalAlone').hide();

	ocultarFiltroBusqueda();
});

function mostrarDivProcesamientoInformacion() {
	$('#divProcensandoInformacion').show();
	mostrarDivModalAlone();
}

function ocultarDivProcesamientoInformacion() {
	$('#divProcensandoInformacion').hide();
	ocultarDivModalAlone();
}

function mostrarDivModalAlone() {
	$('#divModalAlone').show();
}

function ocultarDivModalAlone() {
	$('#divModalAlone').hide();
}

function ocultarFiltroBusqueda() {
	$(".cajaFiltroBusqueda").hide();
}

/**
 * Función para mostrar la app en pantalla completa para un navegador web
 */

function pantallCompleta() {
	var elem = document.body; // Make the body go full screen.
	requestFullScreen(elem);
}

function requestFullScreen(element) {
	var requestMethod = element.requestFullScreen
			|| element.webkitRequestFullScreen || element.mozRequestFullScreen
			|| element.msRequestFullScreen;
	if (requestMethod) { // Native full screen.
		requestMethod.call(element);
	} else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
		var wscript = new ActiveXObject("WScript.Shell");
		if (wscript !== null) {
			wscript.SendKeys("{F11}");
		}
	}
}

/**
 * Función encargada de limpiar los filtros realizados en un objeto datatable de
 * primefaces
 * 
 * @param tabla ->
 *            Nombre de la tabla que se va a limpiar
 */
function limpiarFiltrosBusquedaDataTable(tabla) {
	// $(".ui-corner-left").click();
	$(".cajaFiltroBusquedaGlobal").val('');
	// $(".cajaFiltroBusquedaFecha").val('');
	PF(tabla).filter();

}

function personalizadoChartPrimefaces() {
	this.cfg.seriesDefaults = {
		renderer : $.jqplot.BarRenderer,
		pointLabels : {
			show : true
		}
	};

	this.cfg.highlighter = {
		show : true,
		tooltipLocation : 's',
		useAxesFormatters : false,
		formatString : 'Hora %s: = %d'
	};

}