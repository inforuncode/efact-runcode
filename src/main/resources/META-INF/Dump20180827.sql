-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: base
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounttypeentity`
--

DROP TABLE IF EXISTS `accounttypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounttypeentity` (
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `loginName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK8oixjku214nivbx3mu3rup9q2` FOREIGN KEY (`id`) REFERENCES `identitytypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounttypeentity`
--

LOCK TABLES `accounttypeentity` WRITE;
/*!40000 ALTER TABLE `accounttypeentity` DISABLE KEYS */;
INSERT INTO `accounttypeentity` VALUES ('runcode@runcode.co','Administrador','Sistema','admin','d1713f8e-c8fb-411e-8937-e9ac25d2a043'),('eaar23@gmail.com','Eyder','Ascuntar','eyder.ascuntar','eedc5559-9fc5-4455-8ade-427d532cba07');
/*!40000 ALTER TABLE `accounttypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `street1` varchar(255) DEFAULT NULL,
  `street2` varchar(255) DEFAULT NULL,
  `fk_person` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi13t5axl45dhn5bm9kxhw9cmr` (`fk_person`),
  CONSTRAINT `FKi13t5axl45dhn5bm9kxhw9cmr` FOREIGN KEY (`fk_person`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributedtypeentity`
--

DROP TABLE IF EXISTS `attributedtypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributedtypeentity` (
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributedtypeentity`
--

LOCK TABLES `attributedtypeentity` WRITE;
/*!40000 ALTER TABLE `attributedtypeentity` DISABLE KEYS */;
INSERT INTO `attributedtypeentity` VALUES ('027233b3-75ad-4a67-9ff3-81c40bbe13e6'),('04339d91-5a00-4c0f-8eb9-526f98e61629'),('21aab606-eaac-4f62-ae73-610a05fe7a84'),('2294557a-3de0-4023-a66a-90e5526c6655'),('2d034cf8-f17d-4b30-8009-c741f8ac03c2'),('2ef30d73-2267-48b1-82b6-fb9ef9092b2c'),('354e44aa-ed94-4deb-a379-26999ac812dd'),('3d7a7299-b56c-48c7-8df0-b89008e4d7de'),('3f9c01d4-caf8-4046-931a-2cc259dc0fae'),('49c423a2-ba75-498e-a847-908bb8366edb'),('681ac9e5-c5e0-46e8-a5c6-4694e264e24c'),('690ad24a-23a3-41be-8c01-51c1a120cf26'),('b80879b4-68dd-4b6b-b81e-24452a54c8dd'),('d1713f8e-c8fb-411e-8937-e9ac25d2a043'),('d8996dba-d432-4b04-91f6-e8eda668c297'),('dcd5fea3-a2fe-4777-adad-30246d4413ad'),('eedc5559-9fc5-4455-8ade-427d532cba07'),('efc44d91-99dd-4cdb-b380-3f1f595d4098'),('f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('fd8cadd6-3051-4da0-bb98-955b66dcf235');
/*!40000 ALTER TABLE `attributedtypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributetypeentity`
--

DROP TABLE IF EXISTS `attributetypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributetypeentity` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3fc0sfh4p5fwqujn3bcqyfi1q` (`owner_id`),
  CONSTRAINT `FK3fc0sfh4p5fwqujn3bcqyfi1q` FOREIGN KEY (`owner_id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributetypeentity`
--

LOCK TABLES `attributetypeentity` WRITE;
/*!40000 ALTER TABLE `attributetypeentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributetypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuracionapp`
--

DROP TABLE IF EXISTS `configuracionapp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuracionapp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estadoAlarmas` bit(1) NOT NULL,
  `estadoApp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuracionapp`
--

LOCK TABLES `configuracionapp` WRITE;
/*!40000 ALTER TABLE `configuracionapp` DISABLE KEYS */;
INSERT INTO `configuracionapp` VALUES (1,'','Desarrollo');
/*!40000 ALTER TABLE `configuracionapp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `digestcredentialtypeentity`
--

DROP TABLE IF EXISTS `digestcredentialtypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `digestcredentialtypeentity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `digestHa1` tinyblob,
  `digestRealm` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrbbr5tqb2rx10fh1a81lqrw0b` (`owner_id`),
  CONSTRAINT `FKrbbr5tqb2rx10fh1a81lqrw0b` FOREIGN KEY (`owner_id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `digestcredentialtypeentity`
--

LOCK TABLES `digestcredentialtypeentity` WRITE;
/*!40000 ALTER TABLE `digestcredentialtypeentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `digestcredentialtypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grouptypeentity`
--

DROP TABLE IF EXISTS `grouptypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grouptypeentity` (
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcfwrrwheglinq7fpqp08nvfl8` (`parent_id`),
  CONSTRAINT `FKcfwrrwheglinq7fpqp08nvfl8` FOREIGN KEY (`parent_id`) REFERENCES `grouptypeentity` (`id`),
  CONSTRAINT `FKknkix69udwy3a3spdaefml776` FOREIGN KEY (`id`) REFERENCES `identitytypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grouptypeentity`
--

LOCK TABLES `grouptypeentity` WRITE;
/*!40000 ALTER TABLE `grouptypeentity` DISABLE KEYS */;
INSERT INTO `grouptypeentity` VALUES ('Empleados','/Empleados','027233b3-75ad-4a67-9ff3-81c40bbe13e6',NULL),('Administradores','/Administradores','04339d91-5a00-4c0f-8eb9-526f98e61629',NULL),('Auditores','/Auditores','681ac9e5-c5e0-46e8-a5c6-4694e264e24c',NULL);
/*!40000 ALTER TABLE `grouptypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (22),(22),(22),(22),(22),(22),(22),(22);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identitytypeentity`
--

DROP TABLE IF EXISTS `identitytypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identitytypeentity` (
  `createdDate` datetime DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `partition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbuy38ltx6ij4ykptxgdi07x05` (`partition_id`),
  CONSTRAINT `FK1qxcf48j9k2h32is731dn50qo` FOREIGN KEY (`id`) REFERENCES `attributedtypeentity` (`id`),
  CONSTRAINT `FKbuy38ltx6ij4ykptxgdi07x05` FOREIGN KEY (`partition_id`) REFERENCES `partitiontypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identitytypeentity`
--

LOCK TABLES `identitytypeentity` WRITE;
/*!40000 ALTER TABLE `identitytypeentity` DISABLE KEYS */;
INSERT INTO `identitytypeentity` VALUES ('2018-08-09 22:23:31','',NULL,'org.picketlink.idm.model.basic.Group','027233b3-75ad-4a67-9ff3-81c40bbe13e6','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:31','',NULL,'org.picketlink.idm.model.basic.Group','04339d91-5a00-4c0f-8eb9-526f98e61629','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32','',NULL,'org.picketlink.idm.model.basic.Role','2ef30d73-2267-48b1-82b6-fb9ef9092b2c','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:31','',NULL,'org.picketlink.idm.model.basic.Group','681ac9e5-c5e0-46e8-a5c6-4694e264e24c','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32','',NULL,'org.picketlink.idm.model.basic.Role','b80879b4-68dd-4b6b-b81e-24452a54c8dd','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32','',NULL,'org.picketlink.idm.model.basic.User','d1713f8e-c8fb-411e-8937-e9ac25d2a043','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32','',NULL,'org.picketlink.idm.model.basic.Role','d8996dba-d432-4b04-91f6-e8eda668c297','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 23:43:41','',NULL,'org.picketlink.idm.model.basic.User','eedc5559-9fc5-4455-8ade-427d532cba07','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32','',NULL,'org.picketlink.idm.model.basic.Role','fd8cadd6-3051-4da0-bb98-955b66dcf235','f4306fa1-492a-4049-bd51-8a8e53f0eb5b');
/*!40000 ALTER TABLE `identitytypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otpcredentialtypeentity`
--

DROP TABLE IF EXISTS `otpcredentialtypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otpcredentialtypeentity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `totpDevice` varchar(255) DEFAULT NULL,
  `totpSecretKey` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7161pxl65ej4ccvh9m4itut4l` (`owner_id`),
  CONSTRAINT `FK7161pxl65ej4ccvh9m4itut4l` FOREIGN KEY (`owner_id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otpcredentialtypeentity`
--

LOCK TABLES `otpcredentialtypeentity` WRITE;
/*!40000 ALTER TABLE `otpcredentialtypeentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `otpcredentialtypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partitiontypeentity`
--

DROP TABLE IF EXISTS `partitiontypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partitiontypeentity` (
  `configurationName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKgyobt200r1w4xcx19mbx6viik` FOREIGN KEY (`id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partitiontypeentity`
--

LOCK TABLES `partitiontypeentity` WRITE;
/*!40000 ALTER TABLE `partitiontypeentity` DISABLE KEYS */;
INSERT INTO `partitiontypeentity` VALUES ('default','default','org.picketlink.idm.model.basic.Realm','690ad24a-23a3-41be-8c01-51c1a120cf26'),('default','RuncodePartition','org.picketlink.idm.model.basic.Realm','f4306fa1-492a-4049-bd51-8a8e53f0eb5b');
/*!40000 ALTER TABLE `partitiontypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwordcredentialtypeentity`
--

DROP TABLE IF EXISTS `passwordcredentialtypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordcredentialtypeentity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `passwordEncodedHash` varchar(255) DEFAULT NULL,
  `passwordSalt` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkuakw2lg2vko45b0hwm3vbf01` (`owner_id`),
  CONSTRAINT `FKkuakw2lg2vko45b0hwm3vbf01` FOREIGN KEY (`owner_id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwordcredentialtypeentity`
--

LOCK TABLES `passwordcredentialtypeentity` WRITE;
/*!40000 ALTER TABLE `passwordcredentialtypeentity` DISABLE KEYS */;
INSERT INTO `passwordcredentialtypeentity` VALUES (1,'2018-08-09 22:23:32',NULL,'org.picketlink.idm.credential.storage.EncodedPasswordStorage','FpsUYx0r2fH6q9YlaIxIl4oxTkU6+/eepa/9hMvni0wO5e+OtLdFQheZpIV1LfNaTqHFEVy94cpJ\nloE1yyh3Og==','-7985712138322016946','d1713f8e-c8fb-411e-8937-e9ac25d2a043'),(21,'2018-08-09 23:44:12',NULL,'org.picketlink.idm.credential.storage.EncodedPasswordStorage','a1PKg2GDtzirZaNjBBlxR935i0m+thOqsyuMeE6PPffJIh8Oxkc08uCzlnMIYGJmtaN4T6zcpxbi\n1woptTy3Zg==','-5136168822416419243','eedc5559-9fc5-4455-8ade-427d532cba07');
/*!40000 ALTER TABLE `passwordcredentialtypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissiontypeentity`
--

DROP TABLE IF EXISTS `permissiontypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissiontypeentity` (
  `id` bigint(20) NOT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `resourceClass` varchar(255) DEFAULT NULL,
  `resourceIdentifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissiontypeentity`
--

LOCK TABLES `permissiontypeentity` WRITE;
/*!40000 ALTER TABLE `permissiontypeentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissiontypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `correo` varchar(255) DEFAULT NULL,
  `fechaNacimiento` datetime DEFAULT NULL,
  `nroCelular` varchar(255) DEFAULT NULL,
  `nroIdentificacion` varchar(255) DEFAULT NULL,
  `primerApellido` varchar(255) DEFAULT NULL,
  `primerNombre` varchar(255) DEFAULT NULL,
  `segundoApellido` varchar(255) DEFAULT NULL,
  `segundoNombre` varchar(255) DEFAULT NULL,
  `usuario_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgu6iwob6h6jxr7yrv72q02vf4` (`usuario_id`),
  CONSTRAINT `FKgu6iwob6h6jxr7yrv72q02vf4` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'runcode@runcode.co',NULL,NULL,NULL,'Administrador','Sistema',NULL,NULL,1),(2,'eaar23@gmail.com',NULL,'3208095046','1120216079','Ascuntar','Eyder','Rosales','Albeiro',2);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationshipidentitytypeentity`
--

DROP TABLE IF EXISTS `relationshipidentitytypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationshipidentitytypeentity` (
  `identifier` bigint(20) NOT NULL,
  `descriptor` varchar(255) DEFAULT NULL,
  `identityType_id` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `FK8b6t4v6mf47qq8941o4i2n3kh` (`identityType_id`),
  KEY `FK974q2hxrr2bi72ciduuockjt` (`owner_id`),
  CONSTRAINT `FK8b6t4v6mf47qq8941o4i2n3kh` FOREIGN KEY (`identityType_id`) REFERENCES `identitytypeentity` (`id`),
  CONSTRAINT `FK974q2hxrr2bi72ciduuockjt` FOREIGN KEY (`owner_id`) REFERENCES `relationshiptypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationshipidentitytypeentity`
--

LOCK TABLES `relationshipidentitytypeentity` WRITE;
/*!40000 ALTER TABLE `relationshipidentitytypeentity` DISABLE KEYS */;
INSERT INTO `relationshipidentitytypeentity` VALUES (2,'member','d1713f8e-c8fb-411e-8937-e9ac25d2a043','21aab606-eaac-4f62-ae73-610a05fe7a84'),(3,'group','04339d91-5a00-4c0f-8eb9-526f98e61629','21aab606-eaac-4f62-ae73-610a05fe7a84'),(4,'member','d1713f8e-c8fb-411e-8937-e9ac25d2a043','efc44d91-99dd-4cdb-b380-3f1f595d4098'),(5,'group','027233b3-75ad-4a67-9ff3-81c40bbe13e6','efc44d91-99dd-4cdb-b380-3f1f595d4098'),(6,'member','d1713f8e-c8fb-411e-8937-e9ac25d2a043','354e44aa-ed94-4deb-a379-26999ac812dd'),(7,'group','681ac9e5-c5e0-46e8-a5c6-4694e264e24c','354e44aa-ed94-4deb-a379-26999ac812dd'),(8,'role','d8996dba-d432-4b04-91f6-e8eda668c297','3d7a7299-b56c-48c7-8df0-b89008e4d7de'),(9,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','3d7a7299-b56c-48c7-8df0-b89008e4d7de'),(10,'role','2ef30d73-2267-48b1-82b6-fb9ef9092b2c','2294557a-3de0-4023-a66a-90e5526c6655'),(11,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','2294557a-3de0-4023-a66a-90e5526c6655'),(12,'role','b80879b4-68dd-4b6b-b81e-24452a54c8dd','2d034cf8-f17d-4b30-8009-c741f8ac03c2'),(13,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','2d034cf8-f17d-4b30-8009-c741f8ac03c2'),(14,'role','fd8cadd6-3051-4da0-bb98-955b66dcf235','3f9c01d4-caf8-4046-931a-2cc259dc0fae'),(15,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','3f9c01d4-caf8-4046-931a-2cc259dc0fae'),(17,'member','eedc5559-9fc5-4455-8ade-427d532cba07','dcd5fea3-a2fe-4777-adad-30246d4413ad'),(18,'group','027233b3-75ad-4a67-9ff3-81c40bbe13e6','dcd5fea3-a2fe-4777-adad-30246d4413ad'),(19,'role','fd8cadd6-3051-4da0-bb98-955b66dcf235','49c423a2-ba75-498e-a847-908bb8366edb'),(20,'assignee','eedc5559-9fc5-4455-8ade-427d532cba07','49c423a2-ba75-498e-a847-908bb8366edb');
/*!40000 ALTER TABLE `relationshipidentitytypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationshiptypeentity`
--

DROP TABLE IF EXISTS `relationshiptypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationshiptypeentity` (
  `typeName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKj04ddo357wkwmkb3rptbxsin3` FOREIGN KEY (`id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationshiptypeentity`
--

LOCK TABLES `relationshiptypeentity` WRITE;
/*!40000 ALTER TABLE `relationshiptypeentity` DISABLE KEYS */;
INSERT INTO `relationshiptypeentity` VALUES ('org.picketlink.idm.model.basic.GroupMembership','21aab606-eaac-4f62-ae73-610a05fe7a84'),('org.picketlink.idm.model.basic.Grant','2294557a-3de0-4023-a66a-90e5526c6655'),('org.picketlink.idm.model.basic.Grant','2d034cf8-f17d-4b30-8009-c741f8ac03c2'),('org.picketlink.idm.model.basic.GroupMembership','354e44aa-ed94-4deb-a379-26999ac812dd'),('org.picketlink.idm.model.basic.Grant','3d7a7299-b56c-48c7-8df0-b89008e4d7de'),('org.picketlink.idm.model.basic.Grant','3f9c01d4-caf8-4046-931a-2cc259dc0fae'),('org.picketlink.idm.model.basic.Grant','49c423a2-ba75-498e-a847-908bb8366edb'),('org.picketlink.idm.model.basic.GroupMembership','dcd5fea3-a2fe-4777-adad-30246d4413ad'),('org.picketlink.idm.model.basic.GroupMembership','efc44d91-99dd-4cdb-b380-3f1f595d4098');
/*!40000 ALTER TABLE `relationshiptypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roletypeentity`
--

DROP TABLE IF EXISTS `roletypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roletypeentity` (
  `name` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKnyw0xnnlou75od1kp01n5jhfj` FOREIGN KEY (`id`) REFERENCES `identitytypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roletypeentity`
--

LOCK TABLES `roletypeentity` WRITE;
/*!40000 ALTER TABLE `roletypeentity` DISABLE KEYS */;
INSERT INTO `roletypeentity` VALUES ('Soporte','2ef30d73-2267-48b1-82b6-fb9ef9092b2c'),('Auditor','b80879b4-68dd-4b6b-b81e-24452a54c8dd'),('SuperAdministrador','d8996dba-d432-4b04-91f6-e8eda668c297'),('Operador','fd8cadd6-3051-4da0-bb98-955b66dcf235');
/*!40000 ALTER TABLE `roletypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_datos_facturador`
--

DROP TABLE IF EXISTS `tbl_datos_facturador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_datos_facturador` (
  `nit` varchar(255) NOT NULL,
  `certPwd` varchar(255) DEFAULT NULL,
  `certRuta` varchar(255) DEFAULT NULL,
  `facturasRuta` varchar(255) DEFAULT NULL,
  `claveTecnica` varchar(255) DEFAULT NULL,
  `fchFinalVigencia` date DEFAULT NULL,
  `fchInicialVigencia` date DEFAULT NULL,
  `fchResolucion` date DEFAULT NULL,
  `prefijo` varchar(255) DEFAULT NULL,
  `rangoDesde` int(11) DEFAULT NULL,
  `rangoHasta` int(11) DEFAULT NULL,
  `resolucion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`nit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_datos_facturador`
--

LOCK TABLES `tbl_datos_facturador` WRITE;
/*!40000 ALTER TABLE `tbl_datos_facturador` DISABLE KEYS */;
INSERT INTO `tbl_datos_facturador` VALUES ('846000553','persona_juridica_pruebas','D:/eebp/documentos/persona_juridica_pruebas_vigente.p12','D:/eebp/facturas','dd85db55545bd6566f36b0fd3be9fd8555c36e','2028-07-04','2018-07-04','2018-07-04','PRUE',990000000,995000000,'9000000035671710');
/*!40000 ALTER TABLE `tbl_datos_facturador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_datos_proveedor`
--

DROP TABLE IF EXISTS `tbl_datos_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_datos_proveedor` (
  `idSoftware` varchar(255) NOT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `nit` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idSoftware`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_datos_proveedor`
--

LOCK TABLES `tbl_datos_proveedor` WRITE;
/*!40000 ALTER TABLE `tbl_datos_proveedor` DISABLE KEYS */;
INSERT INTO `tbl_datos_proveedor` VALUES ('b73ec1ef-b215-4405-8bdd-bfd3f08bcc5a','true','846000553','846000553EfactRuncodeEEBPSA','FeebpT1');
/*!40000 ALTER TABLE `tbl_datos_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokencredentialtypeentity`
--

DROP TABLE IF EXISTS `tokencredentialtypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokencredentialtypeentity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `token` text,
  `type` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKax6xg86mlowbydnyg645ekvik` (`owner_id`),
  CONSTRAINT `FKax6xg86mlowbydnyg645ekvik` FOREIGN KEY (`owner_id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokencredentialtypeentity`
--

LOCK TABLES `tokencredentialtypeentity` WRITE;
/*!40000 ALTER TABLE `tokencredentialtypeentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokencredentialtypeentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codVerificacion` varchar(255) DEFAULT NULL,
  `cuentaVerificada` bit(1) NOT NULL,
  `estadoCuenta` bit(1) NOT NULL,
  `fechaRegistro` datetime DEFAULT NULL,
  `fechaUltimaSincronziacion` datetime DEFAULT NULL,
  `fechaUltimoIngreso` datetime DEFAULT NULL,
  `nomUsuario` varchar(255) DEFAULT NULL,
  `password` text,
  `passwordActualizado` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bmjynv6rdddajppf6vi4h4lmd` (`nomUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,NULL,'','',NULL,NULL,'2018-08-24 21:11:11','admin',NULL,''),(2,'51b416826718d488','','','2018-08-09 23:43:41',NULL,'2018-08-09 23:44:06','eyder.ascuntar','ZfuGq+Bo1cdekS0dhn9lNg/OKXjFD+izOgQlpUmTCstLKxXpnTtHwx8AE5yYmTzc','');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x509credentialtypeentity`
--

DROP TABLE IF EXISTS `x509credentialtypeentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x509credentialtypeentity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `base64Cert` varchar(1024) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4e6op6f4hal8weulx8mb3u2ir` (`owner_id`),
  CONSTRAINT `FK4e6op6f4hal8weulx8mb3u2ir` FOREIGN KEY (`owner_id`) REFERENCES `attributedtypeentity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x509credentialtypeentity`
--

LOCK TABLES `x509credentialtypeentity` WRITE;
/*!40000 ALTER TABLE `x509credentialtypeentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `x509credentialtypeentity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-27 11:12:14
