-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: baseDB
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AccountTypeEntity`
--

DROP TABLE IF EXISTS `AccountTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AccountTypeEntity` (
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `loginName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK8oixjku214nivbx3mu3rup9q2` FOREIGN KEY (`id`) REFERENCES `IdentityTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AccountTypeEntity`
--

LOCK TABLES `AccountTypeEntity` WRITE;
/*!40000 ALTER TABLE `AccountTypeEntity` DISABLE KEYS */;
INSERT INTO `AccountTypeEntity` VALUES ('runcode@runcode.co','Administrador','Sistema','admin','d1713f8e-c8fb-411e-8937-e9ac25d2a043'),('eaar23@gmail.com','Eyder','Ascuntar','eyder.ascuntar','eedc5559-9fc5-4455-8ade-427d532cba07');
/*!40000 ALTER TABLE `AccountTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Address`
--

DROP TABLE IF EXISTS `Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `street1` varchar(255) DEFAULT NULL,
  `street2` varchar(255) DEFAULT NULL,
  `fk_person` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKi13t5axl45dhn5bm9kxhw9cmr` (`fk_person`),
  CONSTRAINT `FKi13t5axl45dhn5bm9kxhw9cmr` FOREIGN KEY (`fk_person`) REFERENCES `Persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Address`
--

LOCK TABLES `Address` WRITE;
/*!40000 ALTER TABLE `Address` DISABLE KEYS */;
/*!40000 ALTER TABLE `Address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AttributeTypeEntity`
--

DROP TABLE IF EXISTS `AttributeTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AttributeTypeEntity` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3fc0sfh4p5fwqujn3bcqyfi1q` (`owner_id`),
  CONSTRAINT `FK3fc0sfh4p5fwqujn3bcqyfi1q` FOREIGN KEY (`owner_id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AttributeTypeEntity`
--

LOCK TABLES `AttributeTypeEntity` WRITE;
/*!40000 ALTER TABLE `AttributeTypeEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `AttributeTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AttributedTypeEntity`
--

DROP TABLE IF EXISTS `AttributedTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AttributedTypeEntity` (
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AttributedTypeEntity`
--

LOCK TABLES `AttributedTypeEntity` WRITE;
/*!40000 ALTER TABLE `AttributedTypeEntity` DISABLE KEYS */;
INSERT INTO `AttributedTypeEntity` VALUES ('027233b3-75ad-4a67-9ff3-81c40bbe13e6'),('04339d91-5a00-4c0f-8eb9-526f98e61629'),('21aab606-eaac-4f62-ae73-610a05fe7a84'),('2294557a-3de0-4023-a66a-90e5526c6655'),('2d034cf8-f17d-4b30-8009-c741f8ac03c2'),('2ef30d73-2267-48b1-82b6-fb9ef9092b2c'),('354e44aa-ed94-4deb-a379-26999ac812dd'),('3d7a7299-b56c-48c7-8df0-b89008e4d7de'),('3f9c01d4-caf8-4046-931a-2cc259dc0fae'),('49c423a2-ba75-498e-a847-908bb8366edb'),('681ac9e5-c5e0-46e8-a5c6-4694e264e24c'),('690ad24a-23a3-41be-8c01-51c1a120cf26'),('b80879b4-68dd-4b6b-b81e-24452a54c8dd'),('d1713f8e-c8fb-411e-8937-e9ac25d2a043'),('d8996dba-d432-4b04-91f6-e8eda668c297'),('dcd5fea3-a2fe-4777-adad-30246d4413ad'),('eedc5559-9fc5-4455-8ade-427d532cba07'),('efc44d91-99dd-4cdb-b380-3f1f595d4098'),('f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('fd8cadd6-3051-4da0-bb98-955b66dcf235');
/*!40000 ALTER TABLE `AttributedTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConfiguracionApp`
--

DROP TABLE IF EXISTS `ConfiguracionApp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConfiguracionApp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estadoAlarmas` bit(1) NOT NULL,
  `estadoApp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConfiguracionApp`
--

LOCK TABLES `ConfiguracionApp` WRITE;
/*!40000 ALTER TABLE `ConfiguracionApp` DISABLE KEYS */;
INSERT INTO `ConfiguracionApp` VALUES (1,_binary '','Desarrollo');
/*!40000 ALTER TABLE `ConfiguracionApp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DigestCredentialTypeEntity`
--

DROP TABLE IF EXISTS `DigestCredentialTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DigestCredentialTypeEntity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `digestHa1` tinyblob,
  `digestRealm` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrbbr5tqb2rx10fh1a81lqrw0b` (`owner_id`),
  CONSTRAINT `FKrbbr5tqb2rx10fh1a81lqrw0b` FOREIGN KEY (`owner_id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DigestCredentialTypeEntity`
--

LOCK TABLES `DigestCredentialTypeEntity` WRITE;
/*!40000 ALTER TABLE `DigestCredentialTypeEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `DigestCredentialTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GroupTypeEntity`
--

DROP TABLE IF EXISTS `GroupTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupTypeEntity` (
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcfwrrwheglinq7fpqp08nvfl8` (`parent_id`),
  CONSTRAINT `FKcfwrrwheglinq7fpqp08nvfl8` FOREIGN KEY (`parent_id`) REFERENCES `GroupTypeEntity` (`id`),
  CONSTRAINT `FKknkix69udwy3a3spdaefml776` FOREIGN KEY (`id`) REFERENCES `IdentityTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GroupTypeEntity`
--

LOCK TABLES `GroupTypeEntity` WRITE;
/*!40000 ALTER TABLE `GroupTypeEntity` DISABLE KEYS */;
INSERT INTO `GroupTypeEntity` VALUES ('Empleados','/Empleados','027233b3-75ad-4a67-9ff3-81c40bbe13e6',NULL),('Administradores','/Administradores','04339d91-5a00-4c0f-8eb9-526f98e61629',NULL),('Auditores','/Auditores','681ac9e5-c5e0-46e8-a5c6-4694e264e24c',NULL);
/*!40000 ALTER TABLE `GroupTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IdentityTypeEntity`
--

DROP TABLE IF EXISTS `IdentityTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IdentityTypeEntity` (
  `createdDate` datetime DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `partition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbuy38ltx6ij4ykptxgdi07x05` (`partition_id`),
  CONSTRAINT `FK1qxcf48j9k2h32is731dn50qo` FOREIGN KEY (`id`) REFERENCES `AttributedTypeEntity` (`id`),
  CONSTRAINT `FKbuy38ltx6ij4ykptxgdi07x05` FOREIGN KEY (`partition_id`) REFERENCES `PartitionTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IdentityTypeEntity`
--

LOCK TABLES `IdentityTypeEntity` WRITE;
/*!40000 ALTER TABLE `IdentityTypeEntity` DISABLE KEYS */;
INSERT INTO `IdentityTypeEntity` VALUES ('2018-08-09 22:23:31',_binary '',NULL,'org.picketlink.idm.model.basic.Group','027233b3-75ad-4a67-9ff3-81c40bbe13e6','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:31',_binary '',NULL,'org.picketlink.idm.model.basic.Group','04339d91-5a00-4c0f-8eb9-526f98e61629','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32',_binary '',NULL,'org.picketlink.idm.model.basic.Role','2ef30d73-2267-48b1-82b6-fb9ef9092b2c','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:31',_binary '',NULL,'org.picketlink.idm.model.basic.Group','681ac9e5-c5e0-46e8-a5c6-4694e264e24c','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32',_binary '',NULL,'org.picketlink.idm.model.basic.Role','b80879b4-68dd-4b6b-b81e-24452a54c8dd','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32',_binary '',NULL,'org.picketlink.idm.model.basic.User','d1713f8e-c8fb-411e-8937-e9ac25d2a043','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32',_binary '',NULL,'org.picketlink.idm.model.basic.Role','d8996dba-d432-4b04-91f6-e8eda668c297','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 23:43:41',_binary '',NULL,'org.picketlink.idm.model.basic.User','eedc5559-9fc5-4455-8ade-427d532cba07','f4306fa1-492a-4049-bd51-8a8e53f0eb5b'),('2018-08-09 22:23:32',_binary '',NULL,'org.picketlink.idm.model.basic.Role','fd8cadd6-3051-4da0-bb98-955b66dcf235','f4306fa1-492a-4049-bd51-8a8e53f0eb5b');
/*!40000 ALTER TABLE `IdentityTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OTPCredentialTypeEntity`
--

DROP TABLE IF EXISTS `OTPCredentialTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OTPCredentialTypeEntity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `totpDevice` varchar(255) DEFAULT NULL,
  `totpSecretKey` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7161pxl65ej4ccvh9m4itut4l` (`owner_id`),
  CONSTRAINT `FK7161pxl65ej4ccvh9m4itut4l` FOREIGN KEY (`owner_id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OTPCredentialTypeEntity`
--

LOCK TABLES `OTPCredentialTypeEntity` WRITE;
/*!40000 ALTER TABLE `OTPCredentialTypeEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `OTPCredentialTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PartitionTypeEntity`
--

DROP TABLE IF EXISTS `PartitionTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PartitionTypeEntity` (
  `configurationName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKgyobt200r1w4xcx19mbx6viik` FOREIGN KEY (`id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PartitionTypeEntity`
--

LOCK TABLES `PartitionTypeEntity` WRITE;
/*!40000 ALTER TABLE `PartitionTypeEntity` DISABLE KEYS */;
INSERT INTO `PartitionTypeEntity` VALUES ('default','default','org.picketlink.idm.model.basic.Realm','690ad24a-23a3-41be-8c01-51c1a120cf26'),('default','RuncodePartition','org.picketlink.idm.model.basic.Realm','f4306fa1-492a-4049-bd51-8a8e53f0eb5b');
/*!40000 ALTER TABLE `PartitionTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PasswordCredentialTypeEntity`
--

DROP TABLE IF EXISTS `PasswordCredentialTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PasswordCredentialTypeEntity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `passwordEncodedHash` varchar(255) DEFAULT NULL,
  `passwordSalt` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkuakw2lg2vko45b0hwm3vbf01` (`owner_id`),
  CONSTRAINT `FKkuakw2lg2vko45b0hwm3vbf01` FOREIGN KEY (`owner_id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PasswordCredentialTypeEntity`
--

LOCK TABLES `PasswordCredentialTypeEntity` WRITE;
/*!40000 ALTER TABLE `PasswordCredentialTypeEntity` DISABLE KEYS */;
INSERT INTO `PasswordCredentialTypeEntity` VALUES (1,'2018-08-09 22:23:32',NULL,'org.picketlink.idm.credential.storage.EncodedPasswordStorage','FpsUYx0r2fH6q9YlaIxIl4oxTkU6+/eepa/9hMvni0wO5e+OtLdFQheZpIV1LfNaTqHFEVy94cpJ\nloE1yyh3Og==','-7985712138322016946','d1713f8e-c8fb-411e-8937-e9ac25d2a043'),(21,'2018-08-09 23:44:12',NULL,'org.picketlink.idm.credential.storage.EncodedPasswordStorage','a1PKg2GDtzirZaNjBBlxR935i0m+thOqsyuMeE6PPffJIh8Oxkc08uCzlnMIYGJmtaN4T6zcpxbi\n1woptTy3Zg==','-5136168822416419243','eedc5559-9fc5-4455-8ade-427d532cba07');
/*!40000 ALTER TABLE `PasswordCredentialTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PermissionTypeEntity`
--

DROP TABLE IF EXISTS `PermissionTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PermissionTypeEntity` (
  `id` bigint(20) NOT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `resourceClass` varchar(255) DEFAULT NULL,
  `resourceIdentifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PermissionTypeEntity`
--

LOCK TABLES `PermissionTypeEntity` WRITE;
/*!40000 ALTER TABLE `PermissionTypeEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `PermissionTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Persona`
--

DROP TABLE IF EXISTS `Persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Persona` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `correo` varchar(255) DEFAULT NULL,
  `fechaNacimiento` datetime DEFAULT NULL,
  `nroCelular` varchar(255) DEFAULT NULL,
  `nroIdentificacion` varchar(255) DEFAULT NULL,
  `primerApellido` varchar(255) DEFAULT NULL,
  `primerNombre` varchar(255) DEFAULT NULL,
  `segundoApellido` varchar(255) DEFAULT NULL,
  `segundoNombre` varchar(255) DEFAULT NULL,
  `usuario_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgu6iwob6h6jxr7yrv72q02vf4` (`usuario_id`),
  CONSTRAINT `FKgu6iwob6h6jxr7yrv72q02vf4` FOREIGN KEY (`usuario_id`) REFERENCES `Usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Persona`
--

LOCK TABLES `Persona` WRITE;
/*!40000 ALTER TABLE `Persona` DISABLE KEYS */;
INSERT INTO `Persona` VALUES (1,'runcode@runcode.co',NULL,NULL,NULL,'Administrador','Sistema',NULL,NULL,1),(2,'eaar23@gmail.com',NULL,'3208095046','1120216079','Ascuntar','Eyder','Rosales','Albeiro',2);
/*!40000 ALTER TABLE `Persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RelationshipIdentityTypeEntity`
--

DROP TABLE IF EXISTS `RelationshipIdentityTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelationshipIdentityTypeEntity` (
  `identifier` bigint(20) NOT NULL,
  `descriptor` varchar(255) DEFAULT NULL,
  `identityType_id` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`identifier`),
  KEY `FK8b6t4v6mf47qq8941o4i2n3kh` (`identityType_id`),
  KEY `FK974q2hxrr2bi72ciduuockjt` (`owner_id`),
  CONSTRAINT `FK8b6t4v6mf47qq8941o4i2n3kh` FOREIGN KEY (`identityType_id`) REFERENCES `IdentityTypeEntity` (`id`),
  CONSTRAINT `FK974q2hxrr2bi72ciduuockjt` FOREIGN KEY (`owner_id`) REFERENCES `RelationshipTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RelationshipIdentityTypeEntity`
--

LOCK TABLES `RelationshipIdentityTypeEntity` WRITE;
/*!40000 ALTER TABLE `RelationshipIdentityTypeEntity` DISABLE KEYS */;
INSERT INTO `RelationshipIdentityTypeEntity` VALUES (2,'member','d1713f8e-c8fb-411e-8937-e9ac25d2a043','21aab606-eaac-4f62-ae73-610a05fe7a84'),(3,'group','04339d91-5a00-4c0f-8eb9-526f98e61629','21aab606-eaac-4f62-ae73-610a05fe7a84'),(4,'member','d1713f8e-c8fb-411e-8937-e9ac25d2a043','efc44d91-99dd-4cdb-b380-3f1f595d4098'),(5,'group','027233b3-75ad-4a67-9ff3-81c40bbe13e6','efc44d91-99dd-4cdb-b380-3f1f595d4098'),(6,'member','d1713f8e-c8fb-411e-8937-e9ac25d2a043','354e44aa-ed94-4deb-a379-26999ac812dd'),(7,'group','681ac9e5-c5e0-46e8-a5c6-4694e264e24c','354e44aa-ed94-4deb-a379-26999ac812dd'),(8,'role','d8996dba-d432-4b04-91f6-e8eda668c297','3d7a7299-b56c-48c7-8df0-b89008e4d7de'),(9,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','3d7a7299-b56c-48c7-8df0-b89008e4d7de'),(10,'role','2ef30d73-2267-48b1-82b6-fb9ef9092b2c','2294557a-3de0-4023-a66a-90e5526c6655'),(11,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','2294557a-3de0-4023-a66a-90e5526c6655'),(12,'role','b80879b4-68dd-4b6b-b81e-24452a54c8dd','2d034cf8-f17d-4b30-8009-c741f8ac03c2'),(13,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','2d034cf8-f17d-4b30-8009-c741f8ac03c2'),(14,'role','fd8cadd6-3051-4da0-bb98-955b66dcf235','3f9c01d4-caf8-4046-931a-2cc259dc0fae'),(15,'assignee','d1713f8e-c8fb-411e-8937-e9ac25d2a043','3f9c01d4-caf8-4046-931a-2cc259dc0fae'),(17,'member','eedc5559-9fc5-4455-8ade-427d532cba07','dcd5fea3-a2fe-4777-adad-30246d4413ad'),(18,'group','027233b3-75ad-4a67-9ff3-81c40bbe13e6','dcd5fea3-a2fe-4777-adad-30246d4413ad'),(19,'role','fd8cadd6-3051-4da0-bb98-955b66dcf235','49c423a2-ba75-498e-a847-908bb8366edb'),(20,'assignee','eedc5559-9fc5-4455-8ade-427d532cba07','49c423a2-ba75-498e-a847-908bb8366edb');
/*!40000 ALTER TABLE `RelationshipIdentityTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RelationshipTypeEntity`
--

DROP TABLE IF EXISTS `RelationshipTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelationshipTypeEntity` (
  `typeName` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKj04ddo357wkwmkb3rptbxsin3` FOREIGN KEY (`id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RelationshipTypeEntity`
--

LOCK TABLES `RelationshipTypeEntity` WRITE;
/*!40000 ALTER TABLE `RelationshipTypeEntity` DISABLE KEYS */;
INSERT INTO `RelationshipTypeEntity` VALUES ('org.picketlink.idm.model.basic.GroupMembership','21aab606-eaac-4f62-ae73-610a05fe7a84'),('org.picketlink.idm.model.basic.Grant','2294557a-3de0-4023-a66a-90e5526c6655'),('org.picketlink.idm.model.basic.Grant','2d034cf8-f17d-4b30-8009-c741f8ac03c2'),('org.picketlink.idm.model.basic.GroupMembership','354e44aa-ed94-4deb-a379-26999ac812dd'),('org.picketlink.idm.model.basic.Grant','3d7a7299-b56c-48c7-8df0-b89008e4d7de'),('org.picketlink.idm.model.basic.Grant','3f9c01d4-caf8-4046-931a-2cc259dc0fae'),('org.picketlink.idm.model.basic.Grant','49c423a2-ba75-498e-a847-908bb8366edb'),('org.picketlink.idm.model.basic.GroupMembership','dcd5fea3-a2fe-4777-adad-30246d4413ad'),('org.picketlink.idm.model.basic.GroupMembership','efc44d91-99dd-4cdb-b380-3f1f595d4098');
/*!40000 ALTER TABLE `RelationshipTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RoleTypeEntity`
--

DROP TABLE IF EXISTS `RoleTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RoleTypeEntity` (
  `name` varchar(255) DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FKnyw0xnnlou75od1kp01n5jhfj` FOREIGN KEY (`id`) REFERENCES `IdentityTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RoleTypeEntity`
--

LOCK TABLES `RoleTypeEntity` WRITE;
/*!40000 ALTER TABLE `RoleTypeEntity` DISABLE KEYS */;
INSERT INTO `RoleTypeEntity` VALUES ('Soporte','2ef30d73-2267-48b1-82b6-fb9ef9092b2c'),('Auditor','b80879b4-68dd-4b6b-b81e-24452a54c8dd'),('SuperAdministrador','d8996dba-d432-4b04-91f6-e8eda668c297'),('Operador','fd8cadd6-3051-4da0-bb98-955b66dcf235');
/*!40000 ALTER TABLE `RoleTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TokenCredentialTypeEntity`
--

DROP TABLE IF EXISTS `TokenCredentialTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TokenCredentialTypeEntity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `token` text,
  `type` varchar(255) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKax6xg86mlowbydnyg645ekvik` (`owner_id`),
  CONSTRAINT `FKax6xg86mlowbydnyg645ekvik` FOREIGN KEY (`owner_id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TokenCredentialTypeEntity`
--

LOCK TABLES `TokenCredentialTypeEntity` WRITE;
/*!40000 ALTER TABLE `TokenCredentialTypeEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `TokenCredentialTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codVerificacion` varchar(255) DEFAULT NULL,
  `cuentaVerificada` bit(1) NOT NULL,
  `estadoCuenta` bit(1) NOT NULL,
  `fechaRegistro` datetime DEFAULT NULL,
  `fechaUltimaSincronziacion` datetime DEFAULT NULL,
  `fechaUltimoIngreso` datetime DEFAULT NULL,
  `nomUsuario` varchar(255) DEFAULT NULL,
  `password` text,
  `passwordActualizado` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bmjynv6rdddajppf6vi4h4lmd` (`nomUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES (1,NULL,_binary '',_binary '',NULL,NULL,'2018-08-10 00:03:11','admin',NULL,_binary ''),(2,'51b416826718d488',_binary '',_binary '','2018-08-09 23:43:41',NULL,'2018-08-09 23:44:06','eyder.ascuntar','ZfuGq+Bo1cdekS0dhn9lNg/OKXjFD+izOgQlpUmTCstLKxXpnTtHwx8AE5yYmTzc',_binary '');
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `X509CredentialTypeEntity`
--

DROP TABLE IF EXISTS `X509CredentialTypeEntity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `X509CredentialTypeEntity` (
  `id` bigint(20) NOT NULL,
  `effectiveDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `typeName` varchar(255) DEFAULT NULL,
  `base64Cert` varchar(1024) DEFAULT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4e6op6f4hal8weulx8mb3u2ir` (`owner_id`),
  CONSTRAINT `FK4e6op6f4hal8weulx8mb3u2ir` FOREIGN KEY (`owner_id`) REFERENCES `AttributedTypeEntity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `X509CredentialTypeEntity`
--

LOCK TABLES `X509CredentialTypeEntity` WRITE;
/*!40000 ALTER TABLE `X509CredentialTypeEntity` DISABLE KEYS */;
/*!40000 ALTER TABLE `X509CredentialTypeEntity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (22),(22),(22),(22),(22),(22),(22),(22);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10  0:04:37
